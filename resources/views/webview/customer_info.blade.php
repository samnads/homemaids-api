<html>

<head>
    <title>Customer Information</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous">
    </script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
        integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://code.jquery.com/jquery-3.7.1.min.js"
        integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
</head>

<body>
    <style>
        #button {
            display: inline-block;
            background-color: #00a542;
            color: #fff !important;
            width: 50px;
            height: 50px;
            text-align: center;
            border-radius: 4px;
            position: fixed;
            bottom: 30px;
            right: 30px;
            transition: background-color .3s,
                opacity .5s, visibility .5s;
            opacity: 0;
            visibility: hidden;
            z-index: 1000;
        }

        #button::after {
            content: "\f077";
            font-family: FontAwesome;
            font-weight: normal;
            font-style: normal;
            font-size: 2em;
            line-height: 50px;
            color: #fff;
        }

        #button:hover {
            cursor: pointer;
            background-color: #333;
        }

        #button:active {
            background-color: #555;
        }

        #button.show {
            opacity: 1;
            visibility: visible;
        }
        table {
            border-collapse: collapse;
            border-spacing: 0;
            width: 75%;
            border: 1px solid #ddd;
        }
        th, td {
            text-align: left;
            padding: 8px;
        }
        tr:nth-child(even){background-color: #f2f2f2}
    </style>
    <a id="button"></a>
    <div class="container">
        <p>For Home Maids Cleaning Services, the time required for each task may vary depending on the size and specific needs of your home. As a guideline:</p>
        <p>Duration of Cleaning:</p>
        <table style="width:100%">
            <tr>
                <td>Studio</td>
                <td>Approximately 2 hours</td>
            </tr>
            <tr>
                <td>1 Bedroom</td>
                <td>2-3 hours</td>
            </tr>
            <tr>
                <td>2 Bedrooms</td>
                <td>3-4 hours</td>
            </tr>
            <tr>
                <td>3 Bedrooms</td>
                <td>4-5 hours</td>
            </tr>
            <tr>
                <td>4 Bedrooms</td>
                <td>5-6 hours</td>
            </tr>
            <tr>
                <td>5 Bedrooms</td>
                <td>6-7 hours</td>
            </tr>
        </table>
        <p>Our regular cleaning service includes essential tasks such as general cleaning, surface wiping, vacuuming and mopping. If you require additional tasks like oven or fridge cleaning or balcony cleaning, we recommend allocating an extra 30 minutes per requirement</p>
        <p>For villa residences, we advise booking an additional hour to ensure thorough cleaning.</p>
        <p>Below is a checklist outlining the estimated time for regular cleaning tasks, but we encourage discussing your specific preferences with our professionals:</p>
        <p style="font-weight: bold;">Kitchen - Approximately 30 minutes</p>
        <ol type="1">
            <li>Wash dishes</li>
            <li>Wipe sink and countertops</li>
            <li>Clean kitchen appliances</li>
        </ol>
        <p style="font-weight: bold;">Bathroom - Approximately 30 minutes</p>
        <ol type="1">
            <li>Clean bathtub and showers</li>
            <li>Clean toilet and bidet</li>
            <li>Wipe sink and countertops</li>
            <li>Clean mirrors</li>
        </ol>
        <p style="font-weight: bold;">Bedroom - Approximately 20 minutes</p>
        <ol type="1">
            <li>Make beds</li>
            <li>Fold clothes</li>
            <li>Clear mirrors</li>
        </ol>
        <p style="font-weight: bold;">General - Approximately 40 minutes</p>
        <ol type="1">
            <li>Organize items</li>
            <li>Vacuum floors/rugs</li>
            <li>Dust down furniture</li>
            <li>Dust down all surfaces</li>
            <li>Wipe light switches</li>
            <li>Wipe door handles</li>
            <li>Wipe window ledges</li>
            <li>Mop floors</li>
            <li>Take out rubbish</li>
        </ol>
        <p style="font-weight: bold;">Extras - Add an additional 30-45 minutes per task</p>
        <ol type="1">
            <li>Laundry</li>
            <li>Changing sheets and pillowcases</li>
            <li>Cleaning interior windows</li>
            <li>Cleaning balcony/patio</li>
            <li>Cleaning inside cupboards</li>
            <li>Cleaning inside oven/fridge</li>
        </ol>
        <p style="font-weight: bold;">Ironing</p>
        <p>What's Included?</p>
        <ol type="1">
            <li>Each piece of clothing will take 3-5 minutes,</li>
            <li>Ironing and folding of up 15-20 clothing items in an hour</li>
            <li>Please note that a good iron and board should be provided by the customer.</li>
            <li>Any special instructions regarding the materials and temperature for ironing should be communicated in advance</li>
        </ol>
        <p>What’s excluded?</p>
        <ol type="1">
            <li>Kanduras, Abayas, Sarees are not included.</li>
        </ol>
    </div>
    <script>
        var btn = $('#button');
        $(window).scroll(function() {
            if ($(window).scrollTop() > 300) {
                btn.addClass('show');
            } else {
                btn.removeClass('show');
            }
        });

        btn.on('click', function(e) {
            e.preventDefault();
            $('html, body').animate({
                scrollTop: 0
            }, '300');
        });
    </script>
</body>

</html>
