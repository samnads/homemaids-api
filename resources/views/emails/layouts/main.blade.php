<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="tr" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml"
    xmlns:o="urn:schemas-microsoft-com:office:office">
@include('emails.includes.head_tag_section')
<body style="margin: 0px; padding: 0px;">
    <div class="wrapper-main" style="width: 800px; height: auto; margin: 30px auto 10px;">
        @include('emails.includes.header_banner')
        <div class="container" style="width: 700px; height: auto; margin: 30px auto 0; overflow: hidden !important;">
            @yield('content')
        </div>
        @include('emails.includes.footer_copyright')
    </div>
</body>
</html>
@if($test === true)
{{die()}}
@endif