@if(sizeof($booking_packages))
<div class="section-123" style="width: 700px; height: auto; margin: 50px auto 0;">
    <div style="width: 700px;">
        <p
        style="font: 500 20px/25px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 0px 15px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
        Service Packages</p>
        @foreach ($booking_packages as $booking_package)
            <div class="add-on-tmb"
                style="width: 330px; margin: 0px 10px 20px; height: auto; background: #f7f7f7; border-radius: 7px; overflow: hidden; float: left;">
                <div style="width: 100%; height: auto"><a href="javascript:void(0);"
                        style="display: block; cursor: default !important;"><img
                            src="{{$booking_package->thumbnail_url}}" width="100%"
                            height="" alt="" style="cursor: default !important;" /></a></div>
                <div style="width: 100%; height: auto">
                    <p
                        style="font: 600 18px/25px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 20px 20px 15px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                        {{$booking_package->package_name}}</p>
                    <!--<p
                        style="font: 500 14px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 20px 5px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                        {{$booking_package->package_name}}</p>-->
                    <p
                        style="font: normal 13px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 20px 15px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                        {{$booking_package->package_description}}</p>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <p
                                    style="font: bold 20px/25px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 20px 15px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                                    AED
                                    <label
                                        style="font-size: 17px; color: #999; text-decoration: line-through;">{{$booking_package->strike_amount}}</label>
                                    {{$booking_package->unit_price}}
                                </p>
                            </td>
                            <td>
                                <p
                                    style="font: bold 20px/25px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 20px 15px; text-align: right; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                                    <label
                                        style="display: inline-block; background: #FFF; padding: 3px 10px; border-radius: 5px; overflow: hidden"><span
                                            style="font-size:15px; line-height: 25px; font-weight: normal;">Qty:</span>
                                        {{$booking_package->quantity}}</label>
                                </p>
                            </td>
                        </tr>
                    </table>
                </div>
            </div><!--add-on-tmb end-->
        @endforeach
        <div style="clear:both;"></div>
    </div>
</div>
@endif