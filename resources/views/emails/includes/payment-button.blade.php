<div class="content-section1" style="width: 700px; height: auto; margin: 0px 0px 20px 0px;text-align: center;">
    <a href="https://booking.homes-wiz.com/payment?id={{ $customer_id }}&amount={{ $amount }}&message={{ urldecode($description) }}"
        style="background:#0054a6; color:#fff; padding:15px 20px 10px 15px; border-radius:5px; text-decoration:none; font-weight:bold; font-size:16px;font: normal 13px/18px 'Poppins', sans-serif;"><img
            src="{{ Config::get('values.file_server_url') }}images/payicon.png" width="32"
            height="24">&nbsp;&nbsp;Pay Now</a>
</div>
