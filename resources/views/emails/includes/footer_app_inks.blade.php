<div style="width:700px; text-align:center; margin-top: 30px;">
    <a href="https://play.google.com/store/apps/details?id=com.azinova.homemaid" target="_blank" style="width:130px; display: inline-block; margin-right:5px;"><img
            src="{{ Config::get('values.file_server_url').'images/email/emailer-android.webp?v='.$settings->customer_app_img_version }}" width="100%"
            height="" alt="" style="border-radius: 5px;" /></a>
    <a href="https://apps.apple.com/in/app/homemaids/id868052788" target="_blank" style="width:130px; display: inline-block; margin-left:5px;"><img
            src="{{ Config::get('values.file_server_url').'images/email/emailer-iOS.webp?v='.$settings->customer_app_img_version }}" width="100%" height=""
            alt="" style="border-radius: 5px;" /></a>
</div>