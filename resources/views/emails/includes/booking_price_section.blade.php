<div class="price-section" style="width: 700px; height: auto; margin: 0px auto 0;">

    <div style="width: 700px;">

        <!--NOTE-->
        <!--Customer not select the Ad-On services, need to CHANGE this div margin-top: 95px; to margin-top: 0px; automatically. -->

        <div class="price-details"
            style="width: 330px; margin: 0px 10px 20px; margin-top: 0px; height: auto; float: right;">
            <p
                style="font: bold 20px/25px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 20px 0px 15px 10px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                Price Details</p>

            <table width="320px" border="0" cellpadding="0" cellspacing="0" style="width:320px; float: left;">
                <tr>
                    <td>
                        <p
                            style="font: normal 15px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 0px 15px 10px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                            Payment Method</p>
                    </td>
                    <td>
                        <p
                            style="font: bold 15px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 0px 15px; text-align: right; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                            {{ $booking->payment_method }}</p>
                    </td>
                </tr>

                <tr>
                    <td>
                        <p
                            style="font: normal 15px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 0px 15px 10px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                            Payment Status</p>
                    </td>
                    <td>
                        <p
                            style="font: bold 15px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 0px 15px; text-align: right; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                            {{ $booking->payment_type_id == 1 ? 'Not Paid' : 'Paid' }}
                        </p>
                    </td>
                </tr>
                @if ($booking->_service_amount > 0)
                    <tr>
                        <td>
                            <p
                                style="font: normal 15px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 0px 15px 10px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                                Service Fee</p>
                        </td>
                        <td>
                            <p
                                style="font: bold 15px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 0px 15px; text-align: right; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                                <span style="font-size:12px;">AED</span>
                                {{ number_format($booking->_service_amount, 2, '.', ',') }}
                            </p>
                        </td>
                    </tr>
                @endif
                @if ($booking->_cleaning_materials_amount > 0)
                    <tr>
                        <td>
                            <p
                                style="font: normal 15px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 0px 15px 10px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                                Material Fee</p>
                        </td>
                        <td>
                            <p
                                style="font: bold 15px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 0px 15px; text-align: right; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                                <span style="font-size:12px;">AED</span>
                                {{ number_format($booking->_cleaning_materials_amount, 2, '.', ',') }}
                            </p>
                        </td>
                    </tr>
                @endif
                @if ($booking->_service_addons_amount > 0)
                    <tr>
                        <td>
                            <p
                                style="font: normal 15px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 0px 15px 10px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                                Addons</p>
                        </td>
                        <td>
                            <p
                                style="font: bold 15px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 0px 15px; text-align: right; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                                <span style="font-size:12px;">AED</span>
                                {{ number_format($booking->_service_addons_amount, 2, '.', ',') }}
                            </p>
                        </td>
                    </tr>
                @endif
                @if ($booking->_discount_total > 0)
                    <tr>
                        <td>
                            <p
                                style="font: normal 15px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 0px 15px 10px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                                Discount{{ $booking->_coupon_discount > 0 ? '(' . $booking->coupon_used . ')' : '' }}
                            </p>
                        </td>
                        <td>
                            <p
                                style="font: bold 15px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 0px 15px; text-align: right; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                                <span style="font-size:12px;">AED</span>
                                {{ number_format($booking->_discount_total, 2, '.', ',') }}
                            </p>
                        </td>
                    </tr>
                @endif
                <tr>
                    <td>
                        <p
                            style="font: normal 15px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 0px 15px 10px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                            Net Amount</p>
                    </td>
                    <td>
                        <p
                            style="font: bold 15px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 0px 15px; text-align: right; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                            <span style="font-size:12px;">AED</span>
                            {{ number_format($booking->_taxable_amount, 2, '.', ',') }}
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p
                            style="font: normal 15px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 0px 15px 10px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                            VAT Amount ({{ number_format($booking->_vat_percentage, 1, '.', ',') }}%)</p>
                    </td>
                    <td>
                        <p
                            style="font: bold 15px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 0px 15px; text-align: right; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                            <span style="font-size:12px;">AED</span>
                            {{ number_format($booking->_vat_amount, 2, '.', ',') }}
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p
                            style="font: normal 15px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 0px 15px 10px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                            Total</p>
                    </td>
                    <td>
                        <p
                            style="font: bold 15px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 0px 15px; text-align: right; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                            <span style="font-size:12px;">AED</span>
                            {{ number_format($booking->_taxed_amount, 2, '.', ',') }}
                        </p>
                    </td>
                </tr>
                @if ($booking->_payment_type_charge > 0)
                    <tr>
                        <td>
                            <p
                                style="font: normal 15px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 0px 15px 10px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                                Convenience Fee</p>
                        </td>
                        <td>
                            <p
                                style="font: bold 15px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 0px 15px; text-align: right; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                                <span style="font-size:12px;">AED</span>
                                {{ number_format($booking->_payment_type_charge, 2, '.', ',') }}
                            </p>
                        </td>
                    </tr>
                @endif
                <tr>
                    <td>
                        <p
                            style="font: normal 20px/20px 'Poppins', sans-serif; color: #355fac; display: block; margin: 0px; padding: 0px 0px 15px 10px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                            Total Payable</p>
                    </td>
                    <td>
                        <p
                            style="font: bold 20px/20px 'Poppins', sans-serif; color: #355fac; display: block; margin: 0px; padding: 0px 0px 15px; text-align: right; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                            <span style="font-size:15px;">AED</span>
                            {{ number_format($booking->_total_payable, 2, '.', ',') }}
                        </p>
                    </td>
                </tr>

            </table>

        </div><!--price-details end-->


        <div style="clear:both;"></div>
    </div>
</div>
<div class="section-45" style="width: 700px; height: auto; margin: 0px auto 0;">
    <p
        style="font: normal 13px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px 0px 0px; padding: 10px 20px; text-align: left; background: #eee; border-radius: 5px; overflow: hidden; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
        Feel free to contact us at <a href="#" style="text-decoration: underline;"
            target="_blank">booking@homemaids.ae</a> if you require further assistance.</p>
</div>
