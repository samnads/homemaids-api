<!doctype html>
<html lang="en">
<head>
<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous">
    </script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
        integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://code.jquery.com/jquery-3.7.1.min.js"
        integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
<script src="https://paypage.sandbox.ngenius-payments.com/hosted-sessions/sdk.js"></script>
	<!-- <script src="https://paypage.ngenius-payments.com/hosted-sessions/sdk.js"></script> -->
	<style>
		* {
	border: none;
}
	    .hm-main { width: 500px; margin: 0 auto; float: none;}
		.new-crd-det { min-height: 250px; background: #fff; padding: 0px !important; position: relative;}
		#mount-id iframe { height: 245px !important;}

		#3ds_iframe { height: 100% !important;}
		#3ds_iframe #three_ds_iframe { border: none !important; height: 500px !important;}
		.new-crd-det .loader {width: 100%; height: 100%; position: absolute; left:0px; top: 0px !important; padding-right: 0px; display: block; text-align: center; color: #F00; padding: 25px;}
		.new-crd-det .cssload-container { width: 40px; height: 40px; position: absolute; left: 47%; top: 42%;}
		.new-crd-det .cssload-speeding-wheel {width: 40px; height: 40px;}

		.loader p { color:  #F00;  font-size: 18px; line-height: 20px; text-align: center; margin-top: 80px;}
		.loader p i {font-size: 22px;}
		
		.hm-but { width: 100%; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 20px; font-weight: bold; letter-spacing: 0.3px; color: #FFF; text-align: center; cursor: pointer; display: block; font-weight: bold; padding: 15px 0px; background: #734fbd; border: 0 none; text-transform: uppercase; border-radius: 5px; margin-top: 20px;}
		
		.hm-but-main { padding-left: 0px !important; padding-right: 0px !important}
		.hm-but:hover { background:  #00bfb2;}
		
		.icon-card { padding-right: 5px; font-size:20px; /*-ms-transform: rotate(-25deg);  transform: rotate(-25deg);*/}
		
		.hm-hed-set {}
		
		.lod-pic { width: 40px; height: 40px; margin: 0 auto 15px;}

.lod-pic img { width: 100%; display: block;}

.crd-error { background:#ff9090; color: #FFF; font-size: 14px; font-weight: bold; padding: 10px 20px; border-radius: 5px;  }
		
		
		.hm-but-main01 {padding-left: 0px !important; padding-right: 0px !important}
		.hm-but-main01 .hm-but {background: #00bfb2; text-transform: uppercase; color: #FFF;}
		.hm-but-main01 .hm-but:hover { background: #734fbd;}
		
		.hm-or {width: 45px;height: 45px;border-radius: 50%;font-size: 12px;color: #CCC;margin: 21px auto 15px;border: 1px solid #CCC;text-align: center;line-height: 44px;}
		.text-field-but {
    font-family: Arial, Helvetica, sans-serif;
    color: #FFF;
    font-size: 15px;
    text-align: left;
    background: #13cf56;
    padding: 9px 25px 6px;
    border-radius: 3px;
    border: 0;
    width: 100%;
    cursor: pointer;
    -webkit-appearance: none;
    display: block;
    text-align: center;
    font-weight: bold;
}
		
		@media all and (max-width: 991px) and (min-width: 320px) {
			.hm-main { width: 100%; margin: 0 auto;}
			
		}
		
	</style>
</head>
<body>



	<div class="auto-container hm-main">
		<div class="row clearfix">
			<div class="form-group col-md-12 col-sm-12 col-xs-12 pr-0 pl-0" id="paymountsec" style="margin-top: 10px">
				<div class="col-md-12 col-sm-12 col-xs-12 field-box-main new-crd-det">
                     <div class="col-md-12 col-sm-12 col-xs-12 hm-hed-set">
                          <p><i class="fa fa-credit-card-alt icon-card"></i>Enter Card details</p>
                     </div>
                     <div class="col-md-12 col-sm-12 col-xs-12" id="payerror" style="display: none;">
                          <div class="crd-error">Card details error</div>
                     </div>
					<div class="loader" style="display: none;">
						<p style="text-align: center; color: #F00;"><div class="lod-pic"><img src="{{ Config::get('values.file_server_url') }}images/hm-loader.gif" alt=""></div>
                        <i class="fa fa-exclamation-triangle"></i> &nbsp; Please wait. Do not press refresh or back button</p>
					</div>
					<div id="mount-id"></div>
					<div id="3ds_iframe" style="display: none; height: 500px !important;"></div>
				</div>
				<div class="text-field-main">
                    <input value="PAY NOW"  onclick="createSession()" class="text-field-but" type="button">
                </div>
			</div>
		</div>
	</div>
</body>
</html>
<script>
$( document ).ready(function() 
{
	$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
	// $('.new-crd-det .loader').show();
	const style = {
		main: {
			borderRadius:"0px",
			borderColor:"#F00",
			borderWidth:"0px",
			borderStyle: "solid"} /* the style for the main wrapper div around the card input*/,
		base: {
			borderStyle:"solid",
			borderColor:"#CCC",
			background:"#FFF",	
			borderWidth:"1px"} /* this is the default styles that comes with it */,
		input: {
			color:"#000000",
			borderStyle:"solid",
			borderColor:"#CCC",
			borderRadius:"5px",
			borderWidth:"1px",
			backgroundColor:"#FFF",					
			padding:"6px"} /* custom style options for the input fields 
		/,
		invalid: {} /* custom input invalid styles */
	};

	/* Method call to mount the card input on your website */
	"use strict";
	window.NI.mountCardInput('mount-id' /* the mount id*/, {
		style: style,
		// apiKey: "YzM0MmE0ZjItMGExNC00OGY0LWE5MTUtY2U1ZDAyMzg0Yjc2OmVkZTgwNjIwLWIyNmMtNDZiYy05YzQ0LTViOGRmNDI4ZTIwNA==",
		apiKey: "MmM5ZTljYjEtZTNlNy00ZWUxLWIzYTItZjA4OWRhMDUwMWViOjQ5MzBlYjc2LWIyMmYtNGI1NC04NzRlLTRkMmU3OTgxZjRkZA==",
		// API Key for WEB SDK from the portal
		// outletRef: "beb5e951-90f9-4e17-b87d-e74e43489c6f",
		outletRef: "3072049e-4d9d-4522-b64a-7657c38eaf95",
		// outlet reference from the portal
		onSuccess: function() {
		},
		// Success callback if apiKey validation succeeds
		onFail: function() {
			$("#paymnt_msg").html('<div class="alert alert-danger"><strong>Failed</strong></div>');
			$("#paymnt_msg_btn,#paysec").show();
		}, // Fail callback if apiKey validation fails
		onChangeValidStatus: function (_ref) {
			var isCVVValid = _ref.isCVVValid,
				isExpiryValid = _ref.isExpiryValid,
				isNameValid = _ref.isNameValid,
				isPanValid = _ref.isPanValid;
			    console.log(isCVVValid, isExpiryValid, isNameValid, isPanValid);					 						
		}
	});
});
var sessionId;
function createSession() {
	var payAmount = '<?php echo($input['amount']); ?>';	
	payAmount = (payAmount * 100).toFixed(0);
	var reference_id = '<?php echo($booking->reference_id); ?>';
	var customer_email = '<?php echo($customer->email_address); ?>';
	var customer_mobile = '<?php echo($customer->mobile_number_1); ?>';
	var customer_name = '<?php echo($customer->customer_name); ?>';
	var cust_id = '<?php echo($customer->customer_id); ?>';
	var booking_id = '<?php echo($booking->booking_id); ?>';
	var service_date = '<?php echo($booking->service_start_date); ?>';

	window.NI.generateSessionId().then(function (response) {
		sessionId = response.session_id;
		$("#mount-id,.hm-hed-set,.hm-but-main").hide();
		$('.new-crd-det .loader').show();
		$('#payerror .crd-error').html('');
		$('#payerror').hide();
		
		 $.ajax({
			 url: '{{ url('payment/gateway/checkout/go') }}',
			 type: 'POST',
			 dataType: 'json',
			 data: { sessionId: sessionId, amount:payAmount, f_name:customer_name, l_name:customer_name, email:customer_email, cust_id:cust_id, reference:reference_id,customer_mobile:customer_mobile,booking_id:booking_id,service_date:service_date},
			 cache   : false,
			 success: function (data) {
				$('.new-crd-det .loader').hide();
				$('#mount-id,.hm-hed-set,.hm-but-main').hide();
				$('#3ds_iframe').show();
				"use strict";
				 window.NI.handlePaymentResponse(data, {
					 mountId: '3ds_iframe',
					 style: {
						 width: 500,
						 height: 600,
						 borderWidth: 0,
						 fontSize:'10px'
					 }
				 }).then(function (response) {
					
					 var status = response.status;
					 if (status === window.NI.paymentStates.AUTHORISED || status === window.NI.paymentStates.CAPTURED) {
						window.location.href = "{{ url('payment/gateway/checkout/process') }}/"+booking_id+"/"+data.orderReference;
					 } else if (status === window.NI.paymentStates.FAILED || 
						status === window.NI.paymentStates.THREE_DS_FAILURE) {
						window.location.href = "{{ url('payment/gateway/checkout/process') }}/"+booking_id+"/"+data.orderReference;
					 } else {
						window.location.href = "{{ url('payment/gateway/checkout/process') }}/"+booking_id+"/"+data.orderReference;
					 }
				 });
			 }
		 });
	}).catch(function (error) {
		$("#payerror .crd-error").text(error);
		$('#payerror').show();
	});
}
</script>