<!doctype html>
<html lang="en">

<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <script src="https://code.jquery.com/jquery-3.7.1.min.js"
        integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
</head>

<body>
    <form method="post" id="ccavenue_form" action="{{ Config::get('ccavenue.request_handler_url') }}"
        style="display: none">
        {!! csrf_field() !!}
        <table width="40%" height="100" border='1' align="center">
            <caption>
                <font size="4" color="blue"><b>Integration Kit</b></font>
            </caption>
        </table>
        <table width="40%" height="100" border='1' align="center">
            <tr>
                <td>Parameter Name:</td>
                <td>Parameter Value:</td>
            </tr>
            <tr>
                <td colspan="2"> Compulsory information</td>
            </tr>
            <tr>
                <td>Merchant Id :</td>
                <td><input type="text" name="merchant_id" value="{{ Config::get('ccavenue.merchant_id') }}" /></td>
            </tr>
            <tr>
                <td>Order Id :</td>
                <td><input type="text" name="order_id" value="{{ $payment->payment_id }}" /></td>
            </tr>
            <tr>
                <td>Amount :</td>
                <td><input type="text" name="amount" value="{{ $input['amount'] }}" /></td>
                <!--<td><input type="text" name="amount" value="1" /></td>-->
            </tr>
            <tr>
                <td>Currency :</td>
                <td><input type="text" name="currency" value="AED" /></td>
            </tr>
            <tr>
                <td>Redirect URL :</td>
                <td><input type="text" name="redirect_url"
                        value="{{ Config::get('ccavenue.outstanding_response_handler_url') }}" /></td>
            </tr>
            <tr>
                <td>Cancel URL :</td>
                <td><input type="text" name="cancel_url"
                        value="{{ Config::get('ccavenue.outstanding_response_handler_url') }}" /></td>
            </tr>
            <tr>
                <td>Language :</td>
                <td><input type="text" name="language" value="EN" /></td>
            </tr>
            <tr>
                <td colspan="2">Billing information (optional):</td>
            </tr>
            <tr>
                <td>Billing Name :</td>
                <td><input type="text" name="billing_name"
                        value="{{ ccavenueStripName($customer->customer_name) }}" /></td>
            </tr>
            <tr>
                <td>Billing Address :</td>
                <td><input type="text" name="billing_address" value="Dubai" /></td>
            </tr>
            <tr>
                <td>Billing City :</td>
                <td><input type="text" name="billing_city" value="Dubai" /></td>
            </tr>
            <tr>
                <td>Billing State :</td>
                <td><input type="text" name="billing_state" value="Dubai" /></td>
            </tr>
            <tr>
                <td>Billing Zip :</td>
                <td><input type="text" name="billing_zip" value="118480" /></td>
            </tr>
            <tr>
                <td>Billing Country :</td>
                <td><input type="text" name="billing_country" value="United Arab Emirates" /></td>
            </tr>
            <tr>
                <td>Billing Tel :</td>
                <td><input type="text" name="billing_tel" value="{{ $customer->mobile_number_1 }}" /></td>
            </tr>
            <tr>
                <td>Billing Email :</td>
                <td><input type="text" name="billing_email" value="{{ $customer->email_address }}" /></td>
            </tr>
            <tr>
                <td></td>
                <td><INPUT type="submit" value="CheckOut"></td>
            </tr>
        </table>
    </form>
</body>

</html>
<script>
    $(document).ready(function() {
        $("#ccavenue_form").submit();
    });
</script>
