<!doctype html>
<html lang="en">

<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <script src="https://code.jquery.com/jquery-3.7.1.min.js"
        integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
</head>

<body>
    <form method="post" id="ccavenue_form" action="{{ Config::get('ccavenue.request_handler_url') }}"
        style="display: none">
        {!! csrf_field() !!}
        <table width="40%" height="100" border='1' align="center">
            <caption>
                <font size="4" color="blue"><b>Integration Kit</b></font>
            </caption>
        </table>
        <table width="40%" height="100" border='1' align="center">
            <tr>
                <td>Parameter Name:</td>
                <td>Parameter Value:</td>
            </tr>
            <tr>
                <td colspan="2"> Compulsory information</td>
            </tr>
            <tr>
                <td>Merchant Id :</td>
                <td><input type="text" name="merchant_id" value="{{ Config::get('ccavenue.merchant_id') }}" /></td>
            </tr>
            <tr>
                <td>Order Id :</td>
                <td><input type="text" name="order_id" value="{{ $booking->reference_id }}" /></td>
            </tr>
            <tr>
                <td>Amount :</td>
                <td><input type="text" name="amount" value="{{ $input['amount'] }}" /></td>
                <!--<td><input type="text" name="amount" value="1" /></td>-->
            </tr>
            <tr>
                <td>Currency :</td>
                <td><input type="text" name="currency" value="AED" /></td>
            </tr>
            <tr>
                <td>Redirect URL :</td>
                <td><input type="text" name="redirect_url"
                        value="{{ Config::get('ccavenue.response_handler_url') }}" /></td>
            </tr>
            <tr>
                <td>Cancel URL :</td>
                <td><input type="text" name="cancel_url"
                        value="{{ Config::get('ccavenue.response_handler_url') }}" /></td>
            </tr>
            <tr>
                <td>Language :</td>
                <td><input type="text" name="language" value="EN" /></td>
            </tr>
            <tr>
                <td colspan="2">Billing information (optional):</td>
            </tr>
            <tr>
                <td>Billing Name :</td>
                <td><input type="text" name="billing_name"
                        value="{{ ccavenueStripName($customer->customer_name) }}" /></td>
            </tr>
            <tr>
                <td>Billing Address :</td>
                <td><input type="text" name="billing_address" value="Dubai" /></td>
            </tr>
            <tr>
                <td>Billing City :</td>
                <td><input type="text" name="billing_city" value="Dubai" /></td>
            </tr>
            <tr>
                <td>Billing State :</td>
                <td><input type="text" name="billing_state" value="Dubai" /></td>
            </tr>
            <tr>
                <td>Billing Zip :</td>
                <td><input type="text" name="billing_zip" value="118480" /></td>
            </tr>
            <tr>
                <td>Billing Country :</td>
                <td><input type="text" name="billing_country" value="United Arab Emirates" /></td>
            </tr>
            <tr>
                <td>Billing Tel :</td>
                <td><input type="text" name="billing_tel" value="{{ $customer->mobile_number_1 }}" /></td>
            </tr>
            <tr>
                <td>Billing Email :</td>
                <td><input type="text" name="billing_email" value="{{ $customer->email_address }}" /></td>
            </tr>
            <tr>
                <td></td>
                <td><INPUT type="submit" value="CheckOut"></td>
            </tr>
        </table>
    </form>
</body>

</html>
<script>
    $(document).ready(function() {
        $("#ccavenue_form").submit();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        // $('.new-crd-det .loader').show();
        const style = {
            main: {
                borderRadius: "0px",
                borderColor: "#F00",
                borderWidth: "0px",
                borderStyle: "solid"
            } /* the style for the main wrapper div around the card input*/ ,
            base: {
                borderStyle: "solid",
                borderColor: "#CCC",
                background: "#FFF",
                borderWidth: "1px"
            } /* this is the default styles that comes with it */ ,
            input: {
                color: "#000000",
                borderStyle: "solid",
                borderColor: "#CCC",
                borderRadius: "5px",
                borderWidth: "1px",
                backgroundColor: "#FFF",
                padding: "6px"
            }
            /* custom style options for the input fields 
		/,
		invalid: {} /* custom input invalid styles */
        };

        /* Method call to mount the card input on your website */
        "use strict";
        window.NI.mountCardInput('mount-id' /* the mount id*/ , {
            style: style,
            // apiKey: "YzM0MmE0ZjItMGExNC00OGY0LWE5MTUtY2U1ZDAyMzg0Yjc2OmVkZTgwNjIwLWIyNmMtNDZiYy05YzQ0LTViOGRmNDI4ZTIwNA==",
            apiKey: "MmM5ZTljYjEtZTNlNy00ZWUxLWIzYTItZjA4OWRhMDUwMWViOjQ5MzBlYjc2LWIyMmYtNGI1NC04NzRlLTRkMmU3OTgxZjRkZA==",
            // API Key for WEB SDK from the portal
            // outletRef: "beb5e951-90f9-4e17-b87d-e74e43489c6f",
            outletRef: "3072049e-4d9d-4522-b64a-7657c38eaf95",
            // outlet reference from the portal
            onSuccess: function() {},
            // Success callback if apiKey validation succeeds
            onFail: function() {
                $("#paymnt_msg").html(
                    '<div class="alert alert-danger"><strong>Failed</strong></div>');
                $("#paymnt_msg_btn,#paysec").show();
            }, // Fail callback if apiKey validation fails
            onChangeValidStatus: function(_ref) {
                var isCVVValid = _ref.isCVVValid,
                    isExpiryValid = _ref.isExpiryValid,
                    isNameValid = _ref.isNameValid,
                    isPanValid = _ref.isPanValid;
                console.log(isCVVValid, isExpiryValid, isNameValid, isPanValid);
            }
        });
    });
    var sessionId;

    function createSession() {
        var payAmount = '<?php echo $input['amount']; ?>';
        payAmount = (payAmount * 100).toFixed(0);
        var reference_id = '<?php echo $booking->reference_id; ?>';
        var customer_email = '<?php echo $customer->email_address; ?>';
        var customer_mobile = '<?php echo $customer->mobile_number_1; ?>';
        var customer_name = '<?php echo $customer->customer_name; ?>';
        var cust_id = '<?php echo $customer->customer_id; ?>';
        var booking_id = '<?php echo $booking->booking_id; ?>';
        var service_date = '<?php echo $booking->service_start_date; ?>';

        window.NI.generateSessionId().then(function(response) {
            sessionId = response.session_id;
            $("#mount-id,.hm-hed-set,.hm-but-main").hide();
            $('.new-crd-det .loader').show();
            $('#payerror .crd-error').html('');
            $('#payerror').hide();

            $.ajax({
                url: '{{ url('payment/gateway/checkout/go') }}',
                type: 'POST',
                dataType: 'json',
                data: {
                    sessionId: sessionId,
                    amount: payAmount,
                    f_name: customer_name,
                    l_name: customer_name,
                    email: customer_email,
                    cust_id: cust_id,
                    reference: reference_id,
                    customer_mobile: customer_mobile,
                    booking_id: booking_id,
                    service_date: service_date
                },
                cache: false,
                success: function(data) {
                    $('.new-crd-det .loader').hide();
                    $('#mount-id,.hm-hed-set,.hm-but-main').hide();
                    $('#3ds_iframe').show();
                    "use strict";
                    window.NI.handlePaymentResponse(data, {
                        mountId: '3ds_iframe',
                        style: {
                            width: 500,
                            height: 600,
                            borderWidth: 0,
                            fontSize: '10px'
                        }
                    }).then(function(response) {

                        var status = response.status;
                        if (status === window.NI.paymentStates.AUTHORISED || status ===
                            window.NI.paymentStates.CAPTURED) {
                            window.location.href =
                                "{{ url('payment/gateway/checkout/process') }}/" +
                                booking_id + "/" + data.orderReference;
                        } else if (status === window.NI.paymentStates.FAILED ||
                            status === window.NI.paymentStates.THREE_DS_FAILURE) {
                            window.location.href =
                                "{{ url('payment/gateway/checkout/process') }}/" +
                                booking_id + "/" + data.orderReference;
                        } else {
                            window.location.href =
                                "{{ url('payment/gateway/checkout/process') }}/" +
                                booking_id + "/" + data.orderReference;
                        }
                    });
                }
            });
        }).catch(function(error) {
            $("#payerror .crd-error").text(error);
            $('#payerror').show();
        });
    }
</script>
