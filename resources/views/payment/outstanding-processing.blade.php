<html>

<head>
    <title>Verifying...</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous">
    </script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
        integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js"
        integrity="sha512-3gJwYpMe3QewGELv8k/BX9vcqhryRdzRMxVfq6ngyWXwo03GFEzjsUm8Q7RZcHPHksttq7/GFoxjCVUjkjvPdw=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
</head>

<body class="d-flex align-items-center">
    <div class="container text-center">
        <div class="row">
            <div class="col">
                <div id="loading" class="mb-5">
                    <div class="spinner-border text-info" role="status">
                        <span class="visually-hidden">Loading...</span>
                    </div>
                </div>
                <p class="display-1">Please wait...</p>
                <p class="display-6 mb-5">Do not press refresh or back button !</p>
                <p class="display-6 text-primary" id="response_message">&nbsp;</p>
                <p class="display-6 text-success" id="response_message_success">&nbsp;</p>
                <p class="display-6 text-danger" id="response_message_error">&nbsp;</p>
            </div>
        </div>
    </div>
    <script>
        function verifyPayment() {
            $.ajax({
                url: '{{ url('api/customer/payment/ccavenue/outstanding-verify') }}',
                method: "POST",
                cache: false,
                dataType: "json",
                data: {
                    'status': '{{ request()->status }}',
                    'merchant_reference': '{{ request()->id }}',
                    'order': '{{ request()->order }}',
                    'amount': '{{ request()->amount }}'
                },
                success: function(data) {
                    // console.log(data);
                    // return false;
                    $('#response_message').html("&nbsp;");
                    if (data.result.status == "success") {
                        var status = data.result.data;
                        var stat = data.result.data.transaction_message;
                        // console.log(status);
                        //return;
                        if (stat == "success") {
                            $('#response_message_success').html(status.status);
                            window.location.href = '{{ url('api/customer/payment/payfort/success') }}?payment_method=Card&status=Waiting';
                        } else if (status.fort_id == null) {
                            $('#response_message_error').html(status.status);
                            setTimeout(function() {
                                window.location.href =
                                    '{{ url('api/customer/payment/payfort/failed') }}?payment_method=Card&status=Payment Pending';
                            }, 3000);
                        }
                    } else {
                        $('#response_message_error').html(data.result.message);
                        verifyPayment();
                    }
                },
                error: function(error) {
                    console.log(error);
                    //$('#response_message_error').html(error.responseText);
                    $('#response_message').html("Still Processing...");
                    verifyPayment();
                }
            });
        }
        verifyPayment();
        $(function() {
            // setInterval(verifyPayment, 3000);
        });
    </script>
</body>

</html>
