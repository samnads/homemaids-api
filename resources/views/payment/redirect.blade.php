<?php
$requestParams = [
    'command' => 'PURCHASE',
    'access_code' => Config::get('values.payfort_access_code'),
    'merchant_identifier' => Config::get('values.payfort_merchant_identifier'),
    'amount' => $data['amount'] * 100,
    'currency' => Config::get('values.currency_code'),
    'language' => strtolower(Config::get('values.language_code')),
    'customer_email' => @$data['customer']->email_address ?: 'test@example.com',
    'order_description' => $data['booking_reference'],
    'merchant_reference' => $data['booking_reference'],
    'merchant_extra' => $data['booking_reference'],
    //'eci' => 'RECURRING',
    //'card_number' => '4005550000000001',
    //'expiry_date' => '2312',
    //'card_security_code' => '123',
    //'token_name' => '655b9aecd39a4c85a4c6c2c4d80f4c03',
    'return_url' => url('api/customer/payment/payfort/processing'),
];
ksort($requestParams);
$hash_value = Config::get('values.payfort_sha_request_phrase');
$excludes = ['card_number', 'expiry_date', 'card_security_code'];
foreach ($requestParams as $name => $value) {
    if (!in_array($name, $excludes) && $value != null) {
        $hash_value .= $name . '=' . $value;
    }
}
$hash_value .= Config::get('values.payfort_sha_request_phrase');
$requestParams['signature'] = hash('sha256', $hash_value);
?>
<html>

<head>
    <title>Redirection</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous">
    </script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
        integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js"
        integrity="sha512-3gJwYpMe3QewGELv8k/BX9vcqhryRdzRMxVfq6ngyWXwo03GFEzjsUm8Q7RZcHPHksttq7/GFoxjCVUjkjvPdw=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
</head>

<body class="d-flex align-items-center">
    <!--<b>Hash Value :</b><br><code>{{ $hash_value }}</code>-->
    <div class="container text-center">
        <div class="col">
            <div id="loading" style="display:none">
                <p class="display-3 mb-5">Please wait...</p>
                <div class="spinner-border text-info" role="status">
                    <span class="visually-hidden">Loading...</span>
                </div>
            </div>
            <form method="post" action="{{ Config::get('values.payfort_payment_page_url') }}" id="pay-now-form">
                @foreach ($requestParams as $name => $value)
                    <input type="hidden" name="{{ $name }}" value="{{ $value }}">
                @endforeach
                <p class="display-3 mb-5">Click on "Pay Now" button to pay <code class="display-3"><strong
                            class="display-1">{{ number_format($data['amount'], 2) }}</strong></code>
                    {{ Config::get('values.currency_code') }}.</p>
                <button type="submit" id="pay-now-btn" class="btn btn-lg btn-info"><strong class="p-5 pt-5">PAY
                        NOW</strong></button>
            </form>
        </div>
    </div>
    <script>
        $(function() {
            //$('#form1').submit();
            $("#pay-now-btn").click(function() {
                $("#pay-now-form").hide();
                $("#loading").show();
            });
        });
    </script>
</body>

</html>
