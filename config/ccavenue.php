<?php
return [
    'merchant_id' => env('CCAVENUE_MERCHANT_ID'),
    'request_handler_url' => env('CCAVENUE_REQUEST_HANDLER_URL'),
    'response_handler_url' => env('CCAVENUE_RESPONSE_HANDLER_URL'),
    'outstanding_response_handler_url' => env('CCAVENUE_RESPONSE_HANDLER_OUTSTANDING_URL'),
]
    ?>