<?php
use App\Models\Booking;
use App\Models\CouponCode;
use App\Models\Customer;
use App\Models\Area;
use App\Models\ServiceTypeCleaningMaterial;
use App\Models\CustomerPayments;
use App\Models\OnlinePayment;
use App\Models\CustomerNotifications;
use App\Models\RushSlotCharges;
use App\Models\ServiceAddons;
use App\Models\ServiceTypeCharges;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Response as Response;

function toggleDebug($debug = null)
{
    return false;
    // overide default
    if (isset($debug)) {
        return $debug;
    }
    // default true for local testing
    $locals = array(
        '127.0.0.1',
        'localhost',
        '::1',
    );
    if (in_array($_SERVER['REMOTE_ADDR'], $locals)) {
        return true;
    }
    // url based checking
    /*$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
    if (strpos($url, '-demo') !== false) {
    return true;
    }*/
    return false;
}
function isLive()
{
    if (strpos(url()->full(), '/homemaids-api/') !== FALSE) {
        return true;
    }
    return false;
}
function isDemo()
{
    if (strpos(url()->full(), '/homemaids-api-demo/') !== FALSE) {
        return true;
    }
    return false;
}
function customerResponseJsonConstants()
{
    return JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK;
}
function getHourlyRateByHourAndMaterial($working_hours, $cleaning_material)
{
    $price_settings = DB::table('price_settings as ps')
        ->select(
            'ps.id',
            'ps.hour',
            'ps.price_c',
            'ps.price_n'
        )
        ->where(function ($query) use ($working_hours) {
            $query->where([['ps.hour', '=', $working_hours]]);
            $query->orWhere([['ps.hour', '<', $working_hours]]);
        })
        ->orderBy('ps.hour', 'DESC')
        ->first();
    if ($cleaning_material == 'Y') {
        return $price_settings->price_c;
    } else {
        return $price_settings->price_n;
    }
}
function applyCouponNew($input, $amount, $data)
{
    /************************************************** */
    $coupon = DB::table('coupon_code as cc')
        ->select(
            'cc.*',
            'f.name as frequency',
            DB::raw('IFNULL(st.customer_app_service_type_name,st.service_type_name) as service_type_name'),
        )
        ->leftJoin('frequencies as f', 'cc.coupon_id', 'f.coupon_id')
        ->leftJoin('service_types as st', 'cc.service_id', 'st.service_type_id')
        ->where(['cc.coupon_name' => $input['coupon_code']])
        ->where(['cc.status' => 1])
        ->where('cc.percentage', '>', 0)
        ->first();
    /************************************************** */
    if ($coupon) {
        if (@$input['frequency']) {
            $frequency = DB::table('frequencies as f')
                ->select(
                    'f.*',
                )
                ->where(['f.code' => $input['frequency']])
                ->first();
        }
        /************************************************** */
        // frequency coupon found
        if (@$coupon->type == 'F') {
            // frequency coupon code
            if (@$frequency->coupon_id != $coupon->coupon_id) {
                return 'Coupon valid for ' . $coupon->frequency . ' bookings only.';
            }
        } else {
            //dd($input['id']);
            $booking = Booking::where([['customer_id', '=', $input['id']], ['booking_status', '=', 1]])->first();
            $input['date'] = @$input['date'] ?: date('Y-m-d');
            $input['hours'] = @$input['hours'] ?: 2;
            if ($input['date'] > $coupon->expiry_date) {
                // expired coupon
                return 'Coupon expired on ' . $coupon->expiry_date;
            } else if ($booking && $coupon->coupon_type == "FT") {
                // not first booking
                return 'Coupon is already used.';
            } else if ($input['hours'] < $coupon->min_hrs) {
                // min hours check failed
                return 'Coupon valid for minimum of ' . $coupon->min_hrs . ' working hours only.';
            } else if ($coupon->service_id > 0 && $input['service_type_id'] != $coupon->service_id) {
                // min hours check failed
                return 'Coupon valid for ' . $coupon->service_type_name . ' only.';
            }
            $valid_week_days = explode(',', $coupon->valid_week_day); // 0 - SUNDAY
            $week_day = Carbon::createFromFormat('Y-m-d', $input['date'])->format('w'); // 0 - SUNDAY
            if (!in_array($week_day, $valid_week_days)) {
                return 'Coupon not valid for ' . weekArray()[$week_day] . '.';
            }
        }
        // valid coupon
        if ($coupon->discount_type == 0) {
            // percentage
            return $amount * ($coupon->percentage / 100);
        } else if ($coupon->discount_type == 1) {
            // flat rate
            if ($coupon->offer_type == "P") {
                // per hour rate for service
                $service_fee = ($coupon->percentage * $input['hours'] * $input['professionals_count']) + $data['cleaning_materials_amount'];
                $discount = $amount - $service_fee;
                return $discount;
            }
            return $coupon->percentage;
        }
    } else {
        // not found
        return "Coupon code '" . $input['coupon_code'] . "' is invalid.";
    }
}
function applyCoupon($coupon_id, $customer_id, $date, $hours, $amount)
{
    $coupon = CouponCode::where([['coupon_id', '=', $coupon_id], ['status', '=', 1]])->first();
    if ($coupon) {
        $booking = Booking::where([['customer_id', '=', $customer_id], ['booking_status', '=', 1]])->first();
        if ($date > $coupon->expiry_date) {
            // expired coupon
            return 0;
        } else if ($booking && $coupon->coupon_type == "FT") {
            // not first booking
            return 0;
        } else if ($hours < $coupon->min_hrs) {
            // min hours check failed
            return 0;
        }
        $valid_week_days = explode(',', $coupon->valid_week_day);
        $week_day = Carbon::createFromFormat('Y-m-d', $date)->format('N') - 1;
        if (!in_array($week_day, $valid_week_days)) {
            return 0;
        }
        // valid coupon
        if ($coupon->discount_type == 0) {
            // percentage
            return $amount * ($coupon->percentage / 100);
        } else {
            // flat rate
            return $coupon->percentage;
        }
    } else {
        // not found
        return 0;
    }
}
function get_busy_bookings($booking, $except_booking_ids)
{
    /*************************************** */
    $service_week_day = $booking->service_week_day;
    $service_start_date = $booking->service_start_date;
    $service_actual_end_date = $booking->service_actual_end_date;
    $booking_type = $booking->booking_type;
    $time_from = $booking->time_from;
    $time_to = $booking->time_to;
    $where_in_maids = @$booking->where_in_maids;
    /******************************************************************** */
    $data = DB::table('bookings as b')
        ->select(
            'b.booking_id',
            'b.booking_type',
            'b.time_from',
            'b.time_to',
            'm.maid_id',
            'm.maid_name',
            'm.maid_gender',
            'm.maid_nationality',
            'bd.booking_delete_id',
            DB::raw('ROUND(DATEDIFF(b.service_start_date, "' . $service_start_date . '")/7) as service_start_date_week_difference'),
            'caz.zone_id as booking_zone_id'
        )
        ->leftJoin('maids as m', 'b.maid_id', 'm.maid_id')
        ->leftJoin('customer_addresses as bca', 'b.customer_address_id', 'bca.customer_address_id')
        ->leftJoin('areas as caa', 'bca.area_id', 'caa.area_id')
        ->leftJoin('zones as caz', 'caa.zone_id', 'caz.zone_id')
        ->leftJoin('booking_deletes as bd', 'b.booking_id', 'bd.booking_id')
        ->whereNotIn('b.booking_id', $except_booking_ids) // slow query :(
        ->where([['m.maid_status', '=', 1], ['b.booking_status', '!=', 2], ['b.service_week_day', '=', $service_week_day]]);
    if ($where_in_maids) {
        $data->whereIn('m.maid_id', $where_in_maids);
    }
    if ($booking->service_end == '0') {
        $data->where(function ($query) use ($service_start_date, $service_actual_end_date) {
            // this is optimized at it's best no more change needed
            $query->where([['b.service_end', '=', 0]])
                ->orWhere([['b.service_end', '=', 1], ['b.service_actual_end_date', '>', $service_start_date]]);
        });
    } else {
        $data->where(function ($query) use ($service_start_date, $service_actual_end_date) {
            $query
                ->where([['b.service_end', '=', 0], ['b.service_start_date', '<=', $service_actual_end_date]])
                ->orWhere([['b.service_end', '=', 1], ['b.service_actual_end_date', '>', $service_start_date], ['b.service_start_date', '<', $service_start_date]])
                ->orWhere([['b.service_end', '=', 1], ['b.service_actual_end_date', '>', $service_start_date], ['b.service_actual_end_date', '<=', $service_start_date]])
                ->orWhere([['b.service_end', '=', 1], ['b.service_start_date', '>', $service_start_date], ['b.service_actual_end_date', '<', $service_actual_end_date]])
                ->orWhere([['b.service_end', '=', 1], ['b.service_start_date', '=', $service_start_date]])
                ->orWhere([['b.service_end', '=', 1], ['b.service_actual_end_date', '=', $service_actual_end_date]]);
        });
    }
    $data->where(function ($query) use ($time_from, $time_to) {
        $query->where([['b.time_from', '=', $time_from]])
            ->orWhere([['b.time_from', '=', $time_from], ['b.time_to', '=', $time_to]])
            ->orWhere([['b.time_from', '>', $time_from], ['b.time_to', '<=', $time_to]])
            ->orWhere([['b.time_from', '<', $time_from], ['b.time_to', '=', $time_to]])
            ->orWhere([['b.time_from', '<', $time_to], ['b.time_to', '>', $time_to]])
            ->orWhere([['b.time_from', '<', $time_from], ['b.time_to', '>', $time_from]])
            ->orWhere([['b.time_from', '<', $time_from], ['b.time_to', '>', $time_to]]);
    });
    return $data->get();
}
function get_busy_bookings_by_date($booking, $except_booking_ids)
{
    /*************************************** */
    $service_week_day = $booking->service_week_day;
    $service_start_date = $booking->service_start_date;
    $service_actual_end_date = $booking->service_actual_end_date;
    $booking_type = $booking->booking_type;
    /******************************************************************** */
    $data = DB::table('bookings as b')
        ->select(
            'b.booking_id',
            'b.booking_type',
            'b.time_from',
            'b.time_to',
            'm.maid_id',
            'm.maid_name',
            'm.maid_gender',
            'm.maid_nationality',
            'bd.booking_delete_id',
            DB::raw('ROUND(DATEDIFF(b.service_start_date, "' . $service_start_date . '")/7) as service_start_date_week_difference'),
            'caz.zone_id as booking_zone_id'
        )
        ->leftJoin('maids as m', 'b.maid_id', 'm.maid_id')
        ->leftJoin('customer_addresses as bca', 'b.customer_address_id', 'bca.customer_address_id')
        ->leftJoin('areas as caa', 'bca.area_id', 'caa.area_id')
        ->leftJoin('zones as caz', 'caa.zone_id', 'caz.zone_id')
        ->leftJoin('booking_deletes as bd', 'b.booking_id', 'bd.booking_id')
        ->whereNotIn('b.booking_id', $except_booking_ids) // slow query :(
        ->where([['m.maid_status', '=', 1], ['b.booking_status', '!=', 2], ['b.service_week_day', '=', $service_week_day]]);
    if ($booking->service_end == '0') {
        $data->where(function ($query) use ($service_start_date, $service_actual_end_date) {
            // this is optimized at it's best no more change needed
            $query->where([['b.service_end', '=', 0]])
                ->orWhere([['b.service_end', '=', 1], ['b.service_actual_end_date', '>', $service_start_date]]);
        });
    } else {
        $data->where(function ($query) use ($service_start_date, $service_actual_end_date) {
            $query
                ->where([['b.service_end', '=', 0], ['b.service_start_date', '<=', $service_actual_end_date]])
                ->orWhere([['b.service_end', '=', 1], ['b.service_actual_end_date', '>', $service_start_date], ['b.service_start_date', '<', $service_start_date]])
                ->orWhere([['b.service_end', '=', 1], ['b.service_actual_end_date', '>', $service_start_date], ['b.service_actual_end_date', '<=', $service_start_date]])
                ->orWhere([['b.service_end', '=', 1], ['b.service_start_date', '>', $service_start_date], ['b.service_actual_end_date', '<', $service_actual_end_date]])
                ->orWhere([['b.service_end', '=', 1], ['b.service_start_date', '=', $service_start_date]])
                ->orWhere([['b.service_end', '=', 1], ['b.service_actual_end_date', '=', $service_actual_end_date]]);
        });
    }
    return $data->get();
}
function getActiveMaids()
{
    return DB::table('maids as m')
        ->select(
            'm.maid_id',
        )
        ->where(['m.maid_status' => 1])
        ->get();
}
function getActiveOtherCompanyMaids()
{
    return DB::table('maids as m')
        ->select(
            'm.maid_id',
        )
        ->where(['m.maid_status' => 1])->get();
    /*->where(function ($query) {
$query->where([['m.maid_is_matic', "=", 1]])
->orWhere([['m.maid_is_justmaid', "=", 1]])
->orWhere([['m.maid_is_helpling', "=", 1]])
->orWhere([['m.maid_for_justmop', "=", 1]])
->orWhere([['m.maid_is_servicemarket', "=", 1]]);
})*/

}
function getLeaveMaidsByDate($date)
{
    return DB::table('maid_leave as ml')
        ->leftJoin('maids as m', 'ml.maid_id', 'm.maid_id')
        ->select(
            'm.maid_id',
            'ml.leave_id',
        )
        ->where([['ml.leave_date', "=", $date], ['ml.leave_status', "=", 1], ['m.maid_status', "=", 1]])
        ->get();
}
function is_time_slot_available($date, $time_from, $time_to)
{
    // date format - Y-m-d
    // time format - H:i:s
    $blocked_slot = DB::table('booking_slots as bs')
        ->select(
            'bs.*',
        )
        ->where(['bs.date' => $date, 'bs.status' => 1])
        ->where(function ($query) use ($time_from, $time_to) { // matching more fields
            $query->where([['bs.from_time', '=', $time_from]])
                ->orWhere([['bs.from_time', '<', $time_from], ['bs.to_time', '>', $time_to]])
                ->orWhere([['bs.from_time', '<', $time_to], ['bs.from_time', '>', $time_from]])
                ->orWhere([['bs.to_time', '>', $time_from], ['bs.to_time', '<', $time_to]]);
        })->first();
    if ($blocked_slot) {
        return "Sorry, time slots not available between " . Carbon::createFromFormat('H:i:s', $blocked_slot->from_time)->format('h:i A') . " - " . Carbon::createFromFormat('H:i:s', $blocked_slot->to_time)->format('h:i A') . " on " . Carbon::parse($date)->format('d/m/Y') . ".";
    }
    return true;
}
function isCustomerEligibleForBooking($customer_id)
{
    $customer = Customer::where(['customer_id' => $customer_id])->first();
    if ($customer->is_flag == 'Y') {
        return "Sorry, you're not allowed for booking, contact us.";
    }
    return true;
}
function isCustomerHaveRequiredData($customer_id, $customer = null)
{
    /**
     * this will check the customer profile have all required data filled or not
     * so the admin panel will not in trouble finding the name or email not filled errors :/
     *
     * this will return an object if any data is not filled
     * will return an empty object if everything is ok
     *
     */
    if ($customer == null) {
        $customer = Customer::where(['customer_id' => $customer_id])->first();
    }
    $error = new stdClass();
    if ($customer->customer_name == '') {
        // name is not entered yet
        $error->title = 'Data required';
        $error->type = 'warning';
        $error->message = 'Please update name in your profile.';
        return $error;
    } else if ($customer->email_address == '') {
        // email is not added
        $error->title = 'Data required';
        $error->type = 'warning';
        $error->message = 'Please update email in your profile.';
        return $error;
    }
    // everything is ok
    return $error;
}
function isAreaAllowedForBooking($area_id)
{
    $area = Area::where(['area_id' => $area_id])->first();
    if ($area->web_status == 0) {
        return "Sorry, your address area is not available for booking.";
    }
    return true;
}
function addCustomerNotification($notify)
{
    $notification = new CustomerNotifications();
    $notification->customer_id = @$notify->customer_id;
    $notification->booking_id = @$notify->booking_id;
    $notification->service_date = @$notify->service_date;
    $notification->content = $notify->content;
    $notification->save();
}
function time_elapsed_string($notify_time)
{
    // Code by : [Samnad S]
    if (date('Y') > Carbon::parse($notify_time)->format('Y')) {
        // less than this year
        $string = Carbon::parse($notify_time)->format('Y M d \a\t h:i A');
    } else if (Carbon::now() > Carbon::parse($notify_time)->addWeeks(1)) {
        // greater than 1 week
        $string = Carbon::parse($notify_time)->format('M d \a\t h:i A');
    } else if (Carbon::now() > Carbon::parse($notify_time)->addDays(2)) {
        // than 1 day
        $string = Carbon::parse($notify_time)->format('D \a\t h:i A');
    } else if (Carbon::now()->format('Y-m-d') == Carbon::parse($notify_time)->addDays(1)->format('Y-m-d')) {
        // 1 day before
        $string = Carbon::parse($notify_time)->format('\Y\e\s\t\e\r\d\a\y \a\t h:i A');
    } else if (Carbon::now()->format('Y-m-d H:i:s') >= Carbon::parse($notify_time)->addMinutes(60)->format('Y-m-d H:i:s') && Carbon::now()->format('Y-m-d H:i:s') < Carbon::parse($notify_time)->addMinutes(120)->format('Y-m-d H:i:s')) {
        // than 1 hour < 2 hour
        $string = Carbon::parse($notify_time)->format('1 \h\o\u\r \a\g\o');
    } else if (Carbon::now()->format('Y-m-d H:i:s') > Carbon::parse($notify_time)->addMinutes(120)) {
        // than 1 hour < 2 hour
        $hours = round(Carbon::now()->diff(Carbon::parse($notify_time))->format('%H'));
        $string = Carbon::parse($notify_time)->format($hours . ' \h\o\u\r\s \a\g\o');
    } else if (Carbon::now() >= Carbon::parse($notify_time)->addMinutes(1) && Carbon::now() < Carbon::parse($notify_time)->addMinutes(2)) {
        // 1 minute
        $string = Carbon::parse($notify_time)->format('1 \m\i\n\u\t\e \a\g\o');
    } else if (Carbon::now() > Carbon::parse($notify_time)->addMinutes(2)) {
        // than 1 miutte
        $minutes = round(Carbon::now()->diff(Carbon::parse($notify_time))->format('%i'));
        $string = Carbon::parse($notify_time)->format($minutes . ' \m\i\n\u\t\e\s \a\g\o');
    } else if (Carbon::now() > Carbon::parse($notify_time)->addSeconds(10)) {
        // than 10 seconds
        $seconds = round(Carbon::now()->diff(Carbon::parse($notify_time))->format('%s'));
        $string = Carbon::parse($notify_time)->format($seconds . ' \s\e\c\o\n\d\s \a\g\o');
    } else {
        // less than 10 seconds
        $string = 'Just now';
    }
    return $string;
}
function weekArray()
{
    // 0 for sunday
    return array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
}
function debug_mobiles()
{
    // for hijack real otp for testing purpose
    // add yours for debug
    return array(
        '979797979',
        '501234566',
        '9191919192',
        '501234556',
        '977777777'
    );
}

function rushSlotsByDate($date)
{
    $rush_slot_charges = RushSlotCharges::select('id', 'date_from as debug_date_from', 'date_to as debug_date_to', 'time', 'extra_charge')
        //->where([['date_from', '>=', $date], ['date_to', '<=', $date]])
        ->whereRaw("? BETWEEN `date_from` AND `date_to`", [$date])
        ->orderBy('time', 'ASC')
        ->get();
    foreach ($rush_slot_charges as $key => $rush_slot) {
        $rush_slot_charges[$key]->time = Carbon::createFromFormat('H:i:s', $rush_slot->time)->format('H:i');
        $rush_slot_charges[$key] = $rush_slot;
    }
    return $rush_slot_charges;
}
function calculate($input)
{
    try {
        $debug = toggleDebug(); // pass boolean to overide default
        /*******************************************************************
         * Author : Samnad. S
         * Created at : 02-11-2023
         * Last updated at : 22-04-2024
         * Last updated by : Unknown
         * Usage : APP side calculation (api) and Backend side calculation (for save booking data)
         * !!! Please note that renaming or removing $data[] keys will crash the app side calculation and api side booking calculation
         *******************************************************************/
        $data['no_of_weeks'] = 0;
        $data['amount_devide_by'] = @$input['professionals_count'] ?: 1;
        if (@$input['subscription_package_id']) {
            $subscription_package = DB::table('package as p')
                ->select(
                    'p.*',
                )
                ->where([['p.package_id', '=', $input['subscription_package_id']]])
                ->first();
            $input['service_type_id'] = $subscription_package->service_type_id;
            $input['hours'] = $subscription_package->working_hours;
            $input['frequency'] = 'WE';
            $data['no_of_weeks'] = $subscription_package->no_of_bookings;
            $input['professionals_count'] = 1;
            /****************************************** */
            if ($subscription_package->tax_method == "I") {
                // amount is tax inclusive
                $tax_amount = ($subscription_package->amount * Config::get('values.vat_percentage')) / (100 + Config::get('values.vat_percentage'));
                $subscription_package->amount = $subscription_package->amount - $tax_amount;
            } else {
                // amount is tax exclusive
            }
        }
        /************************************************************* */
        $service_type = DB::table('service_types as st')
            ->select(
                'st.service_type_id',
                'st.service_type_model_id',
                'st.service_type_name',
            )
            ->where([['st.service_type_id', '=', $input['service_type_id']]])
            ->first();
        /************************************************************* */
        if (@$input['subscription_package_id']) {
            // change data
            $data['hours'] = $input['hours'];
            $data['amount_devide_by'] = $subscription_package->no_of_bookings;
        } else if ($service_type->service_type_model_id == 1) {
            // normal model
            $data['amount_devide_by'] = $input['professionals_count'];
        } else if ($service_type->service_type_model_id == 2) {
            // package model
            $input['hours'] = 2; // default
            $input['frequency'] = null;
            $input['professionals_count'] = 1;
            $data['amount_devide_by'] = 1;
            if (@$input['packages']) {
                $time_in_sec = 0;
                $professionals_count = 0;
                $package_ids = array_column($input['packages'], 'package_id');
                $packages = DB::table('building_type_room_packages as btrp')
                    ->select(
                        'btrp.id as package_id',
                        'btrp.service_type_id as service_type_id',
                        'btrp.service_time',
                        'btrp.no_of_maids',
                        'btrp.actual_total_amount',
                        'btrp.total_amount',
                    )
                    ->whereIn('id', $package_ids)
                    ->where([['btrp.service_type_id', "=", $input['service_type_id']], ['deleted_at', "=", null]])
                    ->get()
                    ->toArray();
                foreach ($packages as $key => $package) {
                    $index = array_search($package->package_id, array_column($input['packages'], 'package_id'));
                    $time_in_sec += Carbon::createFromFormat('H:i:s', $package->service_time)->secondsSinceMidnight() * $input['packages'][$index]['quantity'];
                    $professionals_count += $package->no_of_maids * $input['packages'][$index]['quantity'];
                }
                $input['hours'] = $time_in_sec / 3600; // hours based on selected packages
                $input['professionals_count'] = $professionals_count;
                $data['amount_devide_by'] = $professionals_count;
            }
        }
        /************************************************************* */
        $data['service_hours'] = $input['hours'];
        $data['professionals_count'] = $input['professionals_count'];
        $service_type_charge = ServiceTypeCharges::select('service_type_charges.id', 'service_type_charges.no_of_hours_from', 'service_type_charges.no_of_hours_to', 'service_type_charges.rate_per_hour')
            ->where([['service_type_charges.service_type_id', "=", $input['service_type_id']], ['deleted_at', "=", null]])
            ->where(function ($query) use ($input) { // matching more fields
                $query->where([['no_of_hours_from', '=', $input['hours']]])
                    ->orWhere([['no_of_hours_to', '=', $input['hours']]])
                    ->orWhere([['no_of_hours_from', '<', $input['hours']], ['no_of_hours_to', '>', $input['hours']]])
                    ->orWhere([['no_of_hours_from', '<', $input['hours']], ['no_of_hours_to', '=', null]])
                    ->orWhere([['no_of_hours_from', '=', null], ['no_of_hours_to', '=', null]]);
            })->first();
        if (@$input['id']) {
            $customer_charge = Customer::select('customers.price_hourly')
                ->where([['customers.customer_id', "=", $input['id']]])->first();
            if (@$customer_charge->price_hourly > 0) {
                $service_type_charge->rate_per_hour = $customer_charge->price_hourly;
            } else {
                $service_type_charge->rate_per_hour = $service_type_charge->rate_per_hour;
            }
            // **********************
            $is_sunday = Carbon::parse($input['date'])->isSunday();
            if ($is_sunday) {
                $week_day_charge = DB::table('week_day_charges')
                    ->where('service_type_id', $input['service_type_id'])
                    ->first();

                if ($week_day_charge) {
                    $service_type_charge->rate_per_hour = $week_day_charge->rate_per_hour;
                }
            }
        }
        $cleaning_material_rate_per_hour = 10;
        if (@$input['subscription_package_id']) {
            $service_type_charge->rate_per_hour = $subscription_package->amount / $input['hours'];
            $cleaning_material_rate_per_hour = null;
        }
        $data['subscription_package_id'] = @$input['subscription_package_id'];
        $data['service_rate_per_hour'] = $service_type_charge->rate_per_hour;
        $data['cleaning_material_rate_per_hour'] = $cleaning_material_rate_per_hour;
        $data['service_amount'] = $data['service_rate_per_hour'] * $data['service_hours'] * $data['professionals_count'];
        $data['cleaning_materials_amount'] = null;
        if (@$input['cleaning_materials'] == true) {
            $data['cleaning_materials_amount'] = $data['cleaning_material_rate_per_hour'] * $data['service_hours'] * $data['professionals_count'];
        }
        $data['service_addons'] = [];
        $data['service_addons_amount_before_discount'] = null;
        $data['service_addons_amount'] = null;
        $data['service_addons_discount'] = null;

        $data['subscription_package_amount_before_discount'] = @$subscription_package->strikethrough_amount;
        $data['subscription_package_amount'] = @$subscription_package->amount;
        $data['subscription_package_discount'] = @$subscription_package->strikethrough_amount - @$subscription_package->amount;
        /************************************************************* */
        /**
         * cleaning materials have multiple material options (not just true/false)
         */
        //$data['cleaning_materials_amount'] = null;
        $data['cleaning_materials'] = [];
        if (@$input['cleaning_materials'] == true && @$input['materials']) {
            $cleaning_material_ids = array_column($input['materials'], 'id');
            $data['cleaning_materials'] = ServiceTypeCleaningMaterial::select('id', 'service_type_id', 'name', 'amount as unit_price', )->where([['service_type_id', "=", $input['service_type_id']], ['deleted_at', "=", null]])->whereIn('id', $cleaning_material_ids)->get()->toArray();
            foreach ($data['cleaning_materials'] as $key => $material) {
                $data['cleaning_materials'][$key]['quantity'] = $data['service_hours'] * $data['professionals_count'];
                //$data['cleaning_materials_amount'] += $material['unit_price'] * ($data['service_hours'] * $data['professionals_count']);
                $data['cleaning_materials'][$key]['amount'] = $material['unit_price'] * $data['cleaning_materials'][$key]['quantity'];
            }
        }
        /************************************************************* */
        if ($service_type->service_type_model_id == 1) {
            // normal model
        } else if ($service_type->service_type_model_id == 2) {
            // package model
            $data['service_amount'] = 0;
            $data['cleaning_materials_amount'] = null;
        }
        /************************************************************* */
        if (@$input['addons']) {
            $service_addons_ids = array_column($input['addons'], 'service_addons_id');
            $data['service_addons'] = ServiceAddons::select('service_addons_id', 'service_type_id', 'service_minutes', 'strike_amount', 'amount')->where([['service_type_id', "=", $input['service_type_id']], ['deleted_at', "=", null]])->whereIn('service_addons_id', $service_addons_ids)->get()->toArray();
            foreach ($data['service_addons'] as $key => $service_addons) {
                $index = array_search($service_addons['service_addons_id'], array_column($input['addons'], 'service_addons_id'));
                if ($index >= 0) {
                    $data['service_addons'][$key]['quantity'] = $input['addons'][$index]['quantity'];
                    $data['service_addons_amount_before_discount'] += $data['service_addons'][$key]['strike_amount'] * $input['addons'][$index]['quantity'];
                    $data['service_addons_amount'] += $data['service_addons'][$key]['amount'] * $input['addons'][$index]['quantity'];
                }
            }
            $data['service_addons_discount'] = $data['service_addons_amount_before_discount'] - $data['service_addons_amount'];
        }
        /******************************************************** */
        $data['packages'] = [];
        $data['packages_amount_before_discount'] = null;
        $data['packages_amount'] = null;
        $data['packages_discount'] = null;
        if (@$input['packages'] && $service_type->service_type_model_id == 2) {
            // packages only for package type model
            $package_ids = array_column($input['packages'], 'package_id');
            $data['packages'] = DB::table('building_type_room_packages as btrp')
                ->select(
                    'btrp.id as package_id',
                    'btrp.service_type_id as service_type_id',
                    'btrp.service_time',
                    'btrp.actual_total_amount',
                    'btrp.total_amount',
                )
                ->whereIn('id', $package_ids)
                ->where([['btrp.service_type_id', "=", $input['service_type_id']], ['deleted_at', "=", null]])
                ->get()
                ->toArray();
            foreach ($data['packages'] as $key => $package) {
                $index = array_search($package->package_id, array_column($input['packages'], 'package_id'));
                if ($index >= 0) {
                    $data['packages'][$key]->quantity = $input['packages'][$index]['quantity'];
                    $data['packages_amount_before_discount'] += $data['packages'][$key]->actual_total_amount * $input['packages'][$index]['quantity'];
                    $data['packages_amount'] += $data['packages'][$key]->total_amount * $input['packages'][$index]['quantity'];
                }
            }
            $data['packages_discount'] = $data['packages_amount_before_discount'] - $data['packages_amount'];
            $data['service_amount'] = $data['packages_amount'];
        }
        /******************************************************** */
        $data['amount_before_discount'] = $data['service_amount'] + $data['cleaning_materials_amount'] + $data['service_addons_amount'];
        /******************************************************** */
        $data['frequency'] = @$input['frequency'];
        $data['frequency_discount_rate'] = null;
        $data['frequency_discount'] = null;
        if (@$input['frequency']) {
            $frequency = DB::table('frequencies as f')
                ->select(
                    'f.*',
                )
                ->where(['f.code' => $input['frequency']])
                ->first();
        }
        /******************************************************** */
        $data['coupon_id'] = null;
        $data['coupon_code'] = null;
        $data['coupon_discount'] = null;
        $data['coupons_applied'] = [];
        if (@$input['coupon_code']) {
            $coupon = DB::table('coupon_code as cc')
                ->select(
                    'cc.*',
                )
                ->where(['cc.coupon_name' => $input['coupon_code']])
                ->where(['cc.status' => 1])
                ->where('cc.percentage', '>', 0)
                ->first();
            $coupon_discount = applyCouponNew($input, $data['amount_before_discount'], $data);
            if (is_numeric($coupon_discount) && $coupon_discount > 0) {
                $percentage = number_format((($coupon_discount / ($data['amount_before_discount']))) * 100, 2);
                $data['coupons_applied'][] = ['id' => $input['coupon_code'], 'coupon_id' => @$coupon->coupon_id, 'coupon_code' => $coupon->coupon_name, 'title' => (float) $percentage . '% offer', 'message' => $coupon_discount . ' ' . Config::get('values.currency_code') . ' Discount applied !', 'status' => 'success'];
                $data['coupon_discount'] = $coupon_discount;
                $data['coupon_code'] = $coupon->coupon_name;
                $data['coupon_id'] = $coupon->coupon_id;
            } else {
                $data['coupons_applied'][] = ['id' => $input['coupon_code'], 'coupon_id' => @$coupon->coupon_id, 'coupon_code' => $input['coupon_code'], 'title' => 'Coupon code not valid !', 'message' => $coupon_discount, 'status' => 'failed'];
            }
        }
        /*if (@$input['coupon_code']) {
        $input['coupon_code'] = strtoupper(trim($input['coupon_code']));
        $coupon = DB::table('coupon_code as cc')
        ->select(
        'cc.*',
        )
        ->where(['cc.coupon_name' => $input['coupon_code']])
        ->where(['cc.status' => 1])
        ->where('cc.percentage', '>', 0)
        ->first();
        if ($coupon) {
        if (@$coupon->type == 'F') {
        // only for frequency coupon
        if (@$frequency->coupon_id != $coupon->coupon_id) {
        // check correct frequency coupon
        $data['coupons_applied'][] = ['id' => $input['coupon_code'], 'coupon_id' => @$coupon->coupon_id, 'coupon_code' => $input['coupon_code'], 'title' => 'Invalid coupon !', 'message' => 'Discount coupon not valid.', 'status' => 'failed'];
        } else {
        $data['frequency_discount_rate'] = $coupon->percentage;
        $data['coupon_discount'] = ($coupon->percentage / 100) * $data['amount_before_discount'];
        $data['coupons_applied'][] = ['id' => $input['coupon_code'], 'coupon_id' => @$coupon->coupon_id, 'coupon_code' => $coupon->coupon_name, 'title' => (float) $coupon->percentage . '% offer', 'message' => $data['coupon_discount'] . ' ' . Config::get('values.currency_code') . ' Discount applied !', 'status' => 'success'];
        $data['frequency_discount'] = $data['coupon_discount'];
        $data['coupon_code'] = $coupon->coupon_name;
        $data['coupon_id'] = $coupon->coupon_id;
        }
        } else {
        // for other coupons
        //dd($coupon);
        $coupon_discount = applyCoupon($coupon->coupon_id, $input['id'], Carbon::createFromFormat('d/m/Y', $input['date'])->format('Y-m-d'), $input['hours'], $data['amount_before_discount']);
        if ($coupon_discount > 0) {
        $percentage = number_format((($coupon_discount / ($data['amount_before_discount']))) * 100, 2);
        $data['coupons_applied'][] = ['id' => $input['coupon_code'], 'coupon_id' => @$coupon->coupon_id, 'coupon_code' => $coupon->coupon_name, 'title' => (float) $percentage . '% offer', 'message' => $coupon_discount . ' ' . Config::get('values.currency_code') . ' Discount applied !', 'status' => 'success'];
        $data['coupon_discount'] = $coupon_discount;
        $data['coupon_code'] = $coupon->coupon_name;
        $data['coupon_id'] = $coupon->coupon_id;
        } else {
        $data['coupons_applied'][] = ['id' => $input['coupon_code'], 'coupon_id' => @$coupon->coupon_id, 'coupon_code' => $input['coupon_code'], 'title' => 'Invalid coupon !', 'message' => 'Discount coupon not valid.', 'status' => 'failed'];
        }
        }
        } else {
        $data['coupons_applied'][] = ['id' => $input['coupon_code'], 'coupon_id' => @$coupon->coupon_id, 'coupon_code' => $input['coupon_code'], 'title' => 'Invalid coupon !', 'message' => 'Discount coupon not valid.', 'status' => 'failed'];
        }
        }*/
        /******************************************************** */
        $data['service_type_name'] = $service_type->service_type_name;
        $data['striked_discount_total'] = $data['service_addons_discount'] + $data['packages_discount'] + $data['subscription_package_discount'];
        $data['discount_total'] = $data['coupon_discount'];
        $data['taxable_amount'] = $data['amount_before_discount'] - $data['discount_total'];
        $data['vat_percentage'] = Config::get('values.vat_percentage');
        $data['vat_amount'] = $data['taxable_amount'] * ($data['vat_percentage'] / 100);
        $data['taxed_amount'] = $data['taxable_amount'] + $data['vat_amount'];
        $data['summary']['service_amount'] = $data['service_amount'];
        $data['summary']['cleaning_materials_amount'] = $data['cleaning_materials_amount'];
        $data['summary']['service_addons_amount_before_discount'] = $data['service_addons_amount_before_discount'];
        $data['summary']['service_addons_discount'] = $data['service_addons_discount'];
        $data['summary']['service_addons_amount'] = $data['service_addons_amount'];
        ////////
        $data['summary']['packages_amount_before_discount'] = $data['packages_amount_before_discount'];
        $data['summary']['packages_discount'] = $data['packages_discount'];
        $data['summary']['packages_amount'] = $data['packages_amount'];
        ////////
        $data['summary']['subscription_package_amount_before_discount'] = $data['subscription_package_amount_before_discount'];
        $data['summary']['subscription_package_amount'] = $data['subscription_package_amount'];
        $data['summary']['subscription_package_discount'] = $data['subscription_package_discount'];
        ////////
        $data['summary']['amount_before_discount'] = $data['amount_before_discount'];
        $data['summary']['frequency_discount'] = $data['frequency_discount'];
        $data['summary']['coupon_discount'] = $data['coupon_discount'];
        $data['summary']['striked_discount_total'] = $data['striked_discount_total'];
        $data['summary']['discount_total'] = $data['discount_total'];
        $data['summary']['taxable_amount'] = $data['taxable_amount'];
        $data['summary']['vat_percentage'] = Config::get('values.vat_percentage');
        $data['summary']['vat_amount'] = $data['vat_amount'];
        $data['summary']['taxed_amount'] = $data['taxed_amount'];
        /******************************************************** */
        $data['payment_type'] = null;
        $data['payment_type_charge'] = 0;
        if (@$input['payment_method']) {
            $payment_type = DB::table('payment_types as pt')
                ->select(
                    'pt.*',
                )
                ->where(['pt.id' => $input['payment_method']])
                ->first();
            $data['payment_type_id'] = $payment_type->id;
            $data['payment_type'] = $payment_type->name;
            if ($payment_type->charge_type == "F") {
                // fixed charge
                $data['payment_type_charge'] = $payment_type->charge;
            } else {
                // percentage of total
                $data['payment_type_charge'] = ($payment_type->charge / 100) * $data['taxed_amount'];
            }
        }
        /******************************************************** */
        $data['summary']['payment_type'] = $data['payment_type'];
        $data['summary']['payment_type_charge'] = $data['payment_type_charge'];
        $data['summary']['total_payable'] = $data['summary']['taxed_amount'] + $data['payment_type_charge'];
        $data['summary']['currency'] = Config::get('values.currency_code');
        /******************************************************** */
        $data['currency'] = Config::get('values.currency_code');
        $data['total_payable'] = $data['taxed_amount'] + $data['payment_type_charge'];
        /******************************************************** */
        $data['summary']['_popup_service_fee'] = $data['service_amount'];
        $data['summary']['_popup_service_addons_amount'] = $data['service_addons_amount'];
        $data['summary']['_popup_cleaning_materials_amount'] = $data['cleaning_materials_amount'];
        $data['summary']['_popup_amount_before_discount'] = $data['amount_before_discount'];
        $data['summary']['_popup_discount'] = $data['discount_total'];
        $data['summary']['_popup_taxable_amount'] = $data['taxable_amount'];
        $data['summary']['_popup_service_tax'] = $data['vat_amount'];
        $data['summary']['_popup_payment_type_charge'] = $data['payment_type_charge'];
        $data['summary']['_popup_total'] = $data['total_payable'];
        /******************************************************** */
        return $data;
    } catch (\Exception $e) {
        //throw new \ErrorException($e->getMessage());
        return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage() . ' ' . $e->getLine()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
    }
}
function scheduleStatusCode($status_text)
{
    $status_text = strtolower($status_text);
    $codes = [
        'deleted' => -2,
        'unknown' => -1,
        'pending approval' => 0,
        'confirmed' => 1,
        'payment failed' => 2,
        'cancelled' => 3,
    ];
    return isset($codes[$status_text]) ? $codes[$status_text] : $codes['unknown'];
}
function bookingPlatform($platform)
{
    $platform = strtolower($platform);
    $platforms = [
        'mobile' => "M",
        'web' => "W",
        'admin' => "A",
        'unknown' => "A",
    ];
    return isset($platforms[$platform]) ? $platforms[$platform] : $platforms['unknown'];
}
function bookingHistoryMenu()
{
    $menu = new stdClass();
    $menu->retry_payment = false;
    $menu->change_payment_method = false;
    $menu->reschedule = false;
    $menu->cancel_this = false;
    $menu->cancel_all = false;
    return $menu;
}
function pushNotification($customer, $notification = [], $data = [])
{
    /**
     * ---------------------------------
     * SCREEN NAMES
     * ---------------------------------
     * NOTIFICATIONS
     * BOOKING_HISTORY
     * SPECIAL_OFFER
     * PRIVACY_POLICY
     * TERMS_AND_CONDITIONS
     * HOME
     *
     *
     *
     *
     */
    try {
        $post = [
            'notification' => [
                'title' => @$notification['title'],
                'body' => @$notification['body'],
            ],
            "data" => [
                "click_action" => @$data['click_action'] ?: null,
                "sound" => @$data['sound'],
                "status" => @$data['status'],
                "screen" => @$data['screen'] ?: null,
            ],
            'to' => @$customer->push_token,
        ];
        $post = json_encode($post);
        //FCM API end-point
        $url = 'https://fcm.googleapis.com/fcm/send';
        //api_key in Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key
        //header with content_type api key
        $headers = array(
            'Content-Type:application/json',
            'Authorization:key=' . Config::get('values.google_fcm_server_key'),
        );
        //CURL request to route notification to FCM connection server (provided by Google)
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        $result = curl_exec($ch);
        if ($result === false) {
        }
        curl_close($ch);
    } catch (\Exception $e) {
    }
}
function checkoutPayment($checkout_data, $booking_common_id)
{
    $booking = Booking::where('booking_id', $booking_common_id)->firstOrFail();
    $customer = Customer::where('customer_id', $booking->customer_id)->first();
    $success_url = url('api/customer/payment/checkout/process-and-redirect/' . $booking_common_id); // for 3Ds only
    $failure_url = url('api/customer/payment/checkout/process-and-redirect/' . $booking_common_id); // for 3Ds only
    try {
        $client = new \GuzzleHttp\Client([
            'verify' => false,
        ]);
        /**************************************************************** */
        $post_header = ["Authorization" => "Bearer " . Config::get('values.checkout_secret_key'), "Content-Type" => "application/json", "Accept" => "application/json"];
        $checkout_data = [
            'source' => ["type" => "token", "token" => $checkout_data['token']],
            'customer' => ["email" => @$customer->email_address, "name" => @$customer->customer_name],
            '3ds' => ["enabled" => true, "attempt_n3d" => false],
            'amount' => ((float) $booking->_total_payable) * 100,
            //'amount' => ((float) 1) * 100, // TEST PURPOSE ONLY
            'currency' => Config::get('values.currency_code'),
            'processing_channel_id' => Config::get('values.checkout_channel_id'),
            "success_url" => $success_url,
            "failure_url" => $failure_url,
            'reference' => $booking->reference_id,
            'metadata' => ['coupon_code' => 'test']
        ];
        /**************************************************************** */
        $response_payment_data = $client->request('POST', Config::get('values.checkout_service_url'), [
            'headers' => $post_header,
            'http_errors' => false, // no exceptin fix
            'json' => $checkout_data,
        ]);
        return json_decode((string) $response_payment_data->getBody(), true);
    } catch (\Exception $e) {
        throw new \ErrorException('Processing payment failed.' . $e->getMessage());
    }
}
function checkoutApplePay($checkout_token_data, $booking_common_id)
{
    $booking = Booking::where('booking_id', $booking_common_id)->firstOrFail();
    $customer = Customer::where('customer_id', $booking->customer_id)->first();
    $success_url = url('api/customer/payment/checkout/process-and-redirect/' . $booking_common_id); // for 3Ds only
    $failure_url = url('api/customer/payment/checkout/process-and-redirect/' . $booking_common_id); // for 3Ds only
    try {
        $client = new \GuzzleHttp\Client([
            'verify' => false,
        ]);
        /**************************************************************** */
        $post_header = ["Authorization" => "Bearer " . Config::get('values.checkout_secret_key'), "Content-Type" => "application/json", "Accept" => "application/json"];
        $amount = ((float) $booking->_total_payable) * 100;
        if ($booking->customer_id == "39517") {
            $amount = (int) 1 * 100; // TEST PURPOSE ONLY
        }
        $checkout_data = [
            'source' => ["type" => "token", "token" => $checkout_token_data['token']],
            'customer' => ["email" => @$customer->email_address, "name" => @$customer->customer_name],
            '3ds' => ["enabled" => true, "attempt_n3d" => false],
            'amount' => $amount,
            'currency' => Config::get('values.currency_code'),
            'processing_channel_id' => Config::get('values.checkout_channel_id'),
            "success_url" => $success_url,
            "failure_url" => $failure_url,
            'reference' => $booking->reference_id,
            'metadata' => ['coupon_code' => 'test']
        ];
        /**************************************************************** */
        $response_payment_data = $client->request('POST', Config::get('values.checkout_service_url'), [
            'headers' => $post_header,
            'http_errors' => false, // no exceptin fix
            'json' => $checkout_data,
        ]);
        return json_decode((string) $response_payment_data->getBody(), true);
    } catch (\Exception $e) {
        throw new \ErrorException('Processing payment failed.' . $e->getMessage());
    }
}
function checkoutTamara($booking_common_id)
{
    $booking = Booking::where('booking_id', $booking_common_id)->firstOrFail();
    $customer = Customer::where('customer_id', $booking->customer_id)->first();
    try {
        $client = new \GuzzleHttp\Client([
            'verify' => false,
        ]);
        /**************************************************************** */
        $post_header = ["Authorization" => "Bearer " . Config::get('values.tamara_api_token'), "Content-Type" => "application/json", "Accept" => "application/json"];
        $checkout_data = [
            'total_amount' => [
                'amount' => $booking->_total_payable,
                'currency' => 'AED'
            ],
            'shipping_amount' => [
                'amount' => 0.0,
                'currency' => 'AED'
            ],
            'tax_amount' => [
                'amount' => $booking->_vat_amount,
                'currency' => 'AED'
            ],
            "order_reference_id" => $booking->reference_id,
            "items" => [
                [
                    "name" => "Service Name",
                    "type" => "Digital",
                    "reference_id" => $booking->reference_id,
                    "sku" => $booking->reference_id,
                    "quantity" => 1,
                    "total_amount" => [
                        "amount" => $booking->_total_payable,
                        "currency" => "AED"
                    ]
                ]
            ],
            "consumer" => [
                "email" => $customer->email_address,
                "first_name" => $customer->customer_name,
                "phone_number" => '971' . $customer->mobile_number_1
            ],
            "country_code" => "AE",
            "description" => $booking->reference_id,
            "merchant_url" => [
                "cancel" => Config::get('values.web_app_url') . 'booking/failed/' . $booking->reference_id,
                "failure" => url('api/customer/payment/tamara/process-and-redirect/' . $booking_common_id),
                "success" => url('api/customer/payment/tamara/process-and-redirect/' . $booking_common_id),
                "notification" => url('api/customer/payment/tamara/webhook/order_approved') // webhook url (important)
            ],
            "payment_type" => "PAY_BY_INSTALMENTS",
            "shipping_address" => [
                "city" => "City",
                "country_code" => "AE",
                "currency" => 'AED',
                "first_name" => $customer->customer_name,
                "line1" => "Address",
                "phone_number" => '971' . $customer->mobile_number_1
            ],
            "platform" => "web",
            "is_mobile" => false,
        ];
        /**************************************************************** */
        $response_payment_data = $client->request('POST', Config::get('values.tamara_base_url') . "checkout", [
            'headers' => $post_header,
            'http_errors' => false, // no exceptin fix
            'json' => $checkout_data,
        ]);
        return json_decode((string) $response_payment_data->getBody(), true);
    } catch (\Exception $e) {
        throw new \ErrorException('Tamara checkout failed.' . $e->getMessage());
    }
}
function checkoutGooglePay($checkout_data, $booking_common_id)
{
    $booking = Booking::where('booking_id', $booking_common_id)->firstOrFail();
    $customer = Customer::where('customer_id', $booking->customer_id)->first();
    $success_url = url('api/customer/payment/checkout/process-and-redirect/' . $booking_common_id); // for 3Ds only
    $failure_url = url('api/customer/payment/checkout/process-and-redirect/' . $booking_common_id); // for 3Ds only
    try {
        $client = new \GuzzleHttp\Client([
            'verify' => false,
        ]);
        /**************************************************************** */
        $post_header = ["Authorization" => "Bearer " . Config::get('values.checkout_secret_key'), "Content-Type" => "application/json", "Accept" => "application/json"];
        $checkout_data = [
            'source' => ["type" => "token", "token" => $checkout_data['token']],
            'customer' => ["email" => @$customer->email_address, "name" => @$customer->customer_name],
            '3ds' => ["enabled" => true, "attempt_n3d" => true],
            'amount' => ((float) $booking->_total_payable) * 100,
            //'amount' => ((float) 1) * 100, // TEST PURPOSE ONLY
            'currency' => Config::get('values.currency_code'),
            'processing_channel_id' => Config::get('values.checkout_channel_id'),
            "success_url" => $success_url,
            "failure_url" => $failure_url,
            'reference' => $booking->reference_id,
            'metadata' => ['coupon_code' => 'test']
        ];
        /**************************************************************** */
        $response_payment_data = $client->request('POST', Config::get('values.checkout_service_url'), [
            'headers' => $post_header,
            'http_errors' => false, // no exceptin fix
            'json' => $checkout_data,
        ]);
        return json_decode((string) $response_payment_data->getBody(), true);
    } catch (\Exception $e) {
        throw new \ErrorException('Processing payment failed.' . $e->getMessage());
    }
}
function generateApplePayToken($checkout_token_data)
{
    $client = new \GuzzleHttp\Client([
        'verify' => false,
    ]);
    $post_header = ["Authorization" => "Bearer " . Config::get('values.checkout_primary_key'), "Content-Type" => "application/json", "Accept" => "application/json"];
    $checkout_data = [
        "type" => "applepay",
        "token_data" => json_decode($checkout_token_data)
    ]; // don't convert to string
    // https://www.checkout.com/docs/payments/add-payment-methods/apple-pay
    // Step 1: Generate a Checkout.com token from the Apple Pay token
    $response_token_data = $client->request('POST', Config::get('values.checkout_token_url'), [
        'headers' => $post_header, // no exception fix
        'http_errors' => false,
        'json' => $checkout_data,
    ]);
    $token_data = json_decode((string) $response_token_data->getBody(), true);
    return $token_data;
}
function generateGooglePayToken($checkout_token_data)
{
    $client = new \GuzzleHttp\Client([
        'verify' => false,
    ]);
    $post_header = ["Authorization" => "Bearer " . Config::get('values.checkout_primary_key'), "Content-Type" => "application/json", "Accept" => "application/json"];
    $checkout_data = [
        "type" => "googlepay",
        "token_data" => json_decode($checkout_token_data)
    ]; // don't convert to string
    // https://www.checkout.com/docs/payments/add-payment-methods/apple-pay
    // Step 1: Generate a Checkout.com token from the Apple Pay token
    $response_token_data = $client->request('POST', Config::get('values.checkout_token_url'), [
        'headers' => $post_header, // no exception fix
        'http_errors' => false,
        'json' => $checkout_data,
    ]);
    $token_data = json_decode((string) $response_token_data->getBody(), true);
    return $token_data;
}
function afterCheckoutPaymentSuccess($booking_common_id, $checkout_data, $platform = "mobile")
{
    // checkout.com
    // this function will update database for a successful payment
    $booking = Booking::where('booking_id', $booking_common_id)->first();
    $payment_type = DB::table('payment_types as pt')
        ->select(
            'pt.*',
        )
        ->where(['pt.id' => $booking->payment_type_id])
        ->first();
    $bookings = DB::table('bookings as b')
        ->select(
            'b.*',
        )
        ->where(function ($query) use ($booking_common_id) {
            $query->where('b.booking_common_id', $booking_common_id);
            $query->orWhere('b.booking_id', $booking_common_id);
        })
        ->orderBy('b.booking_id', 'ASC')
        ->get();
    $bookings_ids = array_column($bookings->toArray(), 'booking_id');
    /********************************************************************************* */
    Booking::whereIn('booking_id', $bookings_ids)->update(['booking_status' => 0, 'web_book_show' => 1]); // update
    $online_payment = OnlinePayment::where('booking_id', $booking->booking_id)->where('reference_id', $booking->reference_id)->first();
    if ($online_payment) {
        if ($online_payment->payment_status != 'success') {
            $online_payment->transaction_id = $checkout_data['id'];
            $online_payment->amount = $checkout_data['amount'] / 100;
            $online_payment->transaction_charge = 0;
            $online_payment->customer_id = $booking->customer_id;
            $online_payment->description = null;
            $online_payment->payment_status = 'success';
            $online_payment->payment_type = $payment_type->code;
            $online_payment->user_agent = @$_SERVER['HTTP_USER_AGENT'];
            $online_payment->post_data = null;
            $online_payment->return_data = json_encode($checkout_data);
            $online_payment->paid_from = bookingPlatform(@$platform);
            $online_payment->inv_id = null;
            $online_payment->payment_datetime = now();
            $online_payment->save();
        } else {
            return 'Payment Already Verfied !';
        }
    } else {
        $online_payment = new OnlinePayment();
        $online_payment->transaction_id = $checkout_data['id'];
        $online_payment->booking_id = $booking->booking_id;
        $online_payment->reference_id = $booking->reference_id;
        $online_payment->amount = $checkout_data['amount'] / 100;
        $online_payment->transaction_charge = 0;
        $online_payment->customer_id = $booking->customer_id;
        $online_payment->description = null;
        $online_payment->payment_status = 'success';
        $online_payment->payment_type = $payment_type->code;
        $online_payment->user_agent = @$_SERVER['HTTP_USER_AGENT'];
        $online_payment->post_data = null;
        $online_payment->return_data = json_encode($checkout_data);
        $online_payment->paid_from = bookingPlatform(@$platform);
        $online_payment->inv_id = null;
        $online_payment->payment_datetime = now();
        $online_payment->save();
        /**************************************** */
        // update customer payments
        $customer_payment = new CustomerPayments();
        $customer_payment->day_service_id = 0;
        $customer_payment->customer_id = $booking->customer_id;
        $customer_payment->paid_amount = $checkout_data['amount'] / 100;
        $customer_payment->payment_method = $payment_type->id;
        $customer_payment->paid_at = bookingPlatform(@$platform);
        $customer_payment->paid_at_id = 1;
        $customer_payment->booking_id = $booking->booking_id;
        $customer_payment->paid_datetime = now();
        $customer_payment->save();
    }
    /******************************************************************** */
    // send mails
    $customer = Customer::where('customer_id', '=', $booking->customer_id)->first();
    send_booking_confirmation_mail_to_any($booking->booking_id);
    send_booking_confirmation_sms_to_customer($booking->booking_id);
    pushNotification($customer, ['title' => "Booking Received !", 'body' => "Booking received with Ref. Id " . $booking->reference_id . "."], ['screen' => 'BOOKING_HISTORY']);
}
function afterTamaraPaymentSuccess($booking_common_id, $checkout_data, $platform = "mobile")
{
    // checkout.com
    // this function will update database for a successful payment
    $booking = Booking::where('booking_id', $booking_common_id)->first();
    $payment_type = DB::table('payment_types as pt')
        ->select(
            'pt.*',
        )
        ->where(['pt.id' => $booking->payment_type_id])
        ->first();
    $bookings = DB::table('bookings as b')
        ->select(
            'b.*',
        )
        ->where(function ($query) use ($booking_common_id) {
            $query->where('b.booking_common_id', $booking_common_id);
            $query->orWhere('b.booking_id', $booking_common_id);
        })
        ->orderBy('b.booking_id', 'ASC')
        ->get();
    $bookings_ids = array_column($bookings->toArray(), 'booking_id');
    /********************************************************************************* */
    Booking::whereIn('booking_id', $bookings_ids)->update(['booking_status' => 0, 'web_book_show' => 1]); // update
    $online_payment = OnlinePayment::where('booking_id', $booking->booking_id)->where('reference_id', $booking->reference_id)->first();
    if ($online_payment) {
        if ($online_payment->payment_status != 'success') {
            $online_payment->transaction_id = $checkout_data['order_id'];
            $online_payment->amount = $checkout_data['items'][0]['total_amount']['amount'];
            $online_payment->transaction_charge = 0;
            $online_payment->customer_id = $booking->customer_id;
            $online_payment->description = null;
            $online_payment->payment_status = 'success';
            $online_payment->payment_type = $payment_type->code;
            $online_payment->user_agent = @$_SERVER['HTTP_USER_AGENT'];
            $online_payment->post_data = null;
            $online_payment->return_data = json_encode($checkout_data);
            $online_payment->paid_from = bookingPlatform(@$platform);
            $online_payment->inv_id = null;
            $online_payment->payment_datetime = now();
            $online_payment->save();
        } else {
            return 'Payment Already Verfied !';
        }
    } else {
        $online_payment = new OnlinePayment();
        $online_payment->transaction_id = $checkout_data['order_id'];
        $online_payment->booking_id = $booking->booking_id;
        $online_payment->reference_id = $booking->reference_id;
        $online_payment->amount = $checkout_data['items'][0]['total_amount']['amount'];
        $online_payment->transaction_charge = 0;
        $online_payment->customer_id = $booking->customer_id;
        $online_payment->description = null;
        $online_payment->payment_status = 'success';
        $online_payment->payment_type = $payment_type->code;
        $online_payment->user_agent = @$_SERVER['HTTP_USER_AGENT'];
        $online_payment->post_data = null;
        $online_payment->return_data = json_encode($checkout_data);
        $online_payment->paid_from = bookingPlatform(@$platform);
        $online_payment->inv_id = null;
        $online_payment->payment_datetime = now();
        $online_payment->save();
        /**************************************** */
        // update customer payments
        $customer_payment = new CustomerPayments();
        $customer_payment->day_service_id = 0;
        $customer_payment->customer_id = $booking->customer_id;
        $customer_payment->paid_amount = $checkout_data['items'][0]['total_amount']['amount'];
        $customer_payment->payment_method = $payment_type->id;
        $customer_payment->paid_at = bookingPlatform(@$platform);
        $customer_payment->paid_at_id = 1;
        $customer_payment->booking_id = $booking->booking_id;
        $customer_payment->paid_datetime = now();
        $customer_payment->save();
    }
    /******************************************************************** */
    // send mails
    //$customer = Customer::where('customer_id', '=', $booking->customer_id)->first();
    //send_booking_confirmation_mail_to_any($booking->booking_id);
    //send_booking_confirmation_sms_to_customer($booking->booking_id);
    //pushNotification($customer, ['title' => "Booking Received !", 'body' => "Booking received with Ref. Id " . $booking->reference_id . "."], ['screen' => 'BOOKING_HISTORY']);
}

function curlFunOdoo($curl_post, $url)
{
    $curl_header = array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($curl_post)
    );
    $ch2 = curl_init();
    curl_setopt($ch2, CURLOPT_URL, $url);
    curl_setopt($ch2, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($ch2, CURLOPT_POST, 1);
    curl_setopt($ch2, CURLOPT_POSTFIELDS, $curl_post);
    curl_setopt($ch2, CURLOPT_HTTPHEADER, $curl_header);
    curl_setopt($ch2, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch2, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch2, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($ch2, CURLOPT_TIMEOUT, 10);
    $user_datas = curl_exec($ch2);
    if (curl_errno($ch2)) {
        $error_msg = curl_error($ch2);
        throw new \ErrorException($error_msg);
    }
    return $user_datas;
}

function ccavenueStripName($name)
{
    /**
     * Remove special characters from customer name
     */
    return preg_replace('/[^A-Za-z0-9 \-]/', '', $name); // Removes special chars.
}
