<?php
use Response as Response;
use Mail as Mail;
use Config as Config;
use App\Models\Customer;
use App\Models\CustomerAddress;
use App\Models\BookingCleaningMaterial;
use App\Models\Settings;

function send_booking_confirmation_mail_to_any($booking_common_id)
{
    try {
        $settings = Settings::first();
        $bookings = DB::table('bookings as b')
            ->select(
                'b.*',
                DB::raw('(CASE WHEN (b.reference_id IS NULL OR b.reference_id = "") THEN b.booking_id ELSE b.reference_id END) as reference_id'),
                DB::raw('IFNULL(st.customer_app_service_type_name,st.service_type_name) as service_type_name'),
                'st.service_type_model_id',
                'stm.model as service_type_model',
                'm.maid_id',
                'm.maid_name',
                DB::raw('(CASE WHEN b.cleaning_material = "Y" THEN "Yes" ELSE "No" END) as cleaning_material_opted'),
                'f.name as frequency',
                'ca.customer_address as address',
                DB::raw('"' . Config::get('values.currency_code') . '" as currency'),
                DB::raw('(CASE WHEN b.payment_type_id IS NULL THEN b.pay_by ELSE pt.name END) as payment_method'),
                'pt.charge as payment_type_charge',
                DB::raw('DAYNAME(b.service_start_date) as service_week_day_name'),
                'op.transaction_id as op_transaction_id'
            )
            ->leftJoin('service_types as st', 'b.service_type_id', 'st.service_type_id')
            ->leftJoin('service_type_models as stm', 'st.service_type_model_id', 'stm.id')
            ->leftJoin('maids as m', 'b.maid_id', 'm.maid_id')
            ->leftJoin('customers as c', 'b.customer_id', 'c.customer_id')
            ->leftJoin('frequencies as f', 'b.booking_type', 'f.code')
            ->leftJoin('payment_types as pt', 'b.payment_type_id', 'pt.id')
            ->leftJoin('customer_addresses as ca', 'b.customer_address_id', 'ca.customer_address_id')
            ->leftJoin('online_payments as op', function ($join) {
                $join->on('b.booking_id', '=', 'op.booking_id');
                $join->whereColumn('op.reference_id', 'b.reference_id');
                $join->where('op.payment_status', 'success');
            })
            ->where(function ($query) use ($booking_common_id) {
                $query->where('b.booking_common_id', $booking_common_id);
                $query->orWhere('b.booking_id', $booking_common_id);
            })
            ->orderBy('b.booking_id', 'ASC')
            ->get();
        $booking = $bookings[0];
        $customer = Customer::where('customer_id', '=', $booking->customer_id)->first();
        $customer_address = CustomerAddress::
            select(
                'customer_addresses.*',
                'a.area_name',
            )
            ->leftJoin('areas as a', 'customer_addresses.area_id', 'a.area_id')->where('customer_address_id', '=', $booking->customer_address_id)->first();
        $frequency = DB::table('frequencies as f')
            ->select(
                'f.*',
            )
            ->where(['f.code' => $booking->booking_type])
            ->first();
        $booking_addons = DB::table('booking_addons as ba')
            ->select(
                'ba.service_addons_id',
                'ba.service_addon_name',
                'sao.service_addon_description',
                DB::raw('"' . Config::get('values.currency_code') . '" as currency'),
                'ba.service_minutes',
                'ba.strike_amount',
                'ba.unit_price',
                'ba.quantity',
                'ba.amount',
                DB::raw('CONCAT("' . Config::get('values.customer_app_assets_prefix_url') . 'service-addon-images/",IFNULL(sao.customer_app_thumbnail,"service-addons-thumbnail.png"),"?v=' . $settings->customer_app_img_version . '") as image_url'),
            )
            ->leftJoin('service_addons as sao', 'ba.service_addons_id', 'sao.service_addons_id')
            ->where([['ba.booking_id', "=", $booking_common_id]])
            ->orderBy('sao.populariry', 'DESC')
            ->get();
        //dd($booking_addons);
        /****************************************************************************************** */
        // selected packages
        // $booking_packages = DB::table('booking_packages as bp')
        //     ->select(
        //         'bp.*',
        //         DB::raw('IF(btrp.title IS NULL or btrp.title = "", br.name, btrp.title) as package_name'),
        //         'btrp.description as package_description',
        //         DB::raw('"' . Config::get('values.currency_code') . '" as currency'),
        //         'btrp.actual_total_amount as strike_amount',
        //         'bt.customer_app_banner',
        //         'btrp.customer_app_thumbnail',
        //         DB::raw('CONCAT("' . Config::get('values.file_server_url') . 'customer-app-assets/service-package-thumbnails/",IFNULL(btrp.customer_app_thumbnail,"default.png"),"?v=' . $settings->customer_app_img_version . '") as thumbnail_url'),
        //     )
        //     ->leftJoin('building_type_room_packages as btrp', 'bp.building_type_room_package_id', 'btrp.id')
        //     ->leftJoin('service_types as st', 'btrp.service_type_id', 'st.service_type_id')
        //     ->leftJoin('building_type_room as btr', 'btrp.building_type_room_id', 'btr.id')
        //     ->leftJoin('building_types as bt', 'btr.building_type_id', 'bt.id')
        //     ->leftJoin('building_rooms as br', 'btr.building_room_id', 'br.id')
        //     ->where([['bp.booking_id', "=", $booking_common_id]])
        //     ->orderBy('bp.id', 'ASC')
        //     ->get();
        // foreach ($booking_packages as $key => $package_data) {
        //     if ($package_data->customer_app_thumbnail == "") {
        //         $booking_packages[$key]->customer_app_thumbnail = Config::get('values.customer_app_assets_prefix_url') . 'package-type-banner-images/' . $package_data->customer_app_banner . '?v=' . $settings->customer_app_img_version;
        //     }
        // }
        $booking_packages = array();
        //dd($booking_packages);
        /****************************************************************************************** */
        // subscription package
        // $subscription_package = DB::table('booking_subscription_packages as bsp')->select(
        //     'bsp.*',
        //     DB::raw('CONCAT("' . Config::get('values.file_server_url') . '","upload/promotion_images/",p.promotion_image_web,"?v=' . $settings->customer_app_img_version . '") as thumbnail_url'),
        // )
        //     ->leftJoin('package as p', 'bsp.package_id', 'p.package_id')
        //     ->where([['bsp.booking_id', "=", $booking_common_id]])
        //     ->first(); // only 1 package is currently allowed
        $subscription_package = array();
        //dd($subscription_package);
        /****************************************************************************************** */
        $cleaning_materials = BookingCleaningMaterial::
            select(
                'booking_cleaning_materials.*',
                'btcm.description',
                DB::raw('CONCAT("' . Config::get('values.customer_app_assets_prefix_url') . '","cleaning-material-images/",thumbnail_file,"?v=' . $settings->customer_app_img_version . '") as thumbnail_url'),
            )
            ->leftJoin('service_type_cleaning_materials as btcm', 'booking_cleaning_materials.service_type_cleaning_material_id', 'btcm.id')
            ->where('booking_id', $booking->booking_id)
            ->get();
        /****************************************************************************************** */
        // send to admin
        try {
            $text_body = 'Congratulations. You have got a new booking with Ref. No. ' . @$booking->reference_id . '.';
            Mail::send(
                'emails.booking_confirmation_to_any',
                [
                    'booking' => $bookings[0],
                    'bookings' => $bookings,
                    'customer' => $customer,
                    'customer_address' => $customer_address,
                    'greeting' => 'Dear Admin',
                    'text_body' => $text_body,
                    'frequency' => $frequency,
                    'booking_addons' => $booking_addons,
                    'booking_packages' => $booking_packages,
                    'subscription_package' => $subscription_package,
                    'cleaning_materials' => $cleaning_materials,
                    'settings' => $settings,
                    'mail_to' => 'admin'
                ],
                function ($m) use ($customer, $booking) {
                    $m->from(Config::get('mail.mail_from_address'), Config::get('mail.mail_from_name'));
                    $m->subject('New Booking Received ' . $booking->reference_id);
                    $m->to(Config::get('mail.mail_to_admin_address'), Config::get('mail.mail_to_admin_address_name'));
                    //$m->bcc('revathy.l@azinova.info', 'Revathy L');
                    $m->bcc('samnad.s@azinova.info', 'Samnad S - Developer');
                }
            );
        } catch (\Exception $e) {
            if (isDemo()) {
                throw new \ErrorException($e->getMessage());
            }
        }
        /****************************************************************************************** */
        // send to customer
        try {
            $text_body = 'Thank you for choosing '.Config::get('values.company_name').' for your upcoming booking. We have received your booking request and are currently processing it. We hope you have a great experience with our services. Our dedicated team will review your booking details and will reach out to you shortly to confirm your booking.';
            Mail::send(
                'emails.booking_confirmation_to_any',
                [
                    'booking' => $bookings[0],
                    'bookings' => $bookings,
                    'customer' => $customer,
                    'customer_address' => $customer_address,
                    'greeting' => 'Dear ' . $customer->customer_name,
                    'text_body' => $text_body,
                    'frequency' => $frequency,
                    'booking_addons' => $booking_addons,
                    'booking_packages' => $booking_packages,
                    'subscription_package' => $subscription_package,
                    'cleaning_materials' => $cleaning_materials,
                    'settings' => $settings,
                    'mail_to' => 'customer'
                ],
                function ($m) use ($customer, $booking) {
                    $m->from(Config::get('mail.mail_from_address'), Config::get('mail.mail_from_name'));
                    $m->subject('Booking Received ' . $booking->reference_id);
                    $m->to($customer->email_address, $customer->customer_name);
                }
            );
        } catch (\Exception $e) {
            if (isDemo()) {
                throw new \ErrorException($e->getMessage());
            }
        }
        /****************************************************************************************** */
    } catch (\Exception $e) {
        if (isDemo()) {
            throw new \ErrorException($e->getMessage());
        }
    }
}
?>