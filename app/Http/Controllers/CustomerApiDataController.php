<?php

namespace App\Http\Controllers;

use App\Models\Settings;
use App\Models\ServiceTypeCleaningMaterial;
use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;

class CustomerApiDataController extends Controller
{
    public function data(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        $data = json_decode($request->getContent(), true);
        $input = @$data['params'];
        $settings = Settings::first();
        $working_hours_selection = [
            [
                'id' => 1,
                'hours' => 2
            ],
            [
                'id' => 2,
                'hours' => 4
            ],
            [
                'id' => 3,
                'hours' => 6
            ],
            [
                'id' => 4,
                'hours' => 8
            ],
            [
                'id' => 5,
                'hours' => 10
            ],
        ];
        $working_hours_selection = [2, 4, 6, 8, 10];
        /**************************************************************** */
        $cleaning_materials = ServiceTypeCleaningMaterial::
            select(
                'id',
                'service_type_id',
                'name',
                'amount',
                DB::raw('CONCAT("' . Config::get('values.customer_app_assets_prefix_url') . '","cleaning-material-images/",thumbnail_file,"?v=' . $settings->customer_app_img_version . '") as thumbnail_url'),
            )
            ->where([['deleted_at', '=', null]])
            ->orderBy('sort_order', 'ASC')
            ->get()
            ->toArray();
        /**************************************************************** */
        $mapped_services = DB::table('service_type_category_and_service_type_mapping as sm')->select(
            'st.service_type_id',
            'sm.service_type_category_id',
            DB::raw('IFNULL(st.customer_app_service_type_name,st.service_type_name) as service_type_name'),
            'stc.service_category_name as service_type_category_name',
            DB::raw('(CASE WHEN (stc.customer_app_icon_file IS NOT NULL AND stc.customer_app_icon_file != "") THEN CONCAT("' . Config::get('values.customer_app_assets_prefix_url') . 'service-category-icons/",stc.customer_app_icon_file,"?v=' . $settings->customer_app_img_version . '") ELSE NULL END) AS service_type_category_icon'),
            DB::raw('(CASE WHEN (st.customer_app_thumbnail_file IS NOT NULL AND st.customer_app_thumbnail_file != "") THEN CONCAT("' . Config::get('values.customer_app_assets_prefix_url') . 'service-thumbnails/",st.customer_app_thumbnail_file,"?v=' . $settings->customer_app_img_version . '") ELSE NULL END) AS service_type_thumbnail'),
            'st.service_type_model_id',
            'stm.model as service_type_model',
            'st.web_url_slug',
            'st.info_html'
        )
            ->leftJoin('service_types as st', 'sm.service_type_id', 'st.service_type_id')
            ->leftJoin('service_type_categories as stc', 'sm.service_type_category_id', 'stc.id')
            ->leftJoin('service_type_models as stm', 'st.service_type_model_id', 'stm.id')
            ->where([['stc.deleted_at', '=', null], ['sm.deleted_at', '=', null], ['st.customer_app_status', '=', 1]])
            ->orderBy('stc.sort_order', 'ASC')
            ->orderBy('st.customer_app_order_id', 'ASC')
            ->orderBy('st.service_type_id', 'ASC')
            ->get();
        /**************************************************************** */
        // service subscription packages
        // $subscription_packages = DB::table('package as p')->select(
        //     'p.package_id',
        //     'p.package_name',
        //     'p.service_type_id',
        //     DB::raw('IFNULL(st.customer_app_service_type_name,st.service_type_name) as service_type_name'),
        //     'p.cleaning_material',
        //     'p.no_of_bookings',
        //     'p.working_hours',
        //     'p.strikethrough_amount',
        //     'p.amount',
        //     'p.description',
        //     'p.tc_html',
        //     DB::raw('2 as professionals_count'),
        //     DB::raw('CONCAT("' . Config::get('values.file_server_url') . '","upload/promotion_images/",p.promotion_image_web,"?v=' . $settings->customer_app_img_version . '") as banner_image_url'),
        // )
        //     ->leftJoin('service_types as st', 'p.service_type_id', 'st.service_type_id')
        //     ->where([['p.status', '=', 1]])
        //     ->orderBy('st.service_type_id', 'ASC')
        //     ->get();
        // foreach ($subscription_packages as $key => $subscription_package) {
        //     $subscription_packages[$key]->working_hours = $subscription_package->working_hours * $subscription_package->no_of_bookings;
        // }
        /**************************************************************** */
        $response['service_categories'] = [];
        foreach ($mapped_services as $key => $service_type) {
            $key = array_search($service_type->service_type_category_id, array_column($response['service_categories'], 'service_type_category_id'));
            if ($key > -1) {
                // exist
                $objectB = clone $service_type;
                if ($objectB->service_type_model_id == 1) {
                    $objectB->min_working_hours = 2;
                    $objectB->max_working_hours = 10;
                    $objectB->working_hours_selection = $working_hours_selection;
                    $objectB->min_no_of_professionals = 1;
                    $objectB->max_no_of_professionals = 5;
                } else {
                    $objectB->min_working_hours = null;
                    $objectB->max_working_hours = null;
                    $objectB->working_hours_selection = $working_hours_selection;
                    $objectB->min_no_of_professionals = null;
                    $objectB->max_no_of_professionals = null;
                }
                /*$objectB->subscription_packages = array_filter($subscription_packages->toArray(), function ($subscription_package) use ($service_type) {
                    return $subscription_package->service_type_id == $service_type->service_type_id;
                });*/
                $objectB->cleaning_materials = array_values(array_filter($cleaning_materials, function ($cleaning_material) use ($service_type) {
                    return $cleaning_material['service_type_id'] == $service_type->service_type_id;
                }));
                $response['service_categories'][$key]->sub_categories[] = $objectB;
            } else {
                // not exist
                $objectB = clone $service_type;
                unset($objectB->service_type_id);
                unset($objectB->service_type_name);
                unset($objectB->service_type_thumbnail);
                unset($objectB->service_type_model);
                unset($objectB->service_type_model_id);
                unset($objectB->web_url_slug);
                unset($objectB->info_html);
                // sub_categories add from same object
                $objectC = clone $service_type;
                if ($objectC->service_type_model_id == 1) {
                    $objectC->min_working_hours = 2;
                    $objectC->max_working_hours = 10;
                    $objectC->working_hours_selection = $working_hours_selection;
                    $objectC->min_no_of_professionals = 1;
                    $objectC->max_no_of_professionals = 5;
                } else {
                    $objectC->min_working_hours = null;
                    $objectC->max_working_hours = null;
                    $objectC->working_hours_selection = $working_hours_selection;
                    $objectC->min_no_of_professionals = null;
                    $objectC->max_no_of_professionals = null;
                }
                /*$objectC->subscription_packages = array_filter($subscription_packages->toArray(), function ($subscription_package) use ($service_type) {
                    return $subscription_package->service_type_id == $service_type->service_type_id;
                });*/
                $objectC->cleaning_materials = array_values(array_filter($cleaning_materials, function ($cleaning_material) use ($service_type) {
                    return $cleaning_material['service_type_id'] == $service_type->service_type_id;
                }));
                $objectB->sub_categories[] = $objectC;
                $response['service_categories'][] = $objectB;
            }
        }
        //$response['subscription_packages'] = $subscription_packages;
        /**************************************************************** */
        $special_offers = DB::table('offers_sb as so')
            ->select(
                'so.id_offers_sb as offer_id',
                'so.offer_heading',
                'so.offer_subheading',
                'so.offer_content',
                'cc.coupon_name as coupon_code',
                'cc.coupon_id',
                'st.service_type_id',
                'st.web_url_slug',
                DB::raw('IFNULL(st.customer_app_service_type_name,st.service_type_name) as service_type_name'),
                'st.service_type_model_id',
                'stm.model as service_type_model',
                DB::raw('null as banner_image_url'),
                DB::raw('CONCAT("' . Config::get('values.file_server_url') . '","offer_img/uploads/",(CASE WHEN so.offer_image = "" THEN "default.jpg" ELSE so.offer_image END),"?v=' . $settings->customer_app_img_version . '") as banner_image_url'),
                DB::raw('CONCAT("' . Config::get('values.file_server_url') . '","offer_img/uploads/",(CASE WHEN so.banner_image = "" THEN "default.jpg" ELSE so.banner_image END),"?v=' . $settings->customer_app_img_version . '") as banner_image_url_new'),
                'st.info_html'
            )
            ->leftJoin('coupon_code as cc', 'so.coupon_id', 'cc.coupon_id')
            ->leftJoin('service_types as st', 'cc.service_id', 'st.service_type_id')
            ->leftJoin('service_type_models as stm', 'st.service_type_model_id', 'stm.id')
            ->where(['so.offer_status' => 0])
            ->orderBy('so.id_offers_sb', 'DESC')
            ->get();
        foreach ($special_offers as $key => $special_offer) {
            if ($special_offer->service_type_model_id == '1') {
                $special_offers[$key]->min_working_hours = 2;
                $special_offers[$key]->max_working_hours = 10;
                $special_offers[$key]->working_hours_selection = $working_hours_selection;
                $special_offers[$key]->min_no_of_professionals = 1;
                $special_offers[$key]->max_no_of_professionals = 5;
            } else {
                $special_offers[$key]->min_working_hours = null;
                $special_offers[$key]->max_working_hours = null;
                $special_offers[$key]->working_hours_selection = $working_hours_selection;
                $special_offers[$key]->min_no_of_professionals = null;
                $special_offers[$key]->max_no_of_professionals = null;
            }
        }
        /**************************************************************** */
        // $response['subscription_packages_and_special_offers'] = array_merge($subscription_packages->toArray(), $special_offers->toArray());
        $response['subscription_packages_and_special_offers'] = array_merge($special_offers->toArray());
        /**************************************************************** */
        // $packages = DB::table('building_type_room_packages as btrp')
        //     ->select(
        //         'btrp.id as package_id',
        //         'st.service_type_id as service_type_id',
        //         'btrp.building_type_room_id',
        //         'btrp.is_offer',
        //         //DB::raw('(CASE WHEN btrp.title IS NULL THEN br.name ELSE btrp.title END) as package_name'),
        //         DB::raw('IF(btrp.title IS NULL or btrp.title = "", br.name, btrp.title) as package_name'),
        //         'btrp.description as package_description',
        //         DB::raw('CONCAT("' . Config::get('values.package_offer_thumbnail_prefix_url') . '",IFNULL(btrp.customer_app_thumbnail,"default.png")) as imageurl'),
        //         DB::raw('"' . Config::get('values.currency_code') . '" as currency'),
        //         'btrp.actual_total_amount as actual_amount',
        //         'btrp.total_amount as amount',
        //         'bt.name as building_type',
        //         'st.service_type_name as service_type',
        //         'btrp.info_html',
        //         'btrp.service_time',
        //         'btrp.service_time as service_time_in_minutes',
        //         DB::raw('TIME_TO_SEC(btrp.service_time)/60 as service_time_in_minutes'),
        //     )
        //     ->leftJoin('service_types as st', 'btrp.service_type_id', 'st.service_type_id')
        //     ->leftJoin('building_type_room as btr', 'btrp.building_type_room_id', 'btr.id')
        //     ->leftJoin('building_types as bt', 'btr.building_type_id', 'bt.id')
        //     ->leftJoin('building_rooms as br', 'btr.building_room_id', 'br.id')
        //     ->where([['st.service_type_model_id', "=", 2]])
        //     ->where([['btrp.deleted_at', "=", null], ['btr.deleted_at', "=", null], ['bt.deleted_at', "=", null], ['br.deleted_at', "=", null]])
        //     ->orderBy('bt.id', 'ASC')
        //     ->get();
        /**************************************************************** */
        $response['payment_types'] = DB::table('payment_types as pt')
            ->select(
                'pt.id',
                'pt.name',
                'pt.code',
                'pt.charge',
                'pt.charge_type',
                'pt.default',
                'pt.show_in_web',
                'pt.show_in_app',
                DB::raw('CONCAT("' . Config::get('values.customer_app_assets_prefix_url') . '","payment-method-icons/",IFNULL(pt.customer_app_icon,"default.png"),"?v=' . $settings->customer_app_img_version . '") as icon_url'),
            )
            ->where(['deleted_at' => null]);
        if (@$input['platform'] == 'web') {
            //$response['payment_types']->where(['show_in_web' => 1]);
        } else {
            $response['payment_types']->where(['show_in_app' => 1]);
        }
        $response['payment_types'] = $response['payment_types']->orderBy('pt.sort_order', 'ASC')->get();
        /**************************************************************** */
        $response['home_banners'] = DB::table('homepage_banners as hb')
            ->select(
                'hb.id',
                DB::raw('CONCAT("' . Config::get('values.customer_app_assets_prefix_url') . '","home-banners/",IFNULL(hb.banner_image,"default.png"),"?v=' . $settings->customer_app_img_version . '") as bannerImage'),
            )
            ->where(['status' => 1]);
        $response['home_banners'] = $response['home_banners']->orderBy('hb.id', 'ASC')->get();
        /**************************************************************** */
        $response['frequency_list'] = DB::table('frequencies as fl')
            ->select(
                'fl.id',
                'fl.name',
                'fl.description',
                'fl.code',
                'cc.percentage as percentage',
                'cc.coupon_id as coupon_id',
                'cc.coupon_name as coupon_code',
                'fl.hourly_rate',
                'fl.cleaning_material_hourly_rate as cleaning_material_rate',
                'fl.max_working_hour',
            )
            ->leftJoin('coupon_code as cc', function ($join) {
                $join->on('cc.coupon_id', '=', 'fl.coupon_id');
                $join->where('cc.status', '=', 1);
                $join->where('cc.type', '=', 'F');
                $join->where('cc.percentage', '>', 0);
            })
            ->where(['deleted_at' => null])
            ->get();
        foreach ($response['frequency_list'] as $key => $frequency) {
            $response['frequency_list'][$key]->details[] = "Demo detail content 1 here";
            $response['frequency_list'][$key]->details[] = "Demo detail content 2 here";
        }
        /**************************************************************** */
        //$response['banner_images'] = array(Config::get('values.file_server_url') . "upload/promotion_images/Group 26808.jpg",Config::get('values.file_server_url') . "upload/promotion_images/Group 26809.jpg",Config::get('values.file_server_url') . "upload/promotion_images/Group 26810.jpg",Config::get('values.file_server_url') . "upload/promotion_images/Group 26811.jpg",);
        /**************************************************************** */
        //$response['insurance_info'] = array("Dummy Text 1.","Dummy Text 2.","Dummy Text 3.",);
        /**************************************************************** */
        $response['genders'] = DB::table('genders as g')
            ->select(
                'g.gender_id',
                'g.gender'
            )
            ->where(['g.deleted_at' => null])
            ->orderBy('g.sort_order_id', 'ASC')
            ->get();
        /**************************************************************** */
        $response['cancel_reasons'] = DB::table('booking_cancel_reasons as bcr')
            ->select(
                'bcr.id',
                'bcr.reason',
            )
            ->where([['bcr.deleted_at', '=', null]])
            ->orderBy('bcr.order_priority', 'ASC')
            ->get();
        /**************************************************************** */
        $response['address_types'] = array(
            ['code' => 'HO', 'name' => 'Home'],
            ['code' => 'OF', 'name' => 'Office'],
            ['code' => 'OT', 'name' => 'Other'],
        );
        /**************************************************************** */
        $response['google_maps_api_key'] = $settings->customer_app_google_maps_api_key;
        /**************************************************************** */
        $response['urls'] = [
            'terms_and_conditions' => url('webview/terms_and_conditions'),
            'privacy_policy' => url('webview/privacy_policy'),
            'about_us' => url('webview/about_us'),
            'customer_info' => url('webview/customer_info'),
            'material_info' => url('webview/material_info'),
            'apps' => [
                'android' => 'https://play.google.com/store/apps/details?id=com.azinova.homemaid&hl=en&pli=1',
                'ios' => 'https://apps.apple.com/in/app/homemaids/id868052788'
            ]
        ];
        /**************************************************************** */
        // $response['checkout_data'] = [
        //     // DON'T include sensitive data here
        //     'checkout_primary_key' => Config::get('values.checkout_primary_key'),
        //     'checkout_token_url' => Config::get('values.checkout_token_url'),
        // ];
        /**************************************************************** */
        $response['otp_expire_seconds'] = Config::get('values.customer_login_otp_expire_seconds');
        $response['input'] = $input;
        $response['status'] = 'success';
        $response['message'] = 'Data fetched successfully.';
        return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);
    }
}