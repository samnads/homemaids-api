<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\DayServices;
use App\Models\MaidRating;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Response;
use stdClass;

class CustomerApiRatingController extends Controller
{
    public function rate_booking(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
        } else {
            // test input
            $data['params']['id'] = 1; // match with middleware for test
            $data['params']['schedule_ref'] = 263850;
            $data['params']['rate_service'] = 4;
            $data['params']['rate_professional'] = 3;
            $data['params']['comment'] = "goodgh";
        }
        /************************************************************* */
        // required input check
        $input = @$data['params'];
        $validator = Validator::make((array) $input,
            [
                'schedule_ref' => 'required|integer',
                'rate_service' => 'required|numeric|between:1,5',
                'rate_professional' => 'required|numeric|between:1,5',
                'comment' => 'required|string|min:3',
            ],
            [],
            [
                'schedule_ref' => 'Schedule Ref. ID',
                'rate_service' => 'Service Rating',
                'rate_professional' => 'Prof. Rating',
                'comment' => 'Comment',
            ]
        );
        if ($validator->fails()) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        /************************************************************* */
        $day_service = DayServices::where([['day_service_id', '=', $input['schedule_ref']], ['customer_id', '=', $input['id']]])->first();
        if ($day_service) {
            if($day_service->service_status != 2){
                // rating only possible after service completed
                return Response::json(array('result' => array('status' => 'failed', 'message' => 'Service not completed.'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
            }
            $day_service->rating = $input['rate_service'];
            $day_service->comments = $input['comment'];
            $day_service->rate_added_date = Carbon::now()->format('Y-m-d');
            $day_service->save();
            $booking = Booking::where([['booking_id', '=', $day_service->booking_id]])->first();
            $maid_rating = MaidRating::where([['day_service_id', '=', $input['schedule_ref']]])->first();
            if ($maid_rating) {
                $maid_rating->maid_id = $booking->maid_id;
                $maid_rating->service_type_id = $booking->service_type_id;
                $maid_rating->rated_by_customer_id = $input['id'];
                $maid_rating->day_service_id = $input['schedule_ref'];
                $maid_rating->rating = $input['rate_professional'];
                $maid_rating->updated_at = Carbon::now();
                $maid_rating->save();
            } else {
                $maid_rating = new stdClass();
                $maid_rating->maid_id = $booking->maid_id;
                $maid_rating->service_type_id = $booking->service_type_id;
                $maid_rating->rated_by_customer_id = $input['id'];
                $maid_rating->day_service_id = $input['schedule_ref'];
                $maid_rating->rating = $input['rate_professional'];
                $maid_rating->created_at = Carbon::now();
                $maid_rating->updated_at = Carbon::now();
                $id = MaidRating::insertGetId((array) $maid_rating);
            }
            return Response::json(array('result' => array('status' => 'success', 'message' => 'Rating added successfully.'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        } else {
            return Response::json(array('result' => array('status' => 'failed', 'message' => 'Service not found.'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
    }
}
