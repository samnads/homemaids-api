<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\CouponCode;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Response;
use stdClass;

class CustomerApiCouponController extends Controller
{
    public function coupon_list(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
        } else {
            // test input
        }
        /************************************************************* */
        // required input check
        $input = @$data['params'];
        /************************************************************* */
        $response['status'] = 'success';
        $response['available_codelist'] = CouponCode::select(
            'coupon_id as code_id',
            'coupon_name as promocode',
            'coupon_name as code_details',
            'expiry_date as validity',
            DB::raw('"any" as coupon_type'),
            DB::raw('(CASE WHEN discount_type = 0 THEN "percentage" ELSE "amount" END) as discount_type'),
            DB::raw('(CASE WHEN coupon_type = "FT" THEN true ELSE false END) as limited'),
        )
            ->where([['status', '=', 1]])
            ->orderBy('expiry_date', 'DESC')
            ->get();
        $response['message'] = sizeof($response['available_codelist']) ? "Coupons fetched successfully." : "No coupons found.";
        return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
    }
}
