<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Card;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Response;
use stdClass;

class CustomerApiCardController extends Controller
{
    public function add_card(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
        } else {
            // test input
            $data['params']['id'] = 1;
            $data['params']['card_number'] = "4242424242424242";
            $data['params']['nickname'] = "Test Card";
            $data['params']['card_expiry'] = "11/23";
            $data['params']['cvv'] = "517";
        }
        /************************************************************* */
        // required input check
        $input = @$data['params'];
        $validator = Validator::make((array) $input,
            [
                'card_number' => 'required|integer',
                'nickname' => 'required|string',
                'card_expiry' => 'required|date_format:m/y|after:' . date('m/y', strtotime("-1 month")), // current month allowed
                'cvv' => 'nullable|min:6|integer',
            ],
            [],
            [
                'card_number' => 'Card Number',
                'nickname' => 'Nick Name',
                'card_expiry' => 'Expiry Date',
                'cvv' => 'CVV',
            ]
        );
        if ($validator->fails()) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        /************************************************************* */
        $response['status'] = 'success';
        $response['message'] = 'Card added successfully.';
        $card = new stdClass();
        $card->customer_id = $input['id'];
        $card->nick_name = $input['nickname'];
        $card->card_number = $input['card_number'];
        $endOfMonth = Carbon::createFromFormat('m/y', $input['card_expiry'])->endOfMonth()->format('d');
        $card->expiry_date = Carbon::createFromFormat('m/y', $input['card_expiry'])->format('Y-m-' . $endOfMonth);
        $card->cvv = $input['cvv'] ?: null;
        $id = Card::insertGetId((array) $card);
        return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
    }
    public function card_details(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
        } else {
            // test input
            $data['params']['id'] = 1; // match with middleware for test
        }
        /************************************************************* */
        $input = @$data['params'];
        /************************************************************* */
        $response['status'] = 'success';
        $response['card_list'] = DB::table('customer_cards as cc')
            ->select(
                'cc.id as card_id',
                'cc.card_number',
                'cc.nick_name',
                'cc.expiry_date as expiry_date',
                //DB::raw('DATE_FORMAT(cc.expiry_date,"%m") as expiry_month'),
                //DB::raw('DATE_FORMAT(cc.expiry_date,"%Y") as expiry_year'),
                'cc.cvv',
            )
            ->where([['cc.customer_id', '=', $input['id']], ['cc.deleted_at', '=', null]])
            ->orderBy('cc.id', 'DESC')
            ->get();
        $response['message'] = sizeof($response['card_list']) ? "Cards fetched successfully." : "No cards found.";
        return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
    }
    public function delete_card(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
        } else {
            // test input
            $data['params']['id'] = 1; // match with middleware for test
            $data['params']['card_id'] = 22;
        }
        /************************************************************* */
        $input = @$data['params'];
        $validator = Validator::make(
            (array) $input,
            [
                'card_id' => 'required|numeric',
            ],
            [],
            [
                'card_id' => 'Card ID',
            ]
        );
        if ($validator->fails()) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        /************************************************************* */
        $card = Card::where([['customer_id', '=', $input['id']], ['id', '=', $input['card_id']], ['deleted_at', '=', null]])->first();
        if ($card) {
            $card->deleted_at = Carbon::now();
            $card->save();
            return Response::json(array('result' => array('status' => 'success', 'message' => 'Card removed successfully.'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        } else {
            return Response::json(array('result' => array('status' => 'failed', 'message' => 'Card not found.'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
    }
    public function link_card(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
        } else {
            // test input
            $data['params']['id'] = 1; // match with middleware for test
            $data['params']['booking_id'] = 4733;
            $data['params']['card_id'] = 23;
        }
        /************************************************************* */
        $input = @$data['params'];
        $validator = Validator::make(
            (array) $input,
            [
                'booking_id' => 'required|integer',
                'card_id' => 'required|integer',
            ],
            [],
            [
                'booking_id' => 'Booking ID',
                'card_id' => 'Card ID',
            ]
        );
        if ($validator->fails()) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        /************************************************************* */
        $card = Card::where([['customer_id', '=', $input['id']], ['id', '=', $input['card_id']], ['deleted_at', '=', null]])->first();
        if ($card) {
            $booking = Booking::where([['booking_id', '=', $input['booking_id']], ['customer_id', '=', $input['id']], ['booking_status', '=', 1]])->first();
            if ($booking) {
                $booking->customer_card_id = $input['card_id'];
                $booking->save();
                return Response::json(array('result' => array('status' => 'success', 'message' => 'Card linked to booking successfully.'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
            } else {
                return Response::json(array('result' => array('status' => 'failed', 'message' => 'Booking not found.'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
            }
        } else {
            return Response::json(array('result' => array('status' => 'failed', 'message' => 'Card not found.'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
    }
}
