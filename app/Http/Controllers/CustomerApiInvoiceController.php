<?php

namespace App\Http\Controllers;

use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\Customer;
use App\Models\Booking;
use Response;
use Redirect;
use stdClass;

class CustomerApiInvoiceController extends Controller
{
    public function invoice_payment(Request $request)
    {
        try {
            $debug = toggleDebug(); // pass boolean to overide default
            $data = json_decode($request->getContent(), true);
            $input = @$data['params'];
            /************************************************************* */
            $validator = Validator::make(
                (array) $input,
                [
                    'payment_method' => 'required|in:2,3,4',
                    'customer_id' => 'required|integer',
                    'order_id' => 'required|string',
                    'amount' => 'required',
                    'platform' => 'required|in:web',
                ],
                [],
                [
                    'payment_method' => 'Payment Mode',
                    'customer_id' => 'Customer ID',
                    'order_id' => 'Order ID',
                    'amount' => 'Amount',
                    'platform' => 'Platform',
                ]
            );
            if ($validator->fails()) {
                return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
            }
            $response['status'] = 'success';
            /************************************************************* */
            if ($input['payment_method'] == 2) {
                /******************************************************************************
                 *
                 * 
                 * Request contains checkout card token data
                 * 
                 * 
                 **************************************************************************** */
                if (@$input['checkout_token_data']) {
                    // have checkout token data in request
                    $response['checkout_token_data'] = $input['checkout_token_data'];
                    $checkout_data = $this->checkoutPaymentForInvoice($input['checkout_token_data'], $input);
                    $response['checkout_data'] = $checkout_data;
                    if (strtolower(@$checkout_data['status']) == "pending") {
                        // have redirect link to enter OTP
                    } else if (@$checkout_data['approved'] == true) {
                        // payment success
                        //afterCheckoutPaymentSuccess($bookings[0]->booking_id, $checkout_data, @$input['platform']);
                    } else {
                        throw new \ErrorException('Card payment not successful.');
                    }
                } else {
                    throw new \ErrorException('No token data.');
                }
                /****************************************************************************** */
            }
            $response['$input'] = $input;
            return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), JSON_PRETTY_PRINT);
        } catch (\Exception $e) {
            DB::rollback();
            return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
    }
    function checkoutPaymentForInvoice($checkout_data, $input)
    {
        $customer = Customer::where('customer_id', $input['customer_id'])->first();
        $success_url = url('api/customer/invoice-payment/checkout/process-and-redirect/' . $input['order_id']); // for 3Ds only
        $failure_url = url('api/customer/invoice-payment/checkout/process-and-redirect/' . $input['order_id']); // for 3Ds only
        try {
            $client = new \GuzzleHttp\Client([
                'verify' => false,
            ]);
            /**************************************************************** */
            $post_header = ["Authorization" => "Bearer " . Config::get('values.checkout_secret_key'), "Content-Type" => "application/json", "Accept" => "application/json"];
            $checkout_data = [
                'source' => ["type" => "token", "token" => $checkout_data['token']],
                'customer' => ["email" => @$customer->email_address, "name" => @$customer->customer_name],
                '3ds' => ["enabled" => true, "attempt_n3d" => true],
                'amount' => ((float) $input['amount']) * 100,
                //'amount' => ((float) 1) * 100, // TEST PURPOSE ONLY
                'currency' => Config::get('values.currency_code'),
                'processing_channel_id' => Config::get('values.checkout_channel_id'),
                "success_url" => $success_url,
                "failure_url" => $failure_url,
                'reference' => $input['order_id'],
                'metadata' => ['coupon_code' => 'test']
            ];
            /**************************************************************** */
            $response_payment_data = $client->request('POST', Config::get('values.checkout_service_url'), [
                'headers' => $post_header,
                'http_errors' => false, // no exceptin fix
                'json' => $checkout_data,
            ]);
            return json_decode((string) $response_payment_data->getBody(), true);
        } catch (\Exception $e) {
            throw new \ErrorException('Processing payment failed.' . $e->getMessage());
        }
    }
    public function checkoutInvoicePaymentProcessAndRedirect(Request $request)
    {
        // this (route) should be called from checkout.com
        $segments = $request->segments();
        $booking_common_id = $segments[5];//order_id
        $booking = Booking::where('booking_id', $booking_common_id)->first();
        try {
            $bookings = DB::table('bookings as b')
                ->select(
                    'b.*',
                )
                ->where(function ($query) use ($booking_common_id) {
                    $query->where('b.booking_common_id', $booking_common_id);
                    $query->orWhere('b.booking_id', $booking_common_id);
                })
                ->orderBy('b.booking_id', 'ASC')
                ->get();
            $bookings_ids = array_column($bookings->toArray(), 'booking_id');
            $post_header = array("Authorization" => "Bearer " . Config::get('values.checkout_secret_key'), "Content-Type" => "application/json", "Accept" => "application/json");
            $client = new \GuzzleHttp\Client([
                'verify' => false,
            ]);
            $responseFull = $client->request('GET', Config::get('values.checkout_service_url') . $request['cko-session-id'], [
                'headers' => $post_header,
            ]);
            $checkout_data = json_decode((string) $responseFull->getBody(), true);
            //dd($checkout_data);
            if (@$checkout_data['approved'] == true) {
                // payment success
                afterCheckoutPaymentSuccess($booking_common_id, $checkout_data, null);
                return Redirect::to(Config::get('values.web_app_url') . 'booking/success/' . $booking->reference_id);
            } else {
                // payment failed
                return Redirect::to(Config::get('values.web_app_url') . 'booking/failed/' . $booking->reference_id);
            }
        } catch (\Exception $e) {
            // payment failed
            return Redirect::to(Config::get('values.web_app_url') . 'booking/failed/' . $booking->reference_id . '?error=' . $e->getMessage());
        }
    }
}
