<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WebviewStaticPageController extends Controller
{
    public function terms_and_conditions(Request $request)
    {
        return view('webview.terms_and_conditions', []);
    }
    public function privacy_policy(Request $request)
    {
        return view('webview.privacy_policy', []);
    }
    public function about_us(Request $request)
    {
        return view('webview.about_us', []);
    }
    public function customer_info(Request $request)
    {
        return view('webview.customer_info', []);
    }
    public function material_info(Request $request)
    {
        return view('webview.material_info', []);
    }
}
