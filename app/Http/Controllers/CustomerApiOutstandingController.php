<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Carbon\Carbon;
use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;

class CustomerApiOutstandingController extends Controller
{
    public function outstanding_amount(Request $request)
    {
        try {
            $data = json_decode($request->getContent(), true);
            /**
             * TEST DATA
             */
            //$data['params']['id'] = 32592; // test
            /************************************************************* */
            $input = @$data['params'];
            /************************************************************* */
            $customer = Customer::where('customer_id', $input['id'])->first();
            if (@$customer) {
                $odooid = $customer->odoo_package_customer_id;
                try {
                    $post['params']['user_id'] = 1; // 1 - Don't change !
                    if(isDemo() === true){
                        $odooid = 25242; // test
                    }
                    $post['params']['id'] = $odooid;
                    $post_values = json_encode($post);
                    $ServiceURL = "http://limshq.fortiddns.com:8015/customer_outstanding";
                    $orderCreateResponse = curlFunOdoo($post_values, $ServiceURL);
                    $responseBody_odoo = json_decode($orderCreateResponse, true);
                    if ($responseBody_odoo['result']['status'] == "success") {
                        $bal_amt = $responseBody_odoo['result']['response'][0]['balance'];
                        if ($bal_amt > 0) {
                            $unbilled_amount = number_format((float) $responseBody_odoo['result']['response'][0]['unbilled_invoice'], 2, '.', '');
                            $outstanding_amount = number_format((float) $bal_amt, 2, '.', '');
                            $last_invoice_date = date('d-m-Y', strtotime($responseBody_odoo['result']['response'][0]['last_invoice_date']));
                            $response['status'] = 'success';
                            $response['message'] = "Details fetched successfully.";
                            $response['demo'] = isDemo();
                            $response['live'] = isLive();
                            $response['odooid'] = $odooid;
                            $response['details']['outstanding_amount'] = $outstanding_amount;
                            $response['details']['last_billed_date'] = $last_invoice_date;
                            $response['details']['unbilled_amount'] = $unbilled_amount;
                            $response['details']['payment_url'] = url('outstanding/gateway/checkout/entry?customer_id=' . $input['id']);
                            $response['details']['payment_success_url'] = url('api/customer/payment/payfort/success');
                            $response['details']['payment_failed_url'] = url('api/customer/payment/payfort/failed');
                            return Response::json(array('result' => $response), 200, array(), customerResponseJsonConstants());
                        } else if ($bal_amt == 0) {
                            $unbilled_amount = number_format((float) $responseBody_odoo['result']['response'][0]['unbilled_invoice'], 2, '.', '');
                            $outstanding_amount = number_format((float) $bal_amt, 2, '.', '');
                            $last_invoice_date = $responseBody_odoo['result']['response'][0]['last_invoice_date'] ? date('d-m-Y', strtotime($responseBody_odoo['result']['response'][0]['last_invoice_date'])) : 'today';
                            $response['status'] = 'success';
                            $response['message'] = "Details fetched successfully.";
                            $response['demo'] = isDemo();
                            $response['live'] = isLive();
                            $response['odooid'] = $odooid;
                            $response['details']['outstanding_amount'] = $outstanding_amount;
                            $response['details']['last_billed_date'] = $last_invoice_date;
                            $response['details']['unbilled_amount'] = $unbilled_amount;
                            $response['details']['payment_url'] = url('outstanding/gateway/checkout/entry?customer_id=' . $input['id']);
                            $response['details']['payment_success_url'] = url('api/customer/payment/payfort/success');
                            $response['details']['payment_failed_url'] = url('api/customer/payment/payfort/failed');
                            return Response::json(array('result' => $response), 200, array(), customerResponseJsonConstants());
                            //return Response::json(array('result' => array('status' => 'failed', 'message' => 'No outstanding amount.')), 200, array(), customerResponseJsonConstants());
                        } else {
                            return Response::json(array('result' => array('status' => 'failed', 'message' => 'Something went wrong. Try again (001).')), 200, array(), customerResponseJsonConstants());
                        }
                    } else {
                        return Response::json(array('result' => array('status' => 'failed', 'message' => 'Something went wrong. Try again (002).')), 200, array(), customerResponseJsonConstants());
                    }
                } catch (\Exception $e) {
                    return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage().' (003).')), 200, array(), customerResponseJsonConstants());
                }
            } else {
                return Response::json(array('result' => array('status' => 'failed', 'message' => 'Customer does not exist.')), 200, array(), customerResponseJsonConstants());
            }
        } catch (\Exception $e) {
            DB::rollback();
            return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage() . $e->getLine())), 200, array(), customerResponseJsonConstants());
        }
    }
}
