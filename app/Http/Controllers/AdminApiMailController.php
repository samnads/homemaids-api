<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Customer;
use App\Models\CustomerAddress;
use App\Models\OnlinePayment;
use Carbon\Carbon;
use Config;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Mail;
use Response;
use App\Models\Settings;
use App\Models\BookingDeleteRemarks;
use stdClass;

class AdminApiMailController extends Controller
{
    public function schedule_cancel_to_admin($booking_id, $service_date)
    {
        /**
         * 
         * send schedule cancellation mail to admin for a specific service date
         * 
         */
        try {
            $settings = Settings::first();
            /******************************************************************************************* */
            $bookings = DB::table('bookings as b')
                ->select(
                    'b.*',
                    DB::raw('(CASE WHEN (b.reference_id IS NULL OR b.reference_id = "") THEN b.booking_id ELSE b.reference_id END) as reference_id'),
                    DB::raw('IFNULL(st.customer_app_service_type_name,st.service_type_name) as service_type_name'),
                    'st.service_type_model_id',
                    'stm.model as service_type_model',
                    'm.maid_id',
                    'm.maid_name',
                    DB::raw('(CASE WHEN b.cleaning_material = "Y" THEN "Yes" ELSE "No" END) as cleaning_material_opted'),
                    'f.name as frequency',
                    'ca.customer_address as address',
                    DB::raw('"' . Config::get('values.currency_code') . '" as currency'),
                    DB::raw('(CASE WHEN b.payment_type_id IS NULL THEN b.pay_by ELSE pt.name END) as payment_method'),
                    'pt.charge as payment_type_charge',
                    DB::raw('DAYNAME(b.service_start_date) as service_week_day_name'),
                    'op.transaction_id as op_transaction_id',
                    'op.payment_status as op_payment_status'
                )
                ->leftJoin('service_types as st', 'b.service_type_id', 'st.service_type_id')
                ->leftJoin('service_type_models as stm', 'st.service_type_model_id', 'stm.id')
                ->leftJoin('maids as m', 'b.maid_id', 'm.maid_id')
                ->leftJoin('customers as c', 'b.customer_id', 'c.customer_id')
                ->leftJoin('frequencies as f', 'b.booking_type', 'f.code')
                ->leftJoin('payment_types as pt', 'b.payment_type_id', 'pt.id')
                ->leftJoin('customer_addresses as ca', 'b.customer_address_id', 'ca.customer_address_id')
                ->leftJoin('online_payments as op', function ($join) {
                    //$join->on('b.booking_id', '=', 'op.booking_id');
                    $join->whereColumn('op.reference_id', 'b.reference_id');
                    $join->where('op.payment_status', 'success');
                })
                ->where(function ($query) use ($booking_id) {
                    $query->where('b.booking_common_id', $booking_id);
                    $query->orWhere('b.booking_id', $booking_id);
                })
                ->orderBy('b.booking_id', 'ASC')
                ->get();
            $booking = $bookings[0];
            /******************************************************************************************* */
            $customer = Customer::where('customer_id', '=', $booking->customer_id)->first();
            /******************************************************************************************* */
            $customer_address = CustomerAddress::
                select(
                    'customer_addresses.*',
                    'a.area_name',
                )
                ->leftJoin('areas as a', 'customer_addresses.area_id', 'a.area_id')->where('customer_address_id', '=', $booking->customer_address_id)->first();
            /******************************************************************************************* */
            Mail::send(
                'emails.booking-cancel-to-admin',
                [
                    'settings' => $settings,
                    'greeting' => 'Dear Admin',
                    'text_body' => 'New cancellation received.',
                    'booking' => $booking,
                    'bookings' => $bookings,
                    'customer' => $customer,
                    'customer_address' => $customer_address,
                    'service_date' => $service_date,
                    'test' => isset($_GET['test'])
                ],
                function ($m) use ($booking) {
                    $m->from(Config::get('mail.mail_from_address'), Config::get('mail.mail_from_name'));
                    $m->to(Config::get('mail.mail_to_admin_address'), Config::get('mail.mail_to_admin_address_name'));
                    $m->bcc('samnad.s@azinova.info', 'Samnad S - Developer');
                    $m->subject('Booking Cancelled ' . $booking->reference_id);
                }
            );
            /******************************************************************************************* */
        } catch (\Exception $e) {
            //throw new \ErrorException($e->getMessage());
        }
    }
    public function schedule_cancel_to_customer($booking_id, $service_date)
    {
        /**
         * 
         * send schedule cancellation mail to customer for a specific service date
         * 
         */
        try {
            $settings = Settings::first();
            /******************************************************************************************* */
            $bookings = DB::table('bookings as b')
                ->select(
                    'b.*',
                    DB::raw('(CASE WHEN (b.reference_id IS NULL OR b.reference_id = "") THEN b.booking_id ELSE b.reference_id END) as reference_id'),
                    DB::raw('IFNULL(st.customer_app_service_type_name,st.service_type_name) as service_type_name'),
                    'st.service_type_model_id',
                    'stm.model as service_type_model',
                    'm.maid_id',
                    'm.maid_name',
                    DB::raw('(CASE WHEN b.cleaning_material = "Y" THEN "Yes" ELSE "No" END) as cleaning_material_opted'),
                    'f.name as frequency',
                    'ca.customer_address as address',
                    DB::raw('"' . Config::get('values.currency_code') . '" as currency'),
                    DB::raw('(CASE WHEN b.payment_type_id IS NULL THEN b.pay_by ELSE pt.name END) as payment_method'),
                    'pt.charge as payment_type_charge',
                    DB::raw('DAYNAME(b.service_start_date) as service_week_day_name'),
                    'op.transaction_id as op_transaction_id',
                    'op.payment_status as op_payment_status'
                )
                ->leftJoin('service_types as st', 'b.service_type_id', 'st.service_type_id')
                ->leftJoin('service_type_models as stm', 'st.service_type_model_id', 'stm.id')
                ->leftJoin('maids as m', 'b.maid_id', 'm.maid_id')
                ->leftJoin('customers as c', 'b.customer_id', 'c.customer_id')
                ->leftJoin('frequencies as f', 'b.booking_type', 'f.code')
                ->leftJoin('payment_types as pt', 'b.payment_type_id', 'pt.id')
                ->leftJoin('customer_addresses as ca', 'b.customer_address_id', 'ca.customer_address_id')
                ->leftJoin('online_payments as op', function ($join) {
                    //$join->on('b.booking_id', '=', 'op.booking_id');
                    $join->whereColumn('op.reference_id', 'b.reference_id');
                    $join->where('op.payment_status', 'success');
                })
                ->where(function ($query) use ($booking_id) {
                    $query->where('b.booking_common_id', $booking_id);
                    $query->orWhere('b.booking_id', $booking_id);
                })
                ->orderBy('b.booking_id', 'ASC')
                ->get();
            $booking = $bookings[0];
            /******************************************************************************************* */
            $customer = Customer::where('customer_id', '=', $booking->customer_id)->first();
            /******************************************************************************************* */
            $customer_address = CustomerAddress::
                select(
                    'customer_addresses.*',
                    'a.area_name',
                )
                ->leftJoin('areas as a', 'customer_addresses.area_id', 'a.area_id')->where('customer_address_id', '=', $booking->customer_address_id)->first();
            /******************************************************************************************* */
            if ($customer->email_address != '') {
                Mail::send(
                    'emails.booking-cancel-to-customer',
                    [
                        'settings' => $settings,
                        'greeting' => 'Dear ' . $customer->customer_name,
                        'text_body' => 'Your booking with Reference No. ' . $booking->reference_id . ' on service date ' . Carbon::createFromFormat('Y-m-d', @$service_date)->format('d M Y') . ' has been cancelled successfully.',
                        'booking' => $booking,
                        'bookings' => $bookings,
                        'customer' => $customer,
                        'customer_address' => $customer_address,
                        'service_date' => $service_date,
                        'test' => isset($_GET['test'])
                    ],
                    function ($m) use ($booking, $customer) {
                        $m->from(Config::get('mail.mail_from_address'), Config::get('mail.mail_from_name'));
                        $m->to($customer->email_address, $customer->customer_name);
                        $m->bcc('samnad.s@azinova.info', 'Samnad S - Developer');
                        $m->subject('Booking Cancelled ' . $booking->reference_id);
                    }
                );
            }
            /******************************************************************************************* */
        } catch (\Exception $e) {
            //throw new \ErrorException($e->getMessage());
        }
    }
    public function registration_success_to_customer($customer_id)
    {
        /**
         * 
         * send registration confirmation mail to customer
         * 
         */
        try {
            $settings = Settings::first();
            $customer = Customer::where('customer_id', '=', $customer_id)->first();
            if ($customer->email_address != '') {
                Mail::send(
                    'emails.registration-success-to-customer',
                    [
                        'settings' => $settings,
                        'greeting' => 'Dear ' . $customer->customer_name,
                        'text_body' => 'Thank you for registration, you\'ve successfully registered on ' . Config::get('values.company_name') . '.',
                        'test' => isset($_GET['test'])
                    ],
                    function ($m) use ($customer) {
                        $m->from(Config::get('mail.mail_from_address'), Config::get('mail.mail_from_name'));
                        $m->to($customer->email_address, $customer->customer_name);
                        $m->bcc('samnad.s@azinova.info', 'Samnad S - Developer');
                        $m->subject("Registration Confirmed - Let's Get Started!");
                    }
                );
            }
        } catch (\Exception $e) {
            //throw new \ErrorException($e->getMessage());
        }
    }

    public function online_payment_confirm_to_customer($payment_id = null, Request $request)
    {
        try {
            /******************************************************************************************* */
            $payment_id = @$request->payment_id ?: $payment_id;
            $validator = Validator::make(
                [
                    'payment_id' => $payment_id,
                ],
                [
                    'payment_id' => 'required|integer',
                ],
                [],
                [
                    'payment_id' => 'Payment ID',
                ]
            );
            if ($validator->fails()) {
                return Response::json(['status' => false, 'message' => $validator->errors()->first()], 200, [], JSON_PRETTY_PRINT);
            }
            /******************************************************************************************* */
            $settings = Settings::first();
            $online_payment = OnlinePayment::find($payment_id);
            $customer = Customer::where('customer_id', '=', $online_payment['customer_id'])->first();
            /******************************************************************************************* */
            if (trim($customer->email_address) != "") {
                Mail::send(
                    'emails.online_payment_confirm_to_customer',
                    [
                        'settings' => $settings,
                        'greeting' => 'Dear ' . $customer->customer_name,
                        'text_body' => 'Your payment of <b>' . $online_payment['amount'] . '  AED</b> is received.<br>Thank you!',
                        'online_payment' => $online_payment,
                        'test' => isset($_GET['test'])
                    ],
                    function ($m) use ($customer, $online_payment) {
                        $m->from(Config::get('mail.mail_from_address'), Config::get('mail.mail_from_name'));
                        $m->to($customer->email_address, $customer->customer_name);
                        //$m->bcc('samnad.s@azinova.info', 'Samnad S - Azinova Developer');
                        $m->subject('Payment Completed #' . $online_payment['transaction_id']);
                    }
                );
            } else {
                return Response::json(['status' => false, 'message' => 'No email address found for customer.'], 200, [], JSON_PRETTY_PRINT);
            }
            /******************************************************************************************* */
            return Response::json(['status' => true, 'message' => 'Mail send succesfully.'], 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $e) {
            return Response::json(['status' => false, 'message' => $e->getMessage()], 200, [], JSON_PRETTY_PRINT);
        }
    }
    public function online_payment_confirm_to_admin($payment_id = null, Request $request)
    {
        try {
            /******************************************************************************************* */
            $payment_id = @$request->payment_id ?: $payment_id;
            $validator = Validator::make(
                [
                    'payment_id' => $payment_id,
                ],
                [
                    'payment_id' => 'required|integer',
                ],
                [],
                [
                    'payment_id' => 'Payment ID',
                ]
            );
            if ($validator->fails()) {
                return Response::json(['status' => false, 'message' => $validator->errors()->first()], 200, [], JSON_PRETTY_PRINT);
            }
            /******************************************************************************************* */
            $settings = Settings::first();
            $online_payment = OnlinePayment::find($payment_id);
            $customer = Customer::where('customer_id', '=', $online_payment['customer_id'])->first();
            $customer_address = CustomerAddress::
                select(
                    'customer_addresses.*',
                    'a.area_name',
                )
                ->leftJoin('areas as a', 'customer_addresses.area_id', 'a.area_id')
                ->where('customer_id', '=', $customer->customer_id)
                ->where('default_address', 1)
                ->first();
            /******************************************************************************************* */
            if (trim($customer->email_address) != "") {
                Mail::send(
                    'emails.online_payment_confirm_to_admin',
                    [
                        'settings' => $settings,
                        'greeting' => 'Dear Admin',
                        'text_body' => 'New payment of <b>' . $online_payment['amount'] . '  AED</b> received from customer <strong>'. $customer->customer_name.'</strong>.',
                        'customer' => $customer,
                        'customer_address'=>$customer_address,
                        'online_payment' => $online_payment,
                        'test' => isset($_GET['test'])
                    ],
                    function ($m) use ($customer, $online_payment) {
                        $m->from(Config::get('mail.mail_from_address'), Config::get('mail.mail_from_name'));
                        $m->to($customer->email_address, $customer->customer_name);
                        //$m->bcc('samnad.s@azinova.info', 'Samnad S - Azinova Developer');
                        $m->subject('Payment Received #' . $online_payment['transaction_id']);
                    }
                );
            } else {
                return Response::json(['status' => false, 'message' => 'No email address found for customer.'], 200, [], JSON_PRETTY_PRINT);
            }
            /******************************************************************************************* */
            return Response::json(['status' => true, 'message' => 'Mail send succesfully.'], 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $e) {
            return Response::json(['status' => false, 'message' => $e->getMessage()], 200, [], JSON_PRETTY_PRINT);
        }
    }
}
