<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\BookingAddons;
use App\Models\BookingPackages;
use App\Models\SubscriptionPackages;
use App\Models\BookingSubscriptionPackages;
use App\Models\BookingCleaningMaterial;
use App\Models\Customer;
use App\Models\CustomerAddress;
use App\Models\CustomerCoupons;
use App\Models\ServiceAddons;
use Carbon\Carbon;
use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Response;
use stdClass;

class CustomerApiBookingCreateController extends Controller
{
    public function create_booking(Request $request)
    {
        try {
            set_time_limit(300);
            $debug = toggleDebug(); // pass boolean to overide default
            /************************************************************* */
            if (!$debug) {
                // live input
                $data = json_decode($request->getContent(), true);
                /////////////////////////////////
                $input = @$data['params'];
                //$input = @$data['params']['calculation_data']['input'];
                if ($input['id'] == 695) {
                    //return Response::json(array('result' => array('status' => 'failed', 'message' => 'TEST COUPON_CODE = ' . (@$input['coupon_code'])), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
                }
            } else {
                // test input
                $data['params']['id'] = Config::get('values.debug_customer_id');
                $data['params']['service_type_id'] = 1;
                $data['params']['date'] = "22/04/2024"; // dd/mm/yyyy format
                $data['params']['frequency'] = "OD";
                $data['params']['time'] = "18:00";
                $data['params']['hours'] = 6;
                $data['params']['professionals_count'] = 1;
                $professional_prefer = array(
                    // currently allowing same hours for all profs.
                    //array('professional_id' => 2),
                    //array('professional_id' => 3),
                );
                $data['params']['professional_prefer'] = $professional_prefer;
                $data['params']['address_id'] = Config::get('values.debug_customer_address_id');
                $data['params']['instructions'] = "TEST : Yes, I would like to have the lady that came to my house on the 28th at 4:30pm. DO NOT SEND the ladies who cleaned my apartment on the 28th in the morning. Thank you.";
                $data['params']['coupon_code'] = 'WEEKLY';
                $data['params']['payment_method'] = 2;
                //$data['params']['packages'] = [array('package_id' => 1, 'quantity' => 2), array('package_id' => 3, 'quantity' => 1)];
                //$data['params']['packages'] = [];
                //$data['params']['addons'] = [array('service_addons_id' => 1, 'quantity' => 5), array('service_addons_id' => 2, 'quantity' => 1)];
                //$data['params']['addons'] = [];
                //$data['params']['subscription_package_id'] = 4;
                $data['params']['platform'] = 'mobile'; //mobile|web
                $data['params']['cleaning_materials'] = true;
                //$data['params']['materials'] = [['id' => 1], ['id' => 2]];
                /////////////////////////////////
                $input = @$data['params'];
            }
            /************************************************************* */
            // required input check
            $input['frequency'] = @$input['frequency'] ?: 'OD';
            /************************************************************* */
            $validator = Validator::make(
                (array) $input,
                [
                    'frequency' => 'required|in:OD,WE,BW',
                    'date' => 'required|date_format:d/m/Y|after:' . date('d/m/Y', strtotime("-1 day")), // minimum date today
                    'time' => 'nullable|date_format:H:i',
                    'hours' => 'nullable|integer|min:2|in:2,4,6,8,10',
                    'professionals_count' => 'nullable|integer',
                    'professional_prefer' => 'array',
                    'cleaning_materials' => 'required|boolean',
                    'address_id' => 'required|integer|exists:customer_addresses,customer_address_id,address_status,0,customer_id,' . $input['id'],
                    'instructions' => 'nullable|string',
                    'coupon_code' => 'nullable|string',
                    'payment_method' => 'required|integer',
                    'service_type_id' => 'required|integer',
                    'packages' => 'array',
                    'addons' => 'array',
                    'subscription_package_id' => 'nullable|integer',
                    'platform' => 'required|in:mobile,web',
                ],
                [],
                [
                    'frequency' => 'Frequency',
                    'date' => 'Date',
                    'time' => 'Time',
                    'hours' => 'Hours',
                    'professionals_count' => 'Professionals Count',
                    'professional_prefer' => 'Selected Professionals',
                    'cleaning_materials' => 'Cleaning Materials',
                    'address_id' => 'Address',
                    'instructions' => 'Instructions',
                    'coupon_code' => 'Coupon Code',
                    'payment_method' => 'Payment Method',
                    'service_type_id' => 'Service Type ID',
                    'packages' => 'Packages',
                    'addons' => 'Addons',
                    'subscription_package_id' => 'Subscription Package ID',
                    'platform' => 'Platform',
                ]
            );
            if ($validator->fails()) {
                return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
            }
            /************************************************************* */
            // check customer have mandodatory data filled or not
            $customer = Customer::where('customer_id', '=', $input['id'])->first();
            /************************************************************* */
            $error = isCustomerHaveRequiredData($input['id'], $customer);
            if (count((array) $error)) {
                // check customer is allowed for booking
                return Response::json(array('result' => array('status' => 'failed', 'title' => $error->title, 'type' => $error->type, 'message' => $error->message), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
            }
            /************************************************************* */
            // check customer is allowed for booking
            if (($cusomer_error = isCustomerEligibleForBooking($input['id'])) !== true) {
                // check customer is allowed for booking
                return Response::json(array('result' => array('status' => 'failed', 'message' => $cusomer_error), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
            }
            /************************************************************* */
            $input['professional_prefer'] = array_unique(@$input['professional_prefer'] ?: [], SORT_REGULAR); // remove duplicates - rare case or app bug
            /*if (sizeof($input['professional_prefer']) > $input['professionals_count']) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => "Professional count mismatch."), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
            }*/
            /************************************************************* */
            $customer_address = CustomerAddress::select(
                'customer_addresses.*',
                DB::raw('CONCAT(customer_addresses.customer_address," (",a.area_name,")") as address'),
            )
                ->leftJoin('areas as a', 'customer_addresses.area_id', 'a.area_id')
                ->find($input['address_id']);
            if (($error = isAreaAllowedForBooking($customer_address->area_id)) !== true) {
                // check customer is allowed for booking
                return Response::json(array('result' => array('status' => 'failed', 'message' => $error, 'type' => "warning", 'title' => "Area not available"), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
            }
            $input['time'] = @$input['time'] ?: '08:00'; // default if no time found
            $input['date'] = Carbon::createFromFormat('d/m/Y', $input['date'])->format('Y-m-d');
            $input['time_from'] = Carbon::createFromFormat('H:i', $input['time'])->format('H:i:s');
            $calculate = calculate($input);
            $input['time_to'] = Carbon::createFromFormat('H:i:s', $input['time_from'])->addMinutes($calculate['service_hours'] * 60)->format('H:i:s');
            $input['frequency'] = @$calculate['frequency'] ?: 'OD';
            /****************************************************************************************** */
            /**
             * Custom hour requirements for areas
             */
            $four_hour_area_ids = [
                165, // Dubai Creek Harbour
                163, // Villanova,
                166, // NAD AL SHEBA 3
            ];
            if (in_array($customer_address->area_id, $four_hour_area_ids) && $input['hours'] < 4) {
                throw new \ErrorException('Sorry, selected area requires minimum 4 hours booking.');
            }
            /****************************************************************************************** */
            // real time time-slot availability check
            if (($error = is_time_slot_available($input['date'], $input['time_from'], $input['time_to'])) !== true) {
                //return Response::json(array('result' => array('status' => 'failed', 'message' => $error), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
            }
            /****************************************************************************************** */
            // real time maid availability check
            $booking = new stdClass();
            $booking->service_week_day = Carbon::parse($input['date'])->format('w');
            $booking->service_start_date = $input['date'];
            $booking->booking_type = $input['frequency'];
            $booking->time_from = $input['time_from'];
            $booking->time_to = $input['time_to'];
            if ($input['frequency'] == "OD") {
                $service_end_date = $input['date'];
                $service_actual_end_date = $input['date'];
                $service_end = 1; // it's one day
            } else if ($input['frequency'] == "WE") {
                if ($calculate['no_of_weeks'] > 0) {
                    $service_end = 1;
                    $service_end_date = Carbon::parse($input['date'])->addWeeks($calculate['no_of_weeks'] - 1)->format('Y-m-d');
                    $service_actual_end_date = $service_end_date;
                } else {
                    $service_end = 0; // never ends
                    $service_end_date = $input['date'];
                    $service_actual_end_date = $input['date'];
                }
            } else if ($input['frequency'] == "BW") {
                if ($calculate['no_of_weeks'] > 0) {
                    $service_end = 1; // end date available
                    $service_end_date = Carbon::parse($input['date'])->addWeeks(($calculate['no_of_weeks'] * 2) - 2)->format('Y-m-d');
                    $service_actual_end_date = $service_end_date;
                } else {
                    $service_end = 0; // never ends
                    $service_end_date = $input['date'];
                    $service_actual_end_date = $input['date'];
                }
            }
            $booking->service_actual_end_date = $service_actual_end_date;
            $booking->service_end = $service_end;
            /*************************************** */
            $busy_bookings = get_busy_bookings($booking, []);
            //dd(array_unique(array_column($busy_bookings->toArray(), 'booking_id')));
            if ($input['frequency'] == "BW") {
                // trick to exclude not overlapped bw bookings
                foreach ($busy_bookings->toArray() as $key => $busy_booking) {
                    if ($busy_booking->service_start_date_week_difference & 1) { // if odd number
                        // week diff odd number means it will not overlap other bw bookings, so remove it from timed bookings
                        unset($busy_bookings[$key]);
                    }
                }
            }
            $busy_maid_ids = array_unique(array_column($busy_bookings->toArray(), 'maid_id'));
            $available_maids = DB::table('maids as m')
                ->select(
                    'm.maid_id',
                    'm.maid_name as name',
                )
                ->where(['maid_status' => 1])
                ->whereNotIn('maid_id', $busy_maid_ids)
                ->orderBy('maid_name', 'ASC')
                ->get();
            $available_maid_ids = array_column($available_maids->toArray(), 'maid_id');
            $preferred_professional_ids = array_column($input['professional_prefer'], 'professional_id');
            /*************************************** */
            // check selected prof. availability
            //dd($available_maid_ids);
            $selected_professionals = [];
            $prof_validated = 0;
            foreach ($preferred_professional_ids as $key => $preferred_professional_id) {
                // manual professional select
                if (in_array($preferred_professional_id, $available_maid_ids)) {
                    // selected prof. available
                    $prof_validated += 1;
                    $selected_professionals[] = $preferred_professional_id;
                    unset($available_maid_ids[array_search($preferred_professional_id, $available_maid_ids)]); // remove from available list
                    $available_maid_ids = array_values($available_maid_ids); // reset array index keys
                } else {
                    // selected prof. not available
                    $maid = DB::table('maids as m')
                        ->select(
                            'm.maid_id',
                            'm.maid_name as name',
                        )
                        ->where(['maid_id' => $preferred_professional_id])
                        ->first();
                    return Response::json(array('result' => array('status' => 'failed', 'message' => "Selected maid '" . trim($maid->name) . "' is already booked by another customer, please go back and select other maid for selected time slot."), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
                    //return Response::json(array('result' => array('status' => 'failed', 'message' => "Slot for " . $maid->name . " is not available on " . Carbon::parse($input['date'])->format('d/m/Y') . " between " . Carbon::createFromFormat('H:i:s', $input['time_from'])->format('h:i A') . " - " . Carbon::createFromFormat('H:i:s', $input['time_to'])->format('h:i A') . "."), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
                }
            }
            //dd($available_maid_ids);
            /*************************************** */
            for ($i = $prof_validated; $i < $calculate['professionals_count']; $i++) {
                // auto assign professionals
                //die($input['professionals_count']);
                if (isset($available_maid_ids[0])) {
                    $selected_professionals[] = $available_maid_ids[0]; // auto assign first prof. id from list
                    unset($available_maid_ids[0]); // remove from available list
                    $available_maid_ids = array_values($available_maid_ids); // reset array index keys
                } else {
                    if (sizeof($selected_professionals) > 0) {
                        return Response::json(array('result' => array('status' => 'failed', 'message' => "Currently " . sizeof($selected_professionals) . " professional" . (sizeof($selected_professionals) > 1 ? "s" : "") . " only available."), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
                    } else {
                        return Response::json(array('result' => array('status' => 'failed', 'message' => "Sorry, there're no professionals available."), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
                    }
                }
            }
            /****************************************************************************************** */
            DB::beginTransaction();
            $service_start_date = $input['date'];
            /********************************************* */
            // insert addons to booking addons
            $booking_addons_ids = [];
            //dd($calculate['service_addons']);
            if (count($calculate['service_addons'])) {
                foreach ($calculate['service_addons'] as $key => $addon) {
                    $service_addon = ServiceAddons::findOrFail($addon['service_addons_id']);
                    $booking_addon = new stdClass();
                    $booking_addon->booking_id = null; // will update later
                    $booking_addon->service_addons_id = $addon['service_addons_id'];
                    $booking_addon->service_addon_name = $service_addon->service_addon_name;
                    $booking_addon->service_minutes = $service_addon->service_minutes;
                    $booking_addon->strike_amount = $service_addon->strike_amount;
                    $booking_addon->unit_price = $service_addon->amount;
                    $booking_addon->quantity = $addon['quantity'];
                    $booking_addon->amount = $booking_addon->unit_price * $booking_addon->quantity;
                    $booking_addons_ids[] = BookingAddons::insertGetId((array) $booking_addon);
                }
            }
            /********************************************* */
            $booking_common_id = null;
            foreach ($selected_professionals as $key => $professional_id) {
                $row = new stdClass();
                $row->customer_id = $input['id'];
                $row->reference_id = null; // update after getting insert id
                $row->customer_address_id = $input['address_id'];
                //$row->maid_id = $professional_id;
                //$row->maid_id = in_array($professional_id, $preferred_professional_ids) ? $professional_id : 0; // dont auto assign :/
                $row->maid_id = 0;
                $row->service_type_id = $input['service_type_id'];
                $row->service_week_day = Carbon::parse($service_start_date)->format('w');
                $row->time_from = $input['time_from'];
                $row->time_to = $input['time_to'];
                $row->booking_type = $calculate['frequency'] ?: "OD";
                $row->booking_category = "C";
                $row->booked_from = bookingPlatform(@$input['platform']);
                $row->pending_amount = 0;
                $row->price_per_hr = $calculate['service_rate_per_hour'];
                $row->amount_before_discount = $calculate['amount_before_discount'] / $calculate['amount_devide_by'];
                $row->service_charge = $calculate['taxable_amount'] / $calculate['amount_devide_by'];
                $row->discount = $calculate['discount_total'] ? ($calculate['discount_total'] / $calculate['amount_devide_by']) : 0;
                $row->cleaning_material = @$input['cleaning_materials'] ? 'Y' : 'N';
                $row->cleaning_material_fee = @$calculate['cleaning_materials_amount'] / $calculate['amount_devide_by'];
                $row->vat_charge = $calculate['vat_amount'] / $calculate['amount_devide_by'];
                $row->payment_type_charge = $calculate['payment_type_charge'] / $calculate['amount_devide_by'];
                $row->total_amount = $calculate['total_payable'] / $calculate['amount_devide_by'];
                $row->is_locked = 0;
                $row->no_of_maids = $calculate['professionals_count'];
                $row->no_of_hrs = $calculate['service_hours'];
                $row->booked_by = 0;
                $row->payment_type_id = $calculate['payment_type_id'];
                $row->pay_by = $calculate['payment_type'];
                $row->booking_note = @$input['instructions'];
                $row->booked_datetime = Carbon::now()->toDateTimeString();
                $row->service_start_date = $service_start_date;
                $row->coupon_id = $calculate['coupon_id'];
                $row->coupon_used = $calculate['coupon_code'];
                //
                $row->addons_included = $calculate['service_addons'] ? 1 : 0;
                $row->addons_amount = $calculate['service_addons'] ? $calculate['service_addons_amount'] : 0;
                $row->subscription_package_id = @$calculate['subscription_package_id'];
                //$row->is_package = @$input['package_ids'];
                if ($calculate['payment_type_id'] == "1") {
                    // cash mode
                    $row->web_book_show = 1;
                } else {
                    $row->web_book_show = 0;
                }
                $row->booking_status = 0;
                if ($input['frequency'] == "OD") {
                    $row->service_end_date = $service_start_date;
                    $row->service_actual_end_date = $service_start_date;
                    $row->service_end = 1; // it's one day
                } else if ($input['frequency'] == "WE") {
                    if ($calculate['no_of_weeks'] > 0) {
                        $row->service_end = 1; // end date available
                        $service_end_date = Carbon::parse($service_start_date)->addWeeks($calculate['no_of_weeks'] - 1)->format('Y-m-d');
                        $row->service_end_date = $service_end_date;
                        $row->service_actual_end_date = $service_end_date;
                    } else {
                        $row->service_end = 0; // never ends
                        $row->service_end_date = $service_start_date;
                        $row->service_actual_end_date = $service_start_date;
                    }
                } else if ($input['frequency'] == "BW") {
                    if ($calculate['no_of_weeks'] > 0) {
                        $row->service_end = 1; // end date available
                        $service_end_date = Carbon::parse($service_start_date)->addWeeks(($calculate['no_of_weeks'] * 2) - 2)->format('Y-m-d');
                        $row->service_end_date = $service_end_date;
                        $row->service_actual_end_date = $service_end_date;
                    } else {
                        $row->service_end = 0; // never ends
                        $row->service_end_date = $service_start_date;
                        $row->service_actual_end_date = $service_start_date;
                    }
                }
                /********************************************************** */
                // extra amount fields for more accuracy (currently used for mail purpose only)
                // these field not devided by professionals count
                $row->_service_amount = $calculate['service_amount'] ?: 0;
                $row->_cleaning_materials_amount = $calculate['cleaning_materials_amount'] ?: 0;
                $row->_service_addons_amount = $calculate['service_addons_amount'] ?: 0;
                $row->_packages_amount = $calculate['packages_amount'] ?: 0;
                $row->_subscription_package_amount = $calculate['subscription_package_amount'] ?: 0;
                $row->_amount_before_discount = $calculate['amount_before_discount'] ?: 0;
                $row->_coupon_discount = $calculate['coupon_discount'] ?: 0;
                $row->_discount_total = $calculate['discount_total'] ?: 0;
                $row->_taxable_amount = $calculate['taxable_amount'] ?: 0;
                $row->_vat_percentage = $calculate['vat_percentage'] ?: 0;
                $row->_vat_amount = $calculate['vat_amount'] ?: 0;
                $row->_taxed_amount = $calculate['taxed_amount'] ?: 0;
                $row->_payment_type_charge = $calculate['payment_type_charge'] ?: 0;
                $row->_total_payable = $calculate['total_payable'] ?: 0;
                /********************************************************** */
                $id = Booking::insertGetId((array) $row);
                $booking_ids[] = $id;
                $booking = Booking::find($id);
                $booking_common_id = @$booking_common_id ?: $id;
                if ($calculate['professionals_count'] > 1) {
                    // for act as single booking use common id
                    $booking->booking_common_id = $booking_common_id;
                }
                if (@$calculate['subscription_package_id']) {
                    $booking->reference_id = Config::get('values.subscription_package_booking_ref_prefix') . date('Y-') . $booking_common_id;
                } else {
                    $booking->reference_id = Config::get('values.booking_ref_prefix') . date('Y-') . $booking_common_id;
                }
                $booking->save();
                $bookings[] = $booking;
            }
            /********************************************* */
            // save cleaning materials
            if (@$calculate['cleaning_materials']) {
                foreach ($calculate['cleaning_materials'] as $key => $cleaning_material) {
                    $cleaning_material_row = new BookingCleaningMaterial();
                    $cleaning_material_row->booking_id = $booking_common_id;
                    $cleaning_material_row->service_type_cleaning_material_id = $cleaning_material['id'];
                    $cleaning_material_row->service_type_cleaning_material_name = $cleaning_material['name'];
                    $cleaning_material_row->unit_price = $cleaning_material['unit_price'];
                    $cleaning_material_row->quantity = $cleaning_material['quantity'];
                    $cleaning_material_row->amount = $cleaning_material['amount'];
                    $cleaning_material_row->save();
                }
            }
            /********************************************* */
            // save subscription package details
            if (@$calculate['subscription_package_id']) {
                $subscription_package = SubscriptionPackages::find($calculate['subscription_package_id']);
                $BookingSubscriptionPackage = new BookingSubscriptionPackages();
                $BookingSubscriptionPackage->booking_id = $booking_common_id;
                $BookingSubscriptionPackage->package_id = $subscription_package->package_id;
                $BookingSubscriptionPackage->package_name = $subscription_package->package_name;
                $BookingSubscriptionPackage->cleaning_material = $subscription_package->cleaning_material;
                $BookingSubscriptionPackage->no_of_bookings = $subscription_package->no_of_bookings;
                $BookingSubscriptionPackage->working_hours = $subscription_package->working_hours;
                $BookingSubscriptionPackage->booking_type = $subscription_package->booking_type;
                $BookingSubscriptionPackage->strikethrough_amount = $subscription_package->strikethrough_amount;
                $BookingSubscriptionPackage->amount = $subscription_package->amount;
                $BookingSubscriptionPackage->save();
            }
            /********************************************* */
            // customer coupons table update
            if (@$calculate['coupon_id']) {
                $customer_coupon = new CustomerCoupons();
                $customer_coupon->customer_id = $input['id'];
                $customer_coupon->coupon_id = $calculate['coupon_id'];
                $customer_coupon->reference_id = $bookings[0]->reference_id;
                $customer_coupon->discount = $calculate['coupon_discount'];
                $customer_coupon->save();
            }
            /********************************************* */
            // update booking addons (already inserted) with booking id
            BookingAddons::whereIn('id', $booking_addons_ids)->update(['booking_id' => $booking_common_id]);
            /********************************************* */
            if (count($calculate['packages'])) {
                foreach ($calculate['packages'] as $key => $package) {
                    $booking_package = new BookingPackages();
                    $booking_package->booking_id = $bookings[0]->booking_id;
                    $booking_package->building_type_room_package_id = $package->package_id;
                    $booking_package->unit_price = $package->total_amount;
                    $booking_package->quantity = $package->quantity;
                    $booking_package->save();
                }
            }
            /********************************************* */
            $response['status'] = 'success';
            $response['message'] = 'Booking saved with Ref. No. ' . $bookings[0]->reference_id . '.';
            $response['booking_details'] = [
                'service' => $calculate['service_type_name'],
                'booking_id' => $bookings[0]->booking_id,
                'booking_reference_id' => $bookings[0]->reference_id,
                'address' => $customer_address->address,
                'professionals_count' => $calculate['professionals_count'],
                'total_hours' => $calculate['service_hours'],
                'payment_method' => $calculate['payment_type'],
                'payment_type_id' => $calculate['payment_type_id'],
                'status' => $calculate['payment_type_id'] == 1 ? "Pending Approval" : "Payment Pending",
                'total_amount' => $calculate['summary']['total_payable'],
            ];
            /********************************************************************************************* */
            // for payment gateway (webview)
            $response['payment_details']['payment_url'] = null;
            $response['payment_details']['payment_url_params'] = null;
            if ($input['payment_method'] == 2) {
                $response['payment_details']['payment_url'] = url('payment/gateway/checkout/entry/' . $bookings[0]->booking_id . '?booking_id=' . $bookings[0]->booking_id . '&customer_id=' . $input['id'] . '&booking_reference=' . $bookings[0]->reference_id . '&amount=' . $calculate['summary']['total_payable']);
                // $response['payment_details']['payment_success_url'] = url('payment/gateway/checkout/success/' . $bookings[0]->booking_id);
                // $response['payment_details']['payment_failed_url'] = url('payment/gateway/checkout/failed/' . $bookings[0]->booking_id);
                $response['payment_details']['payment_success_url'] = url('api/customer/payment/payfort/success');
                $response['payment_details']['payment_failed_url'] = url('api/customer/payment/payfort/failed');
                $response['payment_details']['payment_success_url_prefix'] = url('payment/gateway/checkout/success/');
                $response['payment_details']['payment_failed_url_prefix'] = url('payment/gateway/checkout/failed/');
                $response['payment_details']['payment_url_params'] = [
                    'booking_id' => $bookings[0]->booking_id,
                    'customer_id' => $input['id'],
                    'booking_reference' => $bookings[0]->reference_id,
                    'amount' => $calculate['summary']['total_payable'],
                ];
                /******************************************************************************
                 *
                 * 
                 * Request contains checkout card token data
                 * 
                 * 
                 **************************************************************************** */
                if (@$input['checkout_token_data']) {
                    // have checkout token data in request
                    $response['checkout_token_data'] = $input['checkout_token_data'];
                    $checkout_data = checkoutPayment($input['checkout_token_data'], $bookings[0]->booking_id);
                    $response['checkout_data'] = $checkout_data;
                    if (strtolower(@$checkout_data['status']) == "pending") {
                        // have redirect link to enter OTP
                    } else if (@$checkout_data['approved'] == true) {
                        // payment success
                        afterCheckoutPaymentSuccess($bookings[0]->booking_id, $checkout_data, @$input['platform']);
                    } else {
                        throw new \ErrorException('Card payment not successful.');
                    }
                }
                /****************************************************************************** */
            } else if ($input['payment_method'] == 3) {
                // apple pay mode
                /******************************************************************************
                 * Request contains checkout applepay token data
                 **************************************************************************** */
                if (@$input['checkout_token_data']) {
                    // have checkout token data in request
                    $response['checkout_token_data'] = $input['checkout_token_data'];
                    $checkout_token_data = generateApplePayToken($input['checkout_token_data']);
                    $checkout_data = checkoutApplePay($checkout_token_data, $bookings[0]->booking_id);
                    $response['checkout_data'] = $checkout_data;
                    if (strtolower(@$checkout_data['status']) == "pending") {
                        // have redirect link to enter OTP
                    } else if (@$checkout_data['approved'] == true) {
                        // payment success
                        afterCheckoutPaymentSuccess($bookings[0]->booking_id, $checkout_data, @$input['platform']);
                    } else {
                        throw new \ErrorException('Payment not successful.');
                    }
                }
                /****************************************************************************** */
            } else if ($input['payment_method'] == 4) {
                // google pay mode
                /******************************************************************************
                 * Request contains checkout google pay token data
                 **************************************************************************** */
                if (@$input['checkout_token_data']) {
                    // have checkout token data in request
                    $response['checkout_token_data'] = $input['checkout_token_data'];
                    $checkout_token_data = generateGooglePayToken($input['checkout_token_data']);
                    $checkout_data = checkoutGooglePay($checkout_token_data, $bookings[0]->booking_id);
                    $response['checkout_data'] = $checkout_data;
                    if (strtolower(@$checkout_data['status']) == "pending") {
                        // have redirect link to enter OTP
                    } else if (@$checkout_data['approved'] == true) {
                        // payment success
                        afterCheckoutPaymentSuccess($bookings[0]->booking_id, $checkout_data, @$input['platform']);
                    } else {
                        throw new \ErrorException('Payment not successful.');
                    }
                }
                /****************************************************************************** */
            } else if ($input['payment_method'] == 5) {
                // tamara mode
                /**************************************************************************** */
                // have checkout token data in request
                $checkout_data = checkoutTamara($bookings[0]->booking_id);
                $response['checkout_data'] = $checkout_data;
            }
            /********************************************************************************************* */
            $response['calculate'] = $calculate;
            $response['debug_input'] = @$data['params'];
            /********************************************************************************************* */
            if ($calculate['payment_type_id'] == "1") {
                // cash mode
                $notify = new stdClass();
                $notify->customer_id = $input['id'];
                $notify->booking_id = $bookings[0]->booking_id;
                $notify->service_date = $input['frequency'] == "OD" ? $input['date'] : null;
                if (@$calculate['packages']) {
                    $notify->content = "Booking (Package) is allocated with Ref. Id. {{booking_ref_id}}, we'll contact you soon for confirmation.";
                } else {
                    $notify->content = "Booking is allocated with Ref. Id. {{booking_ref_id}}, we'll contact you soon for confirmation.";
                }
                // add to notifications
                addCustomerNotification($notify);
                // send booking confirmation mail to admin and customer
                send_booking_confirmation_mail_to_any($booking_common_id);
                // send push notification
                if (bookingPlatform(@$input['platform']) == 'mobile') {
                    pushNotification($customer, ['title' => "Booking Received !", 'body' => "Hi " . $customer->customer_name . ", your booking received with Ref. Id " . $bookings[0]->reference_id . "."], ['screen' => 'BOOKING_HISTORY']);
                } else {
                    pushNotification($customer, ['title' => "Booking Received !", 'body' => "Hi " . $customer->customer_name . ", your booking received with Ref. Id " . $bookings[0]->reference_id . "."], ['screen' => 'BOOKING_HISTORY']);
                }
                send_booking_confirmation_sms_to_customer($booking_common_id);
            }
            /********************************************************************************************* */
            DB::commit();
            return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        } catch (\Exception $e) {
            DB::rollback();
            return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
    }
}
