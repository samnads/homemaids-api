<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\CustomersTemp;
use App\Models\Settings;
use Carbon\Carbon;
use Config;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Response;

class CustomerApiOtpController extends Controller
{
    public function customer_login(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
        } else {
            // test input
            $data['params']['mobilenumber'] = '979797979';
            $data['params']['app_signature'] = 'rerererer';
        }
        /************************************************************* */
        // required input check
        $input = @$data['params'];
        $input['mobilenumber'] = substr(@$input['mobilenumber'], -9); // get last 9 digits only
        $validator = Validator::make(
            (array) $input,
            [
                'mobilenumber' => 'required|numeric|digits:9',
                'app_signature' => 'nullable|string',
            ],
            [],
            [
                'mobilenumber' => 'Mobile Number',
                'app_signature' => 'App Signature',
            ]
        );
        if ($validator->fails()) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        /************************************************************* */
        $customer = Customer::where('mobile_number_1', 'like', '%' . $input['mobilenumber'])->first();
        if ($customer) {
            // EXISTING CUSTOMER FOUND
            DB::beginTransaction();
            try {
                if ($debug || in_array($input['mobilenumber'], debug_mobiles()) || strpos($_SERVER['REQUEST_URI'], 'demo') !== false) {
                    $otp = '1234'; // debug /test otp
                } else {
                    $otp = mt_rand(1000, 9999);
                    send_customer_login_otp($input['mobilenumber'], $otp, @$input['app_signature']);
                }
                $customer->mobile_verification_code = $otp;
                $customer->login_otp_expired_at = Carbon::now()->addSeconds(Config::get('values.customer_login_otp_expire_seconds'));
                $customer->save();
                $response = [
                    'status' => 'success',
                    'message' => 'OTP send successfully!',
                    'signinup_status' => $customer->customer_name == null ? "new signup" : "already signup",
                    'UserDetails' => [
                        'country_code' => '+971',
                        'mobile' => $input['mobilenumber']
                    ]
                ];
                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                return Response::json(array('result' => array('status' => 'failed', 'message' => 'An error occured !'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
            }
        } else {
            // NEW CUSTOMER FOUND
            try {
                if ($debug || in_array($input['mobilenumber'], debug_mobiles()) || strpos($_SERVER['REQUEST_URI'], 'demo') !== false) {
                    $otp = '1234'; // debug /test otp
                } else {
                    $otp = mt_rand(1000, 9999);
                    send_customer_login_otp($input['mobilenumber'], $otp);
                }
                $customer = new Customer();
                $customer->mobile_number_1 = $input['mobilenumber'];
                $customer->odoo_customer_id = 0;
                $customer->customer_type = 'HO';
                $customer->customer_name = null;
                $customer->customer_nick_name = null;
                $customer->website_url = null;
                $customer->payment_type = 'D';
                $customer->payment_mode = 'Cash';
                $customer->balance = 0;
                $customer->signed = null;
                $customer->mobile_status = 0;
                $customer->customer_notes = null;
                $customer->customer_source_id = null;
                $customer->customer_added_datetime = Carbon::now();
                $customer->customer_last_modified_datetime = Carbon::now();
                $customer->mobile_verification_code = $otp;
                $customer->login_otp_expired_at = Carbon::now()->addSeconds(Config::get('values.customer_login_otp_expire_seconds'));
                $customer->save();
                $response = [
                    'status' => 'success',
                    'signinup_status' => 'new signup',
                    'UserDetails' => [
                        'country_code' => '+971',
                        'mobile' => $input['mobilenumber']
                    ]
                ];
            } catch (\Exception $e) {
                DB::rollback();
                return Response::json(array('result' => array('status' => 'failed', 'message' => 'An error occured !'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
            }
        }
        /************************************************************* */
        return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
    }
    public function check_otp(Request $request)
    {
        $settings = Settings::first();
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
        } else {
            // test input
            $data['params']['mobilenumber'] = '979797979';
            $data['params']['otp'] = '1234';
            $data['params']['fcm'] = 'test-fcm';
            $data['params']['device_type'] = 'ios';
            //$data['params']['new_mobilenumber'] = '979797977';
        }
        /************************************************************* */
        // required input check
        $input = @$data['params'];
        $input['mobilenumber'] = substr(@$input['mobilenumber'], -9); // get last 9 digits only
        $input['new_mobilenumber'] = substr(@$input['new_mobilenumber'], -9); // get last 9 digits only
        $validator = Validator::make(
            (array) $input,
            [
                'mobilenumber' => 'required|numeric|digits:9',
                'otp' => 'required|string|numeric|digits:4',
                'fcm' => 'nullable|string',
                'device_type' => 'nullable|string',
                'new_mobilenumber' => 'nullable|numeric|digits:9',
            ],
            [],
            [
                'mobilenumber' => 'Mobile Number',
                'otp' => 'OTP',
                'fcm' => 'FCM Token',
                'device_type' => 'Device Type',
                'new_mobilenumber' => 'New Mobile Number',
            ]
        );
        if ($validator->fails()) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        /************************************************************* */
        DB::beginTransaction();
        try {
            $customer = Customer::select('customers.*', 'ca.customer_address_id')
                ->leftJoin('customer_addresses as ca', function ($join) {
                    $join->on('customers.customer_id', '=', 'ca.customer_id');
                    $join->on('ca.default_address', '=', DB::raw('1'));
                })
                ->where('mobile_number_1', 'like', '%' . $input['mobilenumber'])->first();
            if (@$input['new_mobilenumber']) {
                // update mobile number with valid otp
                if (!$customer) {
                    throw new \ErrorException('Customer not found.');
                }
                $new_mobile_exist = Customer::where('customer_id', '!=', $customer->customer_id)->where('mobile_number_1', 'like', '%' . $input['new_mobilenumber'])->first();
                if ($new_mobile_exist) {
                    // duplicate mobile number
                    throw new \ErrorException('Mobile number already exist.');
                } else {
                    // allow requested new mobile number to db
                    $customer_temp = CustomersTemp::where('customer_id', '=', $customer->customer_id)->where('mobile_number_1', 'like', '%' . $input['new_mobilenumber'])->first();
                    if ($customer_temp) {
                        if ($input['otp'] == $customer_temp->mobile_number_1_otp && $customer_temp->mobile_number_1_otp_expired_at >= date('Y-m-d H:i:s')) {
                            $customer->mobile_verification_code = null;
                            $customer->login_otp_expired_at = null;
                            $customer->mobile_number_1 = $input['new_mobilenumber']; // update with new number
                            $token = Str::random(40);
                            $customer->oauth_token = $token;
                            if (@$input['fcm']) {
                                $customer->device_id = $input['fcm'];
                            }
                            if (@$input['device_type']) {
                                $customer->device_type = $input['device_type'];
                            }
                            $customer->mobile_status = 1;
                            $customer->save();
                            $response['status'] = 'success';
                            $response['message'] = 'Mobile number updated successfully.';
                            $response['UserDetails']['id'] = $customer->customer_id;
                            $response['UserDetails']['UserName'] = $customer->customer_name;
                            $response['UserDetails']['email'] = $customer->email_address;
                            $response['UserDetails']['dob'] = $customer->dob;
                            $response['UserDetails']['gender_id'] = $customer->gender_id;
                            $response['UserDetails']['image'] = Config::get('values.customer_avatar_prefix_url') . (($customer->customer_photo_file != '') ? (File::exists(Config::get('values.customer_avatar_storage_path') . $customer->customer_photo_file) ? ($customer->customer_photo_file . '?v=' . $settings->customer_app_img_version) : (Config::get('values.customer_avatar_default_file') . '?v=' . $settings->customer_app_img_version)) : (Config::get('values.customer_avatar_default_file') . '?v=' . $settings->customer_app_img_version));
                            $response['UserDetails']['token'] = $customer->oauth_token;
                            $response['UserDetails']['country_code'] = '+971';
                            $response['UserDetails']['mobile'] = $customer->mobile_number_1;
                            $response['UserDetails']['default_address_id'] = $customer->customer_address_id;
                            DB::commit();
                            return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
                        } else {
                            throw new \ErrorException('Invalid or Expired OTP !');
                        }
                    } else {
                        throw new \ErrorException('Mobile number change request not found !');
                    }
                }
            } else {
                if ($customer) {
                    if ($input['otp'] == $customer->mobile_verification_code && $customer->login_otp_expired_at >= date('Y-m-d H:i:s')) {
                        $customer->mobile_verification_code = null;
                        $customer->login_otp_expired_at = null;
                        $token = Str::random(40);
                        $customer->oauth_token = $token;
                        if (@$input['fcm']) {
                            $customer->device_id = $input['fcm'];
                        }
                        if (@$input['device_type']) {
                            $customer->device_type = $input['device_type'];
                        }
                        //$customer->mobile_status = 1;
                        $customer->save();
                        $response['status'] = 'success';
                        $response['message'] = 'Logged in successfully.';
                        $response['UserDetails']['id'] = $customer->customer_id;
                        $response['UserDetails']['UserName'] = $customer->customer_name;
                        $response['UserDetails']['email'] = $customer->email_address;
                        $response['UserDetails']['dob'] = $customer->dob;
                        $response['UserDetails']['gender_id'] = $customer->gender_id;
                        $response['UserDetails']['image'] = Config::get('values.customer_avatar_prefix_url') . (($customer->customer_photo_file != '') ? (File::exists(Config::get('values.customer_avatar_storage_path') . $customer->customer_photo_file) ? ($customer->customer_photo_file . '?v=' . $settings->customer_app_img_version) : (Config::get('values.customer_avatar_default_file') . '?v=' . $settings->customer_app_img_version)) : (Config::get('values.customer_avatar_default_file') . '?v=' . $settings->customer_app_img_version));
                        $response['UserDetails']['token'] = $token;
                        $response['UserDetails']['country_code'] = '+971';
                        $response['UserDetails']['mobile'] = $input['mobilenumber'];
                        $response['UserDetails']['default_address_id'] = $customer->customer_address_id;
                        DB::commit();
                        return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
                    } else {
                        throw new \ErrorException('Invalid or Expired OTP !');
                    }
                } else {
                    throw new \ErrorException('Customer not found.');
                }
            }
        } catch (\Exception $e) {
            DB::rollback();
            return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
    }
    public function resend_otp(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
        } else {
            // test input
            $data['params']['mobilenumber'] = 979797977;
            $data['params']['new_mobilenumber'] = '979797978';
        }
        /************************************************************* */
        // required input check
        $input = @$data['params'];
        $input['mobilenumber'] = substr(@$input['mobilenumber'], -9); // get last 9 digits only
        $input['new_mobilenumber'] = substr(@$input['new_mobilenumber'], -9); // get last 9 digits only
        $validator = Validator::make(
            (array) $input,
            [
                'mobilenumber' => 'required|numeric|digits:9',
                'new_mobilenumber' => 'nullable|numeric|digits:9',
            ],
            [],
            [
                'mobilenumber' => 'Mobile Number',
                'new_mobilenumber' => 'New Mobile Number',
            ]
        );
        if ($validator->fails()) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        /************************************************************* */
        try {
            DB::beginTransaction();
            $customer = Customer::where('mobile_number_1', 'like', '%' . $input['mobilenumber'])->first();
            if (!$customer) {
                throw new \ErrorException('Customer not found.');
            }
            $startTime = Carbon::now();
            if (@$input['new_mobilenumber']) {
                $customer_temp = CustomersTemp::where('customer_id', '=', $customer->customer_id)->where('mobile_number_1', 'like', '%' . $input['new_mobilenumber'])->first();
                if ($customer_temp) {
                    // check and resend otp
                    $finishTime = Carbon::parse($customer_temp->mobile_number_1_otp_expired_at);
                    $seconds = $finishTime->diffInSeconds($startTime);
                    if ($customer_temp->mobile_number_1_otp_expired_at > Carbon::now() && $seconds >= 1) {
                        throw new \ErrorException('Please wait ' . $seconds . ' second' . ($seconds > 0 ? 's' : '') . ' before resend OTP.');
                    } else {
                        if ($debug || in_array($input['mobilenumber'], debug_mobiles())) {
                            $otp = '1234'; // debug /test otp
                        } else {
                            $otp = mt_rand(1000, 9999);
                            send_customer_login_otp($input['mobilenumber'], $otp);
                        }
                        $customer_temp->mobile_number_1_otp = $otp;
                        $customer_temp->mobile_number_1_otp_expired_at = Carbon::now()->addSeconds(Config::get('values.customer_login_otp_expire_seconds'));
                        $customer_temp->save();
                        $response['status'] = 'success';
                        $response['message'] = 'OTP resend successfully!';
                        $response['UserDetails']['country_code'] = '+971';
                        $response['UserDetails']['mobile'] = $input['new_mobilenumber'];
                    }
                } else {
                    throw new \ErrorException('Mobile number change request not found !');
                }
            } else {
                $finishTime = Carbon::parse($customer->login_otp_expired_at);
                $seconds = $finishTime->diffInSeconds($startTime);
                if ($customer->login_otp_expired_at > Carbon::now() && $seconds >= 1) {
                    throw new \ErrorException('Please wait ' . $seconds . ' second' . ($seconds > 0 ? 's' : '') . ' before resend OTP.');
                }
                if ($debug || in_array($input['mobilenumber'], debug_mobiles())) {
                    $otp = '1234'; // debug /test otp
                } else {
                    $otp = mt_rand(1000, 9999);
                    send_customer_login_otp($input['mobilenumber'], $otp);
                }
                $customer->mobile_verification_code = $otp;
                $customer->login_otp_expired_at = Carbon::now()->addSeconds(Config::get('values.customer_login_otp_expire_seconds'));
                $customer->save();
                $response['status'] = 'success';
                $response['message'] = 'OTP resend successfully!';
                $response['UserDetails']['country_code'] = '+971';
                $response['UserDetails']['mobile'] = $input['mobilenumber'];
            }
            DB::commit();
            return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        } catch (\Exception $e) {
            DB::rollback();
            return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
    }
}
