<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;

class CustomerApiAreaController extends Controller
{
    public function area_list(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
        } else {
            // test input
            $data['params'] = [];
        }
        /************************************************************* */
        $input = @$data['params'];
        /************************************************************* */
        $response['status'] = 'success';
        $response['data'] = DB::table('areas as a')
            ->select(
                'a.area_id as id',
                'a.area_name as name',
                //DB::raw('CONCAT(a.area_name," [",z.zone_name,"]") as name')
            )
            ->leftJoin('zones as z', 'a.zone_id', 'z.zone_id')
            ->where([['a.area_status', '=', 1], ['a.web_status', '=', 1]])
            ->orderBy('a.area_name', 'ASC')
            ->orderBy('z.zone_name', 'ASC')
            ->get();
        $response['message'] = sizeof($response['data']) ? "Areas fetched successfully." : "No area found.";
        return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
    }
}
