<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Customer;
use App\Models\CustomerAddress;
use App\Models\CustomerPayments;
use App\Models\Frequencies;
use App\Models\OnlinePayment;
use Carbon\Carbon;
use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Mail;
use Redirect;
use Response;
use stdClass;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\AdminApiMailController;

class PaymentFrameController extends Controller
{
    public function checkout_apple_pay_process(Request $request)
    {
        /********************************************************************************* */
        $debug = toggleDebug(); // pass boolean to overide default
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
        } else {
            // test input
            $data['params']['payment_token'] = "{\"version\":\"EC_v1\",\"data\":\"mCX\/uHkM8edphT55wjN\/RvKuvlVSpLJckmHymF3BqS+uE9PZkx4892eJDeVnJgaInqVtjV4ZM7AZiQQ9r9MmlTDaDqyRqBVtOfc1OArD9S1Mn5C3kATFMGzEjD+7IpYyEK+YH+DhlbPCe2MLIh33k5c8FFB0rCIjN3OyAo1AwGIYkH0zlrLqT\/6volEiMY5jIERF\/jrcSxCWsIl995hpEsH3VQA93g0I60rt3DcXPRxFNbEjlTDSCSAE1Gjz0Eeq\/66aFzsqj6hYBgY0a14OZtxBZyy4f4pP9KfKye++zs9kJIGcnJce+eJ6+vCsXSmeP9jijhxDaqUyUp9sz24wTzTXDcUkFbwROU3baWYo35XtzP7lHkoexsKbjZbXaTYOr7RA8r4koL3v6qpXkgOqLoGoal\/+TxfrNXtJscYfcQ==\",\"signature\":\"MIAGCSqGSIb3DQEHAqCAMIACAQExDTALBglghkgBZQMEAgEwgAYJKoZIhvcNAQcBAACggDCCA+MwggOIoAMCAQICCEwwQUlRnVQ2MAoGCCqGSM49BAMCMHoxLjAsBgNVBAMMJUFwcGxlIEFwcGxpY2F0aW9uIEludGVncmF0aW9uIENBIC0gRzMxJjAkBgNVBAsMHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUzAeFw0xOTA1MTgwMTMyNTdaFw0yNDA1MTYwMTMyNTdaMF8xJTAjBgNVBAMMHGVjYy1zbXAtYnJva2VyLXNpZ25fVUM0LVBST0QxFDASBgNVBAsMC2lPUyBTeXN0ZW1zMRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUzBZMBMGByqGSM49AgEGCCqGSM49AwEHA0IABMIVd+3r1seyIY9o3XCQoSGNx7C9bywoPYRgldlK9KVBG4NCDtgR80B+gzMfHFTD9+syINa61dTv9JKJiT58DxOjggIRMIICDTAMBgNVHRMBAf8EAjAAMB8GA1UdIwQYMBaAFCPyScRPk+TvJ+bE9ihsP6K7\/S5LMEUGCCsGAQUFBwEBBDkwNzA1BggrBgEFBQcwAYYpaHR0cDovL29jc3AuYXBwbGUuY29tL29jc3AwNC1hcHBsZWFpY2EzMDIwggEdBgNVHSAEggEUMIIBEDCCAQwGCSqGSIb3Y2QFATCB\/jCBwwYIKwYBBQUHAgIwgbYMgbNSZWxpYW5jZSBvbiB0aGlzIGNlcnRpZmljYXRlIGJ5IGFueSBwYXJ0eSBhc3N1bWVzIGFjY2VwdGFuY2Ugb2YgdGhlIHRoZW4gYXBwbGljYWJsZSBzdGFuZGFyZCB0ZXJtcyBhbmQgY29uZGl0aW9ucyBvZiB1c2UsIGNlcnRpZmljYXRlIHBvbGljeSBhbmQgY2VydGlmaWNhdGlvbiBwcmFjdGljZSBzdGF0ZW1lbnRzLjA2BggrBgEFBQcCARYqaHR0cDovL3d3dy5hcHBsZS5jb20vY2VydGlmaWNhdGVhdXRob3JpdHkvMDQGA1UdHwQtMCswKaAnoCWGI2h0dHA6Ly9jcmwuYXBwbGUuY29tL2FwcGxlYWljYTMuY3JsMB0GA1UdDgQWBBSUV9tv1XSBhomJdi9+V4UH55tYJDAOBgNVHQ8BAf8EBAMCB4AwDwYJKoZIhvdjZAYdBAIFADAKBggqhkjOPQQDAgNJADBGAiEAvglXH+ceHnNbVeWvrLTHL+tEXzAYUiLHJRACth69b1UCIQDRizUKXdbdbrF0YDWxHrLOh8+j5q9svYOAiQ3ILN2qYzCCAu4wggJ1oAMCAQICCEltL786mNqXMAoGCCqGSM49BAMCMGcxGzAZBgNVBAMMEkFwcGxlIFJvb3QgQ0EgLSBHMzEmMCQGA1UECwwdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxEzARBgNVBAoMCkFwcGxlIEluYy4xCzAJBgNVBAYTAlVTMB4XDTE0MDUwNjIzNDYzMFoXDTI5MDUwNjIzNDYzMFowejEuMCwGA1UEAwwlQXBwbGUgQXBwbGljYXRpb24gSW50ZWdyYXRpb24gQ0EgLSBHMzEmMCQGA1UECwwdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxEzARBgNVBAoMCkFwcGxlIEluYy4xCzAJBgNVBAYTAlVTMFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAE8BcRhBnXZIXVGl4lgQd26ICi7957rk3gjfxLk+EzVtVmWzWuItCXdg0iTnu6CP12F86Iy3a7ZnC+yOgphP9URaOB9zCB9DBGBggrBgEFBQcBAQQ6MDgwNgYIKwYBBQUHMAGGKmh0dHA6Ly9vY3NwLmFwcGxlLmNvbS9vY3NwMDQtYXBwbGVyb290Y2FnMzAdBgNVHQ4EFgQUI\/JJxE+T5O8n5sT2KGw\/orv9LkswDwYDVR0TAQH\/BAUwAwEB\/zAfBgNVHSMEGDAWgBS7sN6hWDOImqSKmd6+veuv2sskqzA3BgNVHR8EMDAuMCygKqAohiZodHRwOi8vY3JsLmFwcGxlLmNvbS9hcHBsZXJvb3RjYWczLmNybDAOBgNVHQ8BAf8EBAMCAQYwEAYKKoZIhvdjZAYCDgQCBQAwCgYIKoZIzj0EAwIDZwAwZAIwOs9yg1EWmbGG+zXDVspiv\/QX7dkPdU2ijr7xnIFeQreJ+Jj3m1mfmNVBDY+d6cL+AjAyLdVEIbCjBXdsXfM4O5Bn\/Rd8LCFtlk\/GcmmCEm9U+Hp9G5nLmwmJIWEGmQ8Jkh0AADGCAYkwggGFAgEBMIGGMHoxLjAsBgNVBAMMJUFwcGxlIEFwcGxpY2F0aW9uIEludGVncmF0aW9uIENBIC0gRzMxJjAkBgNVBAsMHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUwIITDBBSVGdVDYwCwYJYIZIAWUDBAIBoIGTMBgGCSqGSIb3DQEJAzELBgkqhkiG9w0BBwEwHAYJKoZIhvcNAQkFMQ8XDTIzMTIwODE1NTYxNVowKAYJKoZIhvcNAQk0MRswGTALBglghkgBZQMEAgGhCgYIKoZIzj0EAwIwLwYJKoZIhvcNAQkEMSIEID1+8zPGFJSL619BKr0Chx9Kbclk5Cubny8zoT893HsXMAoGCCqGSM49BAMCBEgwRgIhAOSTY95u\/VTZMfuNYpKSiTHmLMjtbVkpQoKn3jhyZ+61AiEAtPCvPd7zKtP57RJZquNPHLiLvJK2ZyHFxBAG0eK\/XiQAAAAAAAA=\",\"header\":{\"ephemeralPublicKey\":\"MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAE3bVGphrKRgcVQsXhI4iG573U49xigaoSFosrMhEjUZpuqeRs\/zEJDfxFaTH8t6V8GCoP9P4pYR50cAQfE4rhMw==\",\"publicKeyHash\":\"zGiWLt0wO01zvs\/DWcdtotRU73j175wtVruKL3\/Hc14=\",\"transactionId\":\"0f85c914664701878f4cc0817054c4f1b65606ec4a2bd6152d9ead0540285cd0\"}}"; // payment token
            $data['params']['payment_type_id'] = 3;
            $data['params']['booking_id'] = "178039";
            $data['params']['reference_id'] = "TEST_123";
            $data['params']['amount'] = 100;
            $data['params']['platform'] = "mobile"; // for identifying source - mobile|web
        }
        $input = @$data['params'];
        /********************************************************************************* */
        $payment_type = DB::table('payment_types as pt')
            ->select(
                'pt.*',
            )
            ->where(['pt.id' => $input['payment_type_id']])
            ->first();
        /********************************************************************************* */
        try {
            $booking_common_id = $input['booking_id'];
            $booking = Booking::where('booking_id', $booking_common_id)->first();
            if ($booking->payment_type_id != $input['payment_type_id']) {
                throw new \ErrorException('Payment method doesn\'t match with database.');
            }
            $bookings = DB::table('bookings as b')
                ->select(
                    'b.*',
                )
                ->where(function ($query) use ($booking_common_id) {
                    $query->where('b.booking_common_id', $booking_common_id);
                    $query->orWhere('b.booking_id', $booking_common_id);
                })
                ->orderBy('b.booking_id', 'ASC')
                ->get();
            $bookings_ids = array_column($bookings->toArray(), 'booking_id');
            $customer = Customer::where('customer_id', '=', $booking->customer_id)->first();
            /********************************************************************************* */
            $client = new \GuzzleHttp\Client([
                'verify' => false,
            ]);
            $post_header = ["Authorization" => "Bearer " . Config::get('values.checkout_primary_key'), "Content-Type" => "application/json", "Accept" => "application/json"];
            $checkout_data = [
                "type" => "applepay",
                "token_data" => json_decode($input['payment_token'])
            ]; // don't convert to string
            // https://www.checkout.com/docs/payments/add-payment-methods/apple-pay
            // Step 1: Generate a Checkout.com token from the Apple Pay token
            $response_token_data = $client->request('POST', Config::get('values.checkout_token_url'), [
                'headers' => $post_header, // no exception fix
                'http_errors' => false,
                'json' => $checkout_data,
            ]);
            $token_data = json_decode((string) $response_token_data->getBody(), true);
            if (@$token_data['token']) {
                // token found
                //Step 2: Request a payment
                $post_header = ["Authorization" => "Bearer " . Config::get('values.checkout_secret_key'), "Content-Type" => "application/json", "Accept" => "application/json"];
                $checkout_data = [
                    'source' => ["type" => "token", "token" => $token_data['token']],
                    'customer' => ["email" => @$customer->email_address, "name" => @$customer->customer_name],
                    'amount' => ((float) $input['amount']) * 100,
                    'currency' => Config::get('values.currency_code'),
                    'processing_channel_id' => Config::get('values.checkout_channel_id')
                ];
                $response_payment_data = $client->request('POST', Config::get('values.checkout_service_url'), [
                    'headers' => $post_header, // no exceptin fix
                    'http_errors' => false,
                    'json' => $checkout_data,
                ]);
                $payment_data = json_decode((string) $response_payment_data->getBody(), true);
                //dd($payment_data);
                if (@$payment_data['approved'] == true) {
                    // payment success
                    /********************************************************************
                     * 
                     * 
                     * PAYMENT SUCCESS
                     * 
                     * 
                     *********************************************************************/
                    // show on web bookings in admin
                    Booking::whereIn('booking_id', $bookings_ids)->update(['booking_status' => 0, 'web_book_show' => 1]); // update
                    /******************************************************************** */
                    // update database
                    $online_payment = OnlinePayment::where('booking_id', $booking->booking_id)->where('reference_id', $booking->reference_id)->first();
                    if ($online_payment) {
                        if ($online_payment->payment_status != 'success') {
                            $online_payment->transaction_id = $payment_data['id'];
                            $online_payment->amount = $payment_data['amount'] / 100;
                            $online_payment->transaction_charge = 0;
                            $online_payment->customer_id = $booking->customer_id;
                            $online_payment->description = null;
                            $online_payment->payment_status = 'success';
                            $online_payment->payment_type = $payment_type->code;
                            $online_payment->user_agent = @$_SERVER['HTTP_USER_AGENT'];
                            $online_payment->post_data = null;
                            $online_payment->return_data = json_encode($payment_data);
                            $online_payment->paid_from = bookingPlatform(@$input['platform']);
                            $online_payment->payment_datetime = now();
                            $online_payment->save();
                        } else {
                            return 'Payment Already Verfied !';
                        }
                    } else {
                        $online_payment = new OnlinePayment();
                        $online_payment->transaction_id = $payment_data['id'];
                        $online_payment->booking_id = $booking->booking_id;
                        $online_payment->reference_id = $booking->reference_id;
                        $online_payment->amount = $payment_data['amount'] / 100;
                        $online_payment->transaction_charge = 0;
                        $online_payment->customer_id = $booking->customer_id;
                        $online_payment->description = null;
                        $online_payment->payment_status = 'success';
                        $online_payment->payment_type = $payment_type->code;
                        $online_payment->user_agent = @$_SERVER['HTTP_USER_AGENT'];
                        $online_payment->post_data = null;
                        $online_payment->return_data = json_encode($payment_data);
                        $online_payment->paid_from = bookingPlatform(@$input['platform']);
                        $online_payment->payment_datetime = now();
                        $online_payment->save();
                        /**************************************** */
                        // update customer payments
                        $customer_payment = new CustomerPayments();
                        $customer_payment->day_service_id = 0;
                        $customer_payment->customer_id = $booking->customer_id;
                        $customer_payment->paid_amount = $payment_data['amount'] / 100;
                        $customer_payment->payment_method = $payment_type->id;
                        $customer_payment->paid_at = bookingPlatform(@$input['platform']);
                        $customer_payment->paid_at_id = 1;
                        $customer_payment->booking_id = $booking->booking_id;
                        $customer_payment->paid_datetime = now();
                        $customer_payment->save();
                    }
                    /******************************************************************** */
                    send_booking_confirmation_mail_to_any($booking->booking_id);
                    send_booking_confirmation_sms_to_customer($booking->booking_id);
                    pushNotification($customer, ['title' => "Booking Received !", 'body' => "Booking received with Ref. Id " . $booking->reference_id . "."], ['screen' => 'BOOKING_HISTORY']);
                    $params = [
                        'reference_id' => $booking->reference_id,
                        'booking_id' => $booking->booking_id,
                        'payment_method' => $payment_type->name,
                        'status' => 'Confirmed',
                    ];
                    $response['booking_details'] = $params;
                    $response['status'] = 'success';
                    $response['message'] = 'Payment verified successfully';
                    return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
                } else {
                    //payment failed
                    throw new \ErrorException('Payment verification failed');
                }
            } else {
                throw new \ErrorException('Payment token verification failed');
            }
        } catch (\Exception $e) {
            Mail::send(
                'emails.debug_mail',
                [
                    'input' => json_encode($data)
                ],
                function ($m) {
                    $m->from(Config::get('mail.mail_from_address'), Config::get('mail.mail_from_name'));
                    $m->to('samnad.s@azinova.info', 'Samnad S');
                    $m->subject('Apple Pay Test');
                }
            );
            $params = [
                'reference_id' => @$booking->reference_id,
                'booking_id' => @$booking->booking_id,
                'payment_method' => $payment_type->name,
                'status' => 'Failed',
            ];
            $response['booking_details'] = $params;
            $response['status'] = 'failed';
            $response['message'] = $e->getMessage();
            return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        /********************************************************************************* */
    }
    public function checkout_google_pay_process(Request $request)
    {
        /********************************************************************************* */
        $debug = toggleDebug(); // pass boolean to overide default
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
        } else {
            // test input
            $data['params']['payment_token'] = "{\"version\":\"EC_v1\",\"data\":\"UVQy7jw9vp1HfqXDOFlCXKMLgue8HTAQYLNCCCe0RXwi8Cc8EvmxozIsrfJjg+E0jHbttnBnXXdZXaX78ruvMfOvdXbZOpJNIoBXTElX1+N0\/bqAhLF\/DxL5HnXuoKiatXYzo2yJ5E7ajhFi816dKVCwNqTIrEBeE9GHvCILrb8fiwGVgLHUMKuBlRfkNKoY+Ujg2MdH8P+Osj0Nj4CLlPVuiCsrx66IPI34+DU\/\/oS+nWUH5mIr90PLfpNQIcLHmuf3E1UYMgDfhrOM05GtpVJDv1UFyfEPiWO8dEGRuNFglWIa9TvQE0P++KdFIXStVunvwRE2Cms2XsC8B3PHJ5iWzXfA7OaV1bp4MHQaKR+A35nZvn7LSAWqdpBAfJOU5reEafez5vBUQkmB\",\"signature\":\"MIAGCSqGSIb3DQEHAqCAMIACAQExDTALBglghkgBZQMEAgEwgAYJKoZIhvcNAQcBAACggDCCA+MwggOIoAMCAQICCEwwQUlRnVQ2MAoGCCqGSM49BAMCMHoxLjAsBgNVBAMMJUFwcGxlIEFwcGxpY2F0aW9uIEludGVncmF0aW9uIENBIC0gRzMxJjAkBgNVBAsMHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUzAeFw0xOTA1MTgwMTMyNTdaFw0yNDA1MTYwMTMyNTdaMF8xJTAjBgNVBAMMHGVjYy1zbXAtYnJva2VyLXNpZ25fVUM0LVBST0QxFDASBgNVBAsMC2lPUyBTeXN0ZW1zMRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUzBZMBMGByqGSM49AgEGCCqGSM49AwEHA0IABMIVd+3r1seyIY9o3XCQoSGNx7C9bywoPYRgldlK9KVBG4NCDtgR80B+gzMfHFTD9+syINa61dTv9JKJiT58DxOjggIRMIICDTAMBgNVHRMBAf8EAjAAMB8GA1UdIwQYMBaAFCPyScRPk+TvJ+bE9ihsP6K7\/S5LMEUGCCsGAQUFBwEBBDkwNzA1BggrBgEFBQcwAYYpaHR0cDovL29jc3AuYXBwbGUuY29tL29jc3AwNC1hcHBsZWFpY2EzMDIwggEdBgNVHSAEggEUMIIBEDCCAQwGCSqGSIb3Y2QFATCB\/jCBwwYIKwYBBQUHAgIwgbYMgbNSZWxpYW5jZSBvbiB0aGlzIGNlcnRpZmljYXRlIGJ5IGFueSBwYXJ0eSBhc3N1bWVzIGFjY2VwdGFuY2Ugb2YgdGhlIHRoZW4gYXBwbGljYWJsZSBzdGFuZGFyZCB0ZXJtcyBhbmQgY29uZGl0aW9ucyBvZiB1c2UsIGNlcnRpZmljYXRlIHBvbGljeSBhbmQgY2VydGlmaWNhdGlvbiBwcmFjdGljZSBzdGF0ZW1lbnRzLjA2BggrBgEFBQcCARYqaHR0cDovL3d3dy5hcHBsZS5jb20vY2VydGlmaWNhdGVhdXRob3JpdHkvMDQGA1UdHwQtMCswKaAnoCWGI2h0dHA6Ly9jcmwuYXBwbGUuY29tL2FwcGxlYWljYTMuY3JsMB0GA1UdDgQWBBSUV9tv1XSBhomJdi9+V4UH55tYJDAOBgNVHQ8BAf8EBAMCB4AwDwYJKoZIhvdjZAYdBAIFADAKBggqhkjOPQQDAgNJADBGAiEAvglXH+ceHnNbVeWvrLTHL+tEXzAYUiLHJRACth69b1UCIQDRizUKXdbdbrF0YDWxHrLOh8+j5q9svYOAiQ3ILN2qYzCCAu4wggJ1oAMCAQICCEltL786mNqXMAoGCCqGSM49BAMCMGcxGzAZBgNVBAMMEkFwcGxlIFJvb3QgQ0EgLSBHMzEmMCQGA1UECwwdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxEzARBgNVBAoMCkFwcGxlIEluYy4xCzAJBgNVBAYTAlVTMB4XDTE0MDUwNjIzNDYzMFoXDTI5MDUwNjIzNDYzMFowejEuMCwGA1UEAwwlQXBwbGUgQXBwbGljYXRpb24gSW50ZWdyYXRpb24gQ0EgLSBHMzEmMCQGA1UECwwdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxEzARBgNVBAoMCkFwcGxlIEluYy4xCzAJBgNVBAYTAlVTMFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAE8BcRhBnXZIXVGl4lgQd26ICi7957rk3gjfxLk+EzVtVmWzWuItCXdg0iTnu6CP12F86Iy3a7ZnC+yOgphP9URaOB9zCB9DBGBggrBgEFBQcBAQQ6MDgwNgYIKwYBBQUHMAGGKmh0dHA6Ly9vY3NwLmFwcGxlLmNvbS9vY3NwMDQtYXBwbGVyb290Y2FnMzAdBgNVHQ4EFgQUI\/JJxE+T5O8n5sT2KGw\/orv9LkswDwYDVR0TAQH\/BAUwAwEB\/zAfBgNVHSMEGDAWgBS7sN6hWDOImqSKmd6+veuv2sskqzA3BgNVHR8EMDAuMCygKqAohiZodHRwOi8vY3JsLmFwcGxlLmNvbS9hcHBsZXJvb3RjYWczLmNybDAOBgNVHQ8BAf8EBAMCAQYwEAYKKoZIhvdjZAYCDgQCBQAwCgYIKoZIzj0EAwIDZwAwZAIwOs9yg1EWmbGG+zXDVspiv\/QX7dkPdU2ijr7xnIFeQreJ+Jj3m1mfmNVBDY+d6cL+AjAyLdVEIbCjBXdsXfM4O5Bn\/Rd8LCFtlk\/GcmmCEm9U+Hp9G5nLmwmJIWEGmQ8Jkh0AADGCAYkwggGFAgEBMIGGMHoxLjAsBgNVBAMMJUFwcGxlIEFwcGxpY2F0aW9uIEludGVncmF0aW9uIENBIC0gRzMxJjAkBgNVBAsMHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUwIITDBBSVGdVDYwCwYJYIZIAWUDBAIBoIGTMBgGCSqGSIb3DQEJAzELBgkqhkiG9w0BBwEwHAYJKoZIhvcNAQkFMQ8XDTIzMTIwNTA3NTMzMVowKAYJKoZIhvcNAQk0MRswGTALBglghkgBZQMEAgGhCgYIKoZIzj0EAwIwLwYJKoZIhvcNAQkEMSIEIHDNncxvy+SU+ajdQ5K7wpCw+cw3gnVR2PsgClXngDBHMAoGCCqGSM49BAMCBEgwRgIhAPI65viBJE9GTbRu0NZSLLsH\/yKZaGdnh3YMM7\/uy9MMAiEAjdugTxf54pj2+fjSXdgPfXGrFxes\/l1iEwDy9R1GY3AAAAAAAAA=\",\"header\":{\"ephemeralPublicKey\":\"MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEHrH3e0SmmQ882cLAzRXwp9Ty+4PTAzNp5gj\/h395FgDsBUTqLvK1gtvENXqHgY\/WEaphiFFn00nbhbWBvoeGFQ==\",\"publicKeyHash\":\"zGiWLt0wO01zvs\/DWcdtotRU73j175wtVruKL3\/Hc14=\",\"transactionId\":\"14b1eac3ceb2f548ebeeb69ed21cd2c8662b3b7e0cdb2031decfa96a960e8080\"}}"; // payment token
            $data['params']['payment_type_id'] = 3;
            $data['params']['booking_id'] = "177912";
            $data['params']['reference_id'] = "TEST_123";
            $data['params']['amount'] = 100;
            $data['params']['platform'] = "mobile"; // for identifying source - mobile|web
        }
        $input = @$data['params'];
        /********************************************************************************* */
        $payment_type = DB::table('payment_types as pt')
            ->select(
                'pt.*',
            )
            ->where(['pt.id' => $input['payment_type_id']])
            ->first();
        /********************************************************************************* */
        try {
            $booking_common_id = $input['booking_id'];
            $booking = Booking::where('booking_id', $booking_common_id)->first();
            if ($booking->payment_type_id != $input['payment_type_id']) {
                throw new \ErrorException('Payment method doesn\'t match with database.');
            }
            $bookings = DB::table('bookings as b')
                ->select(
                    'b.*',
                )
                ->where(function ($query) use ($booking_common_id) {
                    $query->where('b.booking_common_id', $booking_common_id);
                    $query->orWhere('b.booking_id', $booking_common_id);
                })
                ->orderBy('b.booking_id', 'ASC')
                ->get();
            $bookings_ids = array_column($bookings->toArray(), 'booking_id');
            $customer = Customer::where('customer_id', '=', $booking->customer_id)->first();
            /********************************************************************************* */
            $client = new \GuzzleHttp\Client([
                'verify' => false,
            ]);
            $post_header = ["Authorization" => "Bearer " . Config::get('values.checkout_primary_key'), "Content-Type" => "application/json", "Accept" => "application/json"];
            $checkout_data = [
                "type" => "googlepay",
                "token_data" => json_decode($input['payment_token'])
            ]; // don't convert to string
            // https://www.checkout.com/docs/payments/add-payment-methods/apple-pay
            // Step 1: Generate a Checkout.com token from the Apple Pay token
            $response_token_data = $client->request('POST', Config::get('values.checkout_token_url'), [
                'headers' => $post_header, // no exception fix
                'http_errors' => false,
                'json' => $checkout_data,
            ]);
            $token_data = json_decode((string) $response_token_data->getBody(), true);
            if (@$token_data['token']) {
                // token found
                //Step 2: Request a payment
                $post_header = ["Authorization" => "Bearer " . Config::get('values.checkout_secret_key'), "Content-Type" => "application/json", "Accept" => "application/json"];
                $checkout_data = [
                    'source' => ["type" => "token", "token" => $token_data['token']],
                    'customer' => ["email" => @$customer->email_address, "name" => @$customer->customer_name],
                    'amount' => ((float) $input['amount']) * 100,
                    'currency' => Config::get('values.currency_code'),
                    'processing_channel_id' => Config::get('values.checkout_channel_id')
                ];
                $response_payment_data = $client->request('POST', Config::get('values.checkout_service_url'), [
                    'headers' => $post_header, // no exceptin fix
                    'http_errors' => false,
                    'json' => $checkout_data,
                ]);
                $payment_data = json_decode((string) $response_payment_data->getBody(), true);
                if (@$payment_data['approved'] == true) {
                    // payment success
                    /********************************************************************
                     * 
                     * 
                     * PAYMENT SUCCESS
                     * 
                     * 
                     *********************************************************************/
                    // show on web bookings in admin
                    Booking::whereIn('booking_id', $bookings_ids)->update(['booking_status' => 0, 'web_book_show' => 1]); // update
                    /******************************************************************** */
                    // update database
                    $online_payment = OnlinePayment::where('booking_id', $booking->booking_id)->where('reference_id', $booking->reference_id)->first();
                    if ($online_payment) {
                        if ($online_payment->payment_status != 'success') {
                            $online_payment->transaction_id = $payment_data['id'];
                            $online_payment->amount = $payment_data['amount'] / 100;
                            $online_payment->transaction_charge = 0;
                            $online_payment->customer_id = $booking->customer_id;
                            $online_payment->description = null;
                            $online_payment->payment_status = 'success';
                            $online_payment->payment_type = $payment_type->code;
                            $online_payment->user_agent = @$_SERVER['HTTP_USER_AGENT'];
                            $online_payment->post_data = null;
                            $online_payment->return_data = json_encode($payment_data);
                            $online_payment->paid_from = bookingPlatform(@$input['platform']);
                            $online_payment->payment_datetime = now();
                            $online_payment->save();
                        } else {
                            return 'Payment Already Verfied !';
                        }
                    } else {
                        $online_payment = new OnlinePayment();
                        $online_payment->transaction_id = $payment_data['id'];
                        $online_payment->booking_id = $booking->booking_id;
                        $online_payment->reference_id = $booking->reference_id;
                        $online_payment->amount = $payment_data['amount'] / 100;
                        $online_payment->transaction_charge = 0;
                        $online_payment->customer_id = $booking->customer_id;
                        $online_payment->description = null;
                        $online_payment->payment_status = 'success';
                        $online_payment->payment_type = $payment_type->code;
                        $online_payment->user_agent = @$_SERVER['HTTP_USER_AGENT'];
                        $online_payment->post_data = null;
                        $online_payment->return_data = json_encode($payment_data);
                        $online_payment->paid_from = bookingPlatform(@$input['platform']);
                        $online_payment->payment_datetime = now();
                        $online_payment->save();
                        /**************************************** */
                        // update customer payments
                        $customer_payment = new CustomerPayments();
                        $customer_payment->day_service_id = 0;
                        $customer_payment->customer_id = $booking->customer_id;
                        $customer_payment->paid_amount = $payment_data['amount'] / 100;
                        $customer_payment->payment_method = $payment_type->id;
                        $customer_payment->paid_at = bookingPlatform(@$input['platform']);
                        $customer_payment->paid_at_id = 1;
                        $customer_payment->booking_id = $booking->booking_id;
                        $customer_payment->paid_datetime = now();
                        $customer_payment->save();
                    }
                    /******************************************************************** */
                    send_booking_confirmation_mail_to_any($booking->booking_id);
                    send_booking_confirmation_sms_to_customer($booking->booking_id);
                    pushNotification($customer, ['title' => "Booking Received !", 'body' => "Booking received with Ref. Id " . $booking->reference_id . "."], ['screen' => 'BOOKING_HISTORY']);
                    $params = [
                        'reference_id' => $booking->reference_id,
                        'booking_id' => $booking->booking_id,
                        'payment_method' => $payment_type->name,
                        'status' => 'Confirmed',
                    ];
                    $response['booking_details'] = $params;
                    $response['status'] = 'success';
                    $response['message'] = 'Payment verified successfully';
                    return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
                } else {
                    //payment failed
                    throw new \ErrorException('Payment verification failed');
                }
            } else {
                throw new \ErrorException('Payment token verification failed');
            }
        } catch (\Exception $e) {
            Mail::send(
                'emails.debug_mail',
                [
                    'input' => json_encode($data)
                ],
                function ($m) {
                    $m->from(Config::get('mail.mail_from_address'), Config::get('mail.mail_from_name'));
                    $m->to('samnad.s@azinova.info', 'Samnad S');
                    $m->subject('Apple Pay Test');
                }
            );
            $params = [
                'reference_id' => @$booking->reference_id,
                'booking_id' => @$booking->booking_id,
                'payment_method' => $payment_type->name,
                'status' => 'Failed',
            ];
            $response['booking_details'] = $params;
            $response['status'] = 'failed';
            $response['message'] = $e->getMessage();
            return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        /********************************************************************************* */
    }
    public function payfort_tokenization(Request $request)
    {
        $requestParams = [
            'service_command' => 'CREATE_TOKEN',
            'access_code' => Config::get('values.payfort_access_code'),
            'merchant_identifier' => Config::get('values.payfort_merchant_identifier'),
            'merchant_reference' => 'TEST-' . time(),
            'language' => strtolower(Config::get('values.language_code')),
            'card_number' => '4005550000000001',
            'expiry_date' => '2505',
            'card_security_code' => '123',
            'return_url' => url('api/customer/payment/payfort/processing'),
            'currency' => Config::get('values.currency_code'),
            'card_holder_name' => 'tedst',
            //"token_name" => "d59ee60c238848d99fff566c58006d23",
        ];
        ksort($requestParams);
        $hash_value = Config::get('values.payfort_sha_request_phrase');
        $excludes = ['card_number', 'expiry_date', 'card_security_code'];
        foreach ($requestParams as $name => $value) {
            if (!in_array($name, $excludes) && $value != null) {
                $hash_value .= $name . '=' . $value;
            }
        }
        $hash_value .= Config::get('values.payfort_sha_request_phrase');
        $requestParams['signature'] = hash('sha256', $hash_value);
        /************************************************************************ */
        $client = new \GuzzleHttp\Client([
            'verify' => false,
        ]);
        $resp = $client->request('POST', Config::get('values.payfort_api_url'), [
            'headers' => [
                'Content-Type' => 'application/json',
            ],
            'json' => $requestParams,

        ]);
        $responseBody = json_decode((string) $resp->getBody(), true);
        /************************************************************************ */
    }
    public function tokenization_form(Request $request)
    {
        if ($request->isMethod('post')) {
            die($request->all());
        }
        return view('payment.tokenization_form', []);
    }
    public function processing(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        //$original_response = $request->post();
        //if (@$original_response['card_number']) {
            // save card detailds for future use
            /*if ($debug) {
        $original_response['merchant_reference'] = "TEST";
        }
        $booking = Booking::where('reference_id', $original_response['merchant_reference'])->first();
        $customer_payment_token = CustomerPaymentToken::where([['customer_id', '=', $booking->customer_id], ['card_number', '=', $original_response['card_number']], ['status', '=', 1]])->first();
        if ($customer_payment_token) {
        $customer_payment_token->payment_token = $original_response['token_name'];
        } else {
        $customer_payment_token = new CustomerPaymentToken();
        $customer_payment_token->customer_id = $booking->customer_id;
        $customer_payment_token->payment_token = $original_response['token_name'];
        $customer_payment_token->card_number = $original_response['card_number'];
        $customer_payment_token->card_holder_name = $original_response['card_holder_name'];
        $customer_payment_token->expiry_date = $original_response['expiry_date'];
        $customer_payment_token->card_type = $original_response['payment_option'];
        $customer_payment_token->addedAt = Carbon::now();
        }
        $customer_payment_token->save();*/
        //}
        return view('payment.processing', ['post' => $request]);
    }
    public function iframe(Request $request)
    {
        return view('payment.iframe', []);
    }
    public function success(Request $request)
    {
        return view('payment.payfort_success');
        /*$debug = toggleDebug();
    $response['status'] = 'success';
    $response['message'] = 'Payment Success !';
    $response['booking_details'] = array('payment_method' => 'Card', 'status' => 'Waiting');
    return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());*/
    }
    public function failed(Request $request)
    {
        return view('payment.payfort_failed');
        /*$debug = toggleDebug();
    $response['status'] = 'failed';
    $response['message'] = 'Payment Failed !';
    $response['booking_details'] = array('payment_method' => 'Card', 'status' => 'Payment Pending');
    return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());*/
    }
    public function verify(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        try {
            if (@$request->merchant_reference && strtolower($request->status) == 'success') {
                // payment success
                if ($debug) {
                    //$merchant_reference = "TEST";
                }
                $merchant_reference = $request->merchant_reference;
                // update payment data on database
                $booking = Booking::where('reference_id', $merchant_reference)->first();
                $onlinepayment = OnlinePayment::where([['transaction_id', '=', $request->order], ['booking_id', '=', $booking->booking_id]])->first();
                if (!$onlinepayment) {
                    $payment = new OnlinePayment();
                    $payment->booking_id = $booking->booking_id;
                    $payment->transaction_id = $request->order;
                    $payment->amount = $request->amount;
                    $payment->transaction_charge = 0;
                    $payment->customer_id = $booking->customer_id;
                    $payment->description = null;
                    $payment->payment_status = strtolower($request->status);
                    $payment->ip_address = @$request->ip();
                    $payment->user_agent = @$request->header('User-Agent');
                    $payment->post_data = json_encode($request->all());
                    $payment->paid_from = 'M';
                    $payment->payment_type = 'card';
                    $payment->payment_datetime = Carbon::now();
                    // $payment->debitType = "MANUAL";
                    $payment->save();
                    $payment->reference_id = $booking->reference_id;
                    $payment->save();
                } else {
                    $onlinepayment->payment_status = strtolower($request->status);
                    $onlinepayment->save();
                }
                // update booking to active
                $booking = Booking::where('reference_id', $merchant_reference)->first();
                if ($booking->booking_common_id) {
                    $bookings = Booking::where('booking_common_id', $booking->booking_common_id)->get();
                    $bookings_ids = array_column($bookings->toArray(), 'booking_id');
                } else {
                    $bookings_ids = [$booking->booking_id];
                }
                Booking::whereIn('booking_id', $bookings_ids)->update(['booking_status' => 0, 'web_book_show' => 1]);
                /********************************************************************************************* */
                // success app notify
                $notify = new stdClass();
                $notify->customer_id = $booking->customer_id;
                $notify->booking_id = $booking->booking_id;
                $notify->service_date = $booking->booking_type == "OD" ? $booking->service_start_date : null;
                $notify->content = "Booking is confirmed with Ref. Id. {{booking_ref_id}} and payment Ref. Id. " . $request->order . ".";
                // addCustomerNotification($notify);
                /********************************************************************************************* */
                send_booking_confirmation_mail_to_any($booking->booking_common_id ?: $booking->booking_id);
                /********************************************************************************************* */
            }
            $response['data'] = $request;
            $response['merchant_reference'] = @$request->merchant_reference ?: null;
            $response['fort_id'] = @$request->order ?: null;
            $response['response_message'] = "success";
            $response['transaction_message'] = @strtolower($request->status) ?: null;
            // $response['transaction_code'] = @$responseBody['transaction_code'] ?: null;
            $response['captured_amount'] = @$request->amount ?: null;
            $response['message'] = 'Payment status received.';
            return Response::json(array('result' => array('status' => 'success', 'data' => $response), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        } catch (\Exception $e) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
    }
    public function verify_old(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        $arrData = array(
            'query_command' => 'CHECK_STATUS',
            'access_code' => Config::get('values.payfort_access_code'),
            'merchant_identifier' => Config::get('values.payfort_merchant_identifier'),
            'merchant_reference' => $request->merchant_reference,
            'language' => 'en',
        );
        ksort($arrData);
        $hash_value = Config::get('values.payfort_sha_request_phrase');
        foreach ($arrData as $name => $value) {
            $hash_value .= $name . '=' . $value;
        }
        $hash_value .= Config::get('values.payfort_sha_request_phrase');
        $arrData['signature'] = hash('sha256', $hash_value);
        $client = new \GuzzleHttp\Client([
            'verify' => false,
        ]);
        $resp = $client->request('POST', Config::get('values.payfort_api_url'), [
            'headers' => [
                'Content-Type' => 'application/json',
            ],
            'json' => $arrData,

        ]);
        $responseBody = json_decode((string) $resp->getBody(), true);
        //$original_response = json_decode(html_entity_decode($request->response), true);
        try {
            if (@$responseBody['merchant_reference'] && @$responseBody['fort_id']) {
                // payment success
                if ($debug) {
                    $responseBody['merchant_reference'] = "TEST";
                }
                // update payment data on database
                $booking = Booking::where('reference_id', $responseBody['merchant_reference'])->first();
                $onlinepayment = OnlinePayment::where([['transaction_id', '=', $responseBody['fort_id']], ['booking_id', '=', $booking->booking_id]])->first();
                if (!$onlinepayment) {
                    $payment = new OnlinePayment();
                    $payment->booking_id = $booking->booking_id;
                    $payment->transaction_id = $responseBody['fort_id'];
                    $payment->amount = $responseBody['captured_amount'] / 100;
                    $payment->transaction_charge = 0;
                    $payment->customer_id = $booking->booking_id;
                    $payment->description = null;
                    $payment->payment_status = strtolower($responseBody['transaction_message']);
                    $payment->ip_address = @$request->ip();
                    $payment->user_agent = @$request->header('User-Agent');
                    $payment->post_data = json_encode($responseBody);
                    $payment->paid_from = 'M';
                    $payment->payment_datetime = Carbon::now();
                    $payment->debitType = "MANUAL";
                    $payment->save();
                    $payment->reference_id = $booking->reference_id;
                    $payment->save();
                } else {
                    $onlinepayment->payment_status = strtolower($responseBody['transaction_message']);
                    $onlinepayment->save();
                }
                // update booking to active
                $booking = Booking::where('reference_id', $responseBody['merchant_reference'])->first();
                if ($booking->booking_common_id) {
                    $bookings = Booking::where('booking_common_id', $booking->booking_common_id)->get();
                    $bookings_ids = array_column($bookings->toArray(), 'booking_id');
                } else {
                    $bookings_ids = [$booking->booking_id];
                }
                Booking::whereIn('booking_id', $bookings_ids)->update(['booking_status' => 0, 'web_book_show' => 1]);
                /********************************************************************************************* */
                // success app notify
                $notify = new stdClass();
                $notify->customer_id = $booking->customer_id;
                $notify->booking_id = $booking->booking_id;
                $notify->service_date = $booking->booking_type == "OD" ? $booking->service_start_date : null;
                $notify->content = "Booking is confirmed with Ref. Id. {{booking_ref_id}} and payment Ref. Id. " . $responseBody['fort_id'] . ".";
                addCustomerNotification($notify);
                /********************************************************************************************* */
                // send payment confirmation mail to customer
                try {
                    if ($booking->booking_common_id) {
                        $bookings = Booking::where('booking_common_id', $booking->booking_common_id)->get();
                    }
                    $customer = Customer::where('customer_id', $booking->customer_id)->first();
                    $customer_address = CustomerAddress::where(['customer_address_id' => $booking->customer_address_id])->first();
                    $frequency = Frequencies::where('code', $booking->booking_type)->first();
                    $payment_type = DB::table('payment_types as pt')->select('pt.*', )->where(['pt.id' => $booking->payment_type_id])->first();
                    $tax_amount = number_format((($responseBody['captured_amount'] / 100) * Config::get('values.vat_percentage')) / (100 + Config::get('values.vat_percentage')), 2);
                    Mail::send(
                        'emails.to_customer.booking_payment_confirmation_to_customer',
                        [
                            'customer' => $customer,
                            'booking' => $booking,
                            'bookings' => @$bookings ?: [],
                            'frequency' => $frequency,
                            'customer_address' => $customer_address,
                            'payment_type' => $payment_type,
                            'payment' => array(
                                'total' => ($responseBody['captured_amount'] / 100) - $tax_amount,
                                'tax_amount' => $tax_amount,
                                'total_payable' => $responseBody['captured_amount'] / 100,
                                'transaction_id' => $responseBody['fort_id'],
                                'transaction_date' => date('d/m/Y'),
                            ),
                        ],
                        function ($m) use ($debug, $customer) {
                            $m->from(env('MAIL_USERNAME'), 'Company Admin');
                            if (strpos($_SERVER['REQUEST_URI'], 'live') !== false) {
                                $m->to(@$customer->email_address)->subject('Company Payment Received');
                            } else {
                                $m->to('samnad.s@azinova.info')->subject('Company Payment Received');
                            }
                        }
                    );
                } catch (\Exception $e) {
                    //return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
                }
                /********************************************************************************************* */
            }
            $response['data'] = $responseBody;
            $response['merchant_reference'] = @$responseBody['merchant_reference'] ?: null;
            $response['fort_id'] = @$responseBody['fort_id'] ?: null;
            $response['response_message'] = @$responseBody['response_message'] ?: null;
            $response['transaction_message'] = @$responseBody['transaction_message'] ?: null;
            $response['transaction_code'] = @$responseBody['transaction_code'] ?: null;
            $response['captured_amount'] = @$responseBody['captured_amount'] ?: null;
            $response['message'] = 'Payment status received.';
            return Response::json(array('result' => array('status' => 'success', 'data' => $response), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        } catch (\Exception $e) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
    }
    public function redirect(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
            //$data['params']['booking_id'] = 359556;
        } else {
            // test input
            $data['params']['booking_id'] = 1;
        }
        /************************************************************* */
        // required input check
        $input = @$data['params'];
        $validator = Validator::make(
            (array) $input,
            [
                'booking_id' => 'required|integer',
            ],
            [],
            [
                'booking_id' => 'Booking ID',
            ]
        );
        if ($validator->fails()) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        /************************************************************* */
        if ($debug) {
            // test input
            $data['params']['booking_id'] = "ID_" . time();
            $data['params']['customer'] = new stdClass();
            $data['params']['booking_reference'] = "REF_" . time();
            $data['params']['amount'] = 1.5;
        } else {
            $booking = DB::table('bookings as b')
                ->select(
                    'b.*',
                    'so.no_of_visits',
                )
                ->leftJoin('special_offers as so', 'b.special_offer_id', 'so.id')
                ->where([['b.booking_id', '=', $input['booking_id']]])
                ->first();
            /************************************************************* */
            $bookings_count = 1;
            if ($booking->booking_common_id) {
                $bookings = DB::table('bookings as b')
                    ->select(
                        'b.*',
                    )
                    ->where([['b.booking_common_id', '=', $booking->booking_common_id]])
                    ->get();
                $bookings_count = sizeof($bookings);
            }
            $data['params']['booking_id'] = $booking->booking_id;
            $data['params']['customer'] = Customer::where('customer_id', $booking->customer_id)->first();
            $data['params']['booking_reference'] = @$booking->reference_id ?: $booking->booking_id;
            if ($booking->special_offer_id) {
                //$data['params']['amount'] = ($booking->total_amount * $bookings_count) * $booking->no_of_visits;
            } else {
                //$data['params']['amount'] = $booking->total_amount * $bookings_count;
            }
            $data['params']['amount'] = $input['bookingAmount'];
        }
        /************************************************************* */
        return view('payment.redirect', ['data' => $data['params']]);
    }
    public function payfort_transaction_feedback(Request $request)
    {
        Mail::send(
            'emails.test',
            [
            ],
            function ($m) {
                $m->from(env('MAIL_USERNAME'), 'Company Admin');
                $m->to('samnad.s@azinova.info')->subject('Company Feedback Test');
            }
        );
    }
    public function checkout_gateway_entry(Request $request)
    {
        $segments = $request->segments();
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
            // $data['params']['booking_id'] = 10;
        } else {
            // test input
            /*$data['params']['booking_id'] = $segments[4];
            $data['params']['customer'] = new stdClass();
            $data['params']['booking_reference'] = "REF_" . time();
            $data['params']['amount'] = 73;*/
        }
        /************************************************************* */
        //$data['params']['amount'] = 1; // TEST PURPOSE ONLY
        //Log::debug(json_encode(@$data));
        $input = $request->all();
        $validator = Validator::make(
            (array) $input,
            [
                'booking_id' => 'required|integer',
            ],
            [],
            [
                'booking_id' => 'Booking ID',
            ]
        );
        if ($validator->fails()) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        /************************************************************* */
        $data['input'] = $request->all();
        $data['booking'] = DB::table('bookings as b')
            ->select('b.*', )
            ->where([['b.booking_id', '=', $input['booking_id']]])
            ->first();
        $data['customer'] = DB::table('customers as c')
            ->select('c.*', )
            ->where([['c.customer_id', '=', $data['booking']->customer_id]])
            ->first();
        // return view('payment.gateway-entry.ngenius_entry', $data);
        return view('payment.gateway-entry.ccavenue_entry', $data);
    }
    public function checkout_gateway_go(Request $request)
    {
        // $outlet = "beb5e951-90f9-4e17-b87d-e74e43489c6f";
        $outlet = "3072049e-4d9d-4522-b64a-7657c38eaf95";
        // $apikey = "MzUyNjI5MzUtOTA5ZS00MzU2LWFhZDktNmE3NzgwNGY3ODdjOjY5Y2EwMzM3LTQwMjgtNDFkYi1iNjVhLTI0NDFkZTk4OWQ3Mg==";
        $apikey = "YjU1ZDQ4MWMtZjA1Ni00Yzg3LTk0YzUtMjkxNzdhYTcwMDgzOmQyNzRmODQ5LTg3ZDktNDVkZS04OTJkLTI3NzU3NWZjMmMxOQ==";
        if ($request->sessionId != "") 
        {
            $session = $request->sessionId;
            $amount = $request->amount;
            $f_name = $request->f_name;
            $l_name = $request->l_name;
            $email = $request->email;
            // $addr = $request->f_name;$this->input->post('addr');
            $custid = $request->cust_id;
            $reference = $request->reference;
            $customer_mobile = $request->customer_mobile;
            $booking_id = $request->booking_id;
            $service_date = $request->service_date;
            $in_amt=($amount/100);
            $curdatetime=date("Y-m-d H:i:s");

            try { 
                $idData = $this->identify($apikey);
                if (isset($idData->access_token)) {
                    $token = $idData->access_token;
                    $payData = $this->pay($session, $token, $outlet, $amount, $f_name, $l_name, $email, $custid, $customer_mobile,$booking_id,$reference,$service_date);
                    echo(json_encode($payData));
                    exit();
                }
            } catch (Exception $e) {
                echo($e->getMessage());
            }
        }
    }
    function identify($apikey) {
	
        // $idUrl = "https://identity.ngenius-payments.com/auth/realms/NetworkInternational/protocol/openid-connect/token";
        $idUrl = "https://identity-uat.ngenius-payments.com/auth/realms/ni/protocol/openid-connect/token";
        $idHead = array("Authorization: Basic ".$apikey, "Content-Type: application/x-www-form-urlencoded");
        $idPost = http_build_query(array('grant_type' => 'client_credentials'));
        $idOutput = $this->invokeCurlRequest("POST", $idUrl, $idHead, $idPost,true);
        return $idOutput;
    }
    function pay($session, $token, $outlet, $amount, $f_name, $l_name, $email, $custid, $phone,$bookingId,$reference,$service_date) {  //$paymentid = Online table,$bookingId = Booking id
    
        $refnce = $reference;
        $ord = new stdClass;
        $ord->action = "SALE";
        $ord->amount = new stdClass;
        $ord->amount->currencyCode = "AED";
        $ord->amount->value = $amount;
        $ord->merchantOrderReference =$refnce;
        $ord->emailAddress =$email;
        $ord->billingAddress = new StdClass();
        $ord->billingAddress->firstName = $f_name;
        $ord->billingAddress->lastName = $l_name;
        // $ord->billingAddress->address1 = $addr;
        $ord->billingAddress->city = 'Dubai';
        $ord->billingAddress->countryCode = 'UAE';
        $ord->merchantDefinedData = new StdClass();
        $ord->merchantDefinedData->custId = $custid;
        $ord->merchantDefinedData->mobileNo = $phone;
        // $ord->merchantDefinedData->paymentid = $paymntid;
        $ord->merchantDefinedData->booking_id = $bookingId;
        $ord->merchantDefinedData->service_date = $service_date;
    
        // $payUrl = "https://api-gateway.ngenius-payments.com/transactions/outlets/".$outlet."/payment/hosted-session/".$session;
        $payUrl = "https://api-gateway-uat.ngenius-payments.com/transactions/outlets/".$outlet."/payment/hosted-session/".$session;
        $payHead = array("Authorization: Bearer ".$token, "Content-Type: application/vnd.ni-payment.v2+json", "Accept: application/vnd.ni-payment.v2+json");
        $payPost = json_encode($ord); 
        $payOutput = $this->invokeCurlRequest("POST", $payUrl, $payHead, $payPost, true);
        return $payOutput;
    }
    function invokeCurlRequest($type, $url, $headers, $post, $json) 
    {
        
        $ch = curl_init();
        // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, $certificate_location);
        // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $certificate_location);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if ($type == "POST" || $type == "PUT") {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
            if ($type == "PUT") {
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
            }
        }
        $server_output = curl_exec ($ch);
        return json_decode($server_output);
    }
    public function checkout_gateway_go_old(Request $request)
    {
        $booking = DB::table('bookings as b')
            ->select('b.*', )
            ->where([['b.booking_id', '=', $request->booking_id]])
            ->first();
        /**************************************************************** */
        $checkout_data['source'] = array("type" => "token", "token" => $request->token);
        $checkout_data['customer'] = array("email" => $request->email, "name" => $request->name);
        $checkout_data['3ds'] = array("enabled" => true);
        $checkout_data['amount'] = (int) $request->amount;
        $checkout_data['currency'] = $request->currency;
        $checkout_data['reference'] = $request->reference;
        $checkout_data['processing_channel_id'] = Config::get('values.checkout_channel_id');
        $checkout_data['success_url'] = url('payment/gateway/checkout/process/' . $booking->booking_id . '');
        $checkout_data['failure_url'] = url('payment/gateway/checkout/process/' . $booking->booking_id . '');
        $post_header = array("Authorization" => "Bearer " . Config::get('values.checkout_secret_key'), "Content-Type" => "application/json", "Accept" => "application/json");
        /**************************************************************** */
        try {
            $client = new \GuzzleHttp\Client([
                'verify' => false,
            ]);
            $response = $client->request('POST', Config::get('values.checkout_service_url'), [
                'headers' => $post_header,
                'json' => $checkout_data,
            ]);
            $result = json_decode((string) $response->getBody(), true);
            if (!isset($result['error_type'])) {
                $status = $result['status'];
                if ($status == 'Pending') {
                    $redirect_to = $result['_links']['redirect']['href'];
                    return Redirect::to($redirect_to);
                } else {
                    return 'failed';
                }
            } else {
                return 'failed';
            }
        } catch (\Exception $e) {
            return 'Payment Error !';
        }
        /**************************************************************** */
    }
    public function checkout_payment_process(Request $request)
    {
        try {
            $segments = $request->segments();
            $booking_common_id = $segments[4];
            $orderReference = $segments[5];
            $booking = Booking::where('booking_id', $booking_common_id)->firstOrFail();
            $bookings = DB::table('bookings as b')
                ->select(
                    'b.*',
                )
                ->where(function ($query) use ($booking_common_id) {
                    $query->where('b.booking_common_id', $booking_common_id);
                    $query->orWhere('b.booking_id', $booking_common_id);
                })
                ->orderBy('b.booking_id', 'ASC')
                ->get();
            $bookings_ids = array_column($bookings->toArray(), 'booking_id');
            
            // $outlet = "570556cc-13f7-4160-9eaf-e21c17f7f0a6";
            // $apikey = "MGZiY2EyYmQtODBkMi00MDRjLTliZjYtZTE2ZjI2ZGRmZjliOjEwNjgxMzIxLWVkM2QtNDJhMC1iNDA3LWRjYTI0MjgzMjVhZQ==";
            $outlet = "3072049e-4d9d-4522-b64a-7657c38eaf95";
            $apikey = "YjU1ZDQ4MWMtZjA1Ni00Yzg3LTk0YzUtMjkxNzdhYTcwMDgzOmQyNzRmODQ5LTg3ZDktNDVkZS04OTJkLTI3NzU3NWZjMmMxOQ==";
            if($orderReference != "")
            {
                $idData = $this->identify($apikey);
                if (isset($idData->access_token)) 
                {
                    $token = $idData->access_token;
                    $statusData = $this->orderSstatus($orderReference, $token, $outlet);
                    $res=json_decode($statusData, true);
                    $pay_stat=$res['_embedded']['payment'][0]['state'];
                    $transactionId=$res['reference'];
                    $pay_amt=$res['amount']['value'];
                    $pay_amt=($pay_amt/100);
                    if(strtolower($pay_stat)=='captured')
                    {
                        /********************************************************************
                         * PAYMENT SUCCESS
                         *********************************************************************/
                        // show on web bookings in admin
                        Booking::whereIn('booking_id', $bookings_ids)->update(['booking_status' => 0, 'web_book_show' => 1]);
                        $params = [
                            'cko-session-id' => @$request['cko-session-id'],
                            'reference_id' => $booking->reference_id,
                            'booking_id' => $booking->booking_id,
                            'payment_method' => 'Card',
                            'status' => 'Confirmed',
                        ];
                        /******************************************************************** */
                        // update database
                        $online_payment = OnlinePayment::where('booking_id', $booking->booking_id)->where('reference_id', $booking->reference_id)->first();
                        if ($online_payment) {
                            if ($online_payment->payment_status != 'success') {
                                $online_payment->transaction_id = $transactionId;
                                $online_payment->amount = $pay_amt;
                                $online_payment->transaction_charge = 0;
                                $online_payment->customer_id = $booking->customer_id;
                                $online_payment->description = null;
                                $online_payment->payment_status = 'success';
                                $online_payment->payment_type = 'card';
                                $online_payment->user_agent = @$_SERVER['HTTP_USER_AGENT'];
                                $online_payment->post_data = null;
                                $online_payment->return_data = json_encode($res);
                                $online_payment->paid_from = 'A';
                                $online_payment->payment_datetime = date('Y-m-d H:i:s');
                                $online_payment->save();
                            } else {
                                return 'Payment Already Verfied !';
                            }
                        } else {
                            $online_payment = new OnlinePayment();
                            $online_payment->transaction_id = $transactionId;
                            $online_payment->booking_id = $booking->booking_id;
                            $online_payment->reference_id = $booking->reference_id;
                            $online_payment->amount = $pay_amt;
                            $online_payment->transaction_charge = 0;
                            $online_payment->customer_id = $booking->customer_id;
                            $online_payment->description = null;
                            $online_payment->payment_status = 'success';
                            $online_payment->payment_type = 'card';
                            $online_payment->user_agent = @$_SERVER['HTTP_USER_AGENT'];
                            $online_payment->post_data = null;
                            $online_payment->return_data = json_encode($res);
                            $online_payment->paid_from = 'A';
                            $online_payment->payment_datetime = date('Y-m-d H:i:s');
                            $online_payment->save();
                            /**************************************** */
                            // update customer payments
                            $customer_payment = new CustomerPayments();
                            $customer_payment->day_service_id = 0;
                            $customer_payment->customer_id = $booking->customer_id;
                            $customer_payment->paid_amount = $pay_amt;
                            $customer_payment->payment_method = 2;
                            $customer_payment->paid_at = 'M';
                            $customer_payment->paid_at_id = 1;
                            $customer_payment->booking_id = $booking->booking_id;
                            $customer_payment->paid_datetime = date('Y-m-d H:i:s');
                            $customer_payment->save();
                        }
                        /******************************************************************** */
                        $customer = Customer::where('customer_id', '=', $booking->customer_id)->first();
                        send_booking_confirmation_mail_to_any($booking->booking_id);
                        // send_booking_confirmation_sms_to_customer($booking->booking_id);
                        // pushNotification($customer, ['title' => "Booking Received !", 'body' => "Booking received with Ref. Id " . $booking->reference_id . "."], ['screen' => 'BOOKING_HISTORY']);
                        return redirect()->route('checkout-success', $params);
                    } else {
                        // payment failed
                        $params = [
                            'cko-session-id' => @$request['cko-session-id'],
                            'reference_id' => $booking->reference_id,
                            'booking_id' => $booking->booking_id,
                            'payment_method' => 'Card',
                            'status' => $response['status'],
                        ];
                        return redirect()->route('checkout-failed', $params);
                    }
                } else {
                    // payment failed
                    $params = [
                        'cko-session-id' => @$request['cko-session-id'],
                        'reference_id' => $booking->reference_id,
                        'booking_id' => $booking->booking_id,
                        'payment_method' => 'Card',
                        'status' => $response['status'],
                    ];
                    return redirect()->route('checkout-failed', $params);
                }
            } else {
                // payment failed
                $params = [
                    'cko-session-id' => @$request['cko-session-id'],
                    'reference_id' => $booking->reference_id,
                    'booking_id' => $booking->booking_id,
                    'payment_method' => 'Card',
                    'status' => $response['status'],
                ];
                return redirect()->route('checkout-failed', $params);
            }
        } catch (\Exception $e) {
            // payment failed
            $params = [
                'cko-session-id' => @$request['cko-session-id'],
                'exception' => @$e->getMessage(),
                'reference_id' => @$booking->reference_id,
                'booking_id' => @$booking->booking_id,
                'payment_method' => 'Card',
                'status' => 'Failed',
            ];
            return redirect()->route('checkout-failed', $params);
        }
    }
    public function checkout_payment_process_old(Request $request)
    {
        try {
            $segments = $request->segments();
            $booking_common_id = $segments[4];
            $booking = Booking::where('booking_id', $booking_common_id)->firstOrFail();
            $bookings = DB::table('bookings as b')
                ->select(
                    'b.*',
                )
                ->where(function ($query) use ($booking_common_id) {
                    $query->where('b.booking_common_id', $booking_common_id);
                    $query->orWhere('b.booking_id', $booking_common_id);
                })
                ->orderBy('b.booking_id', 'ASC')
                ->get();
            $bookings_ids = array_column($bookings->toArray(), 'booking_id');
            $post_header = array("Authorization" => "Bearer " . Config::get('values.checkout_secret_key'), "Content-Type" => "application/json", "Accept" => "application/json");
            $client = new \GuzzleHttp\Client([
                'verify' => false,
            ]);
            $responseFull = $client->request('GET', Config::get('values.checkout_service_url') . $request['cko-session-id'], [
                'headers' => $post_header,
            ]);
            $response = json_decode((string) $responseFull->getBody(), true);
            if ($response['status']) {
                $success_statuses = ['authorized', 'captured'];
                if (in_array(strtolower($response['status']), $success_statuses)) {
                    /********************************************************************
                     * 
                     * 
                     * PAYMENT SUCCESS
                     * 
                     * 
                     *********************************************************************/
                    // show on web bookings in admin
                    Booking::whereIn('booking_id', $bookings_ids)->update(['booking_status' => 0, 'web_book_show' => 1]);
                    $params = [
                        'cko-session-id' => @$request['cko-session-id'],
                        'reference_id' => $booking->reference_id,
                        'booking_id' => $booking->booking_id,
                        'payment_method' => 'Card',
                        'status' => 'Confirmed',
                    ];
                    /******************************************************************** */
                    // update database
                    $online_payment = OnlinePayment::where('booking_id', $booking->booking_id)->where('reference_id', $booking->reference_id)->first();
                    if ($online_payment) {
                        if ($online_payment->payment_status != 'success') {
                            $online_payment->transaction_id = $response['id'];
                            $online_payment->amount = $response['amount'] / 100;
                            $online_payment->transaction_charge = 0;
                            $online_payment->customer_id = $booking->customer_id;
                            $online_payment->description = null;
                            $online_payment->payment_status = 'success';
                            $online_payment->payment_type = 'card';
                            $online_payment->user_agent = @$_SERVER['HTTP_USER_AGENT'];
                            $online_payment->post_data = null;
                            $online_payment->return_data = json_encode($response);
                            $online_payment->paid_from = 'A';
                            $online_payment->payment_datetime = date('Y-m-d H:i:s');
                            $online_payment->save();
                        } else {
                            return 'Payment Already Verfied !';
                        }
                    } else {
                        $online_payment = new OnlinePayment();
                        $online_payment->transaction_id = $response['id'];
                        $online_payment->booking_id = $booking->booking_id;
                        $online_payment->reference_id = $booking->reference_id;
                        $online_payment->amount = $response['amount'] / 100;
                        $online_payment->transaction_charge = 0;
                        $online_payment->customer_id = $booking->customer_id;
                        $online_payment->description = null;
                        $online_payment->payment_status = 'success';
                        $online_payment->payment_type = 'card';
                        $online_payment->user_agent = @$_SERVER['HTTP_USER_AGENT'];
                        $online_payment->post_data = null;
                        $online_payment->return_data = json_encode($response);
                        $online_payment->paid_from = 'A';
                        $online_payment->payment_datetime = date('Y-m-d H:i:s');
                        $online_payment->save();
                        /**************************************** */
                        // update customer payments
                        $customer_payment = new CustomerPayments();
                        $customer_payment->day_service_id = 0;
                        $customer_payment->customer_id = $booking->customer_id;
                        $customer_payment->paid_amount = $response['amount'] / 100;
                        $customer_payment->payment_method = 2;
                        $customer_payment->paid_at = 'M';
                        $customer_payment->paid_at_id = 1;
                        $customer_payment->booking_id = $booking->booking_id;
                        $customer_payment->paid_datetime = date('Y-m-d H:i:s');
                        $customer_payment->save();
                    }
                    /******************************************************************** */
                    $customer = Customer::where('customer_id', '=', $booking->customer_id)->first();
                    send_booking_confirmation_mail_to_any($booking->booking_id);
                    send_booking_confirmation_sms_to_customer($booking->booking_id);
                    pushNotification($customer, ['title' => "Booking Received !", 'body' => "Booking received with Ref. Id " . $booking->reference_id . "."], ['screen' => 'BOOKING_HISTORY']);
                    return redirect()->route('checkout-success', $params);
                } else {
                    // payment failed
                    $params = [
                        'cko-session-id' => @$request['cko-session-id'],
                        'reference_id' => $booking->reference_id,
                        'booking_id' => $booking->booking_id,
                        'payment_method' => 'Card',
                        'status' => $response['status'],
                    ];
                    return redirect()->route('checkout-failed', $params);
                }
            } else {
                // payment failed
            }
        } catch (\Exception $e) {
            // payment failed
            $params = [
                'cko-session-id' => @$request['cko-session-id'],
                'exception' => @$e->getMessage(),
                'reference_id' => @$booking->reference_id,
                'booking_id' => @$booking->booking_id,
                'payment_method' => 'Card',
                'status' => 'Failed',
            ];
            return redirect()->route('checkout-failed', $params);
        }
    }
    public function checkout_success(Request $request)
    {
        return 'Success !';
        //return '<span style="color:green"><b>Success !</b></span><br><br><br>CURRENT URL - ' . url()->full();
    }
    public function checkout_failed(Request $request)
    {
        return 'Failed !';
        //return '<span style="color:red"><b>Failed !</b></span><br><br><br>CURRENT URL - ' . url()->full();
    }
    public function checkoutPaymentProcessAndRedirect(Request $request)
    {
        // this (route) should be called from checkout.com
        $segments = $request->segments();
        $booking_common_id = $segments[5];
        $booking = Booking::where('booking_id', $booking_common_id)->first();
        try {
            $bookings = DB::table('bookings as b')
                ->select(
                    'b.*',
                )
                ->where(function ($query) use ($booking_common_id) {
                    $query->where('b.booking_common_id', $booking_common_id);
                    $query->orWhere('b.booking_id', $booking_common_id);
                })
                ->orderBy('b.booking_id', 'ASC')
                ->get();
            $bookings_ids = array_column($bookings->toArray(), 'booking_id');
            $post_header = array("Authorization" => "Bearer " . Config::get('values.checkout_secret_key'), "Content-Type" => "application/json", "Accept" => "application/json");
            $client = new \GuzzleHttp\Client([
                'verify' => false,
            ]);
            $responseFull = $client->request('GET', Config::get('values.checkout_service_url') . $request['cko-session-id'], [
                'headers' => $post_header,
            ]);
            $checkout_data = json_decode((string) $responseFull->getBody(), true);
            //dd($checkout_data);
            if (@$checkout_data['approved'] == true) {
                // payment success
                afterCheckoutPaymentSuccess($booking_common_id, $checkout_data, null);
                return Redirect::to(Config::get('values.web_app_url') . 'booking/success/' . $booking->reference_id);
            } else {
                // payment failed
                return Redirect::to(Config::get('values.web_app_url') . 'booking/failed/' . $booking->reference_id);
            }
        } catch (\Exception $e) {
            // payment failed
            return Redirect::to(Config::get('values.web_app_url') . 'booking/failed/' . $booking->reference_id . '?error=' . $e->getMessage());
        }
    }
    public function tamaraPaymentProcessAndRedirect(Request $request)
    {
        // this (route) should be called from checkout.com
        //dd($request);
        $segments = $request->segments();
        $booking_common_id = $segments[5];
        $booking = Booking::where('booking_id', $booking_common_id)->first();
        try {
            $bookings = DB::table('bookings as b')
                ->select(
                    'b.*',
                )
                ->where(function ($query) use ($booking_common_id) {
                    $query->where('b.booking_common_id', $booking_common_id);
                    $query->orWhere('b.booking_id', $booking_common_id);
                })
                ->orderBy('b.booking_id', 'ASC')
                ->get();
            $bookings_ids = array_column($bookings->toArray(), 'booking_id');
            $post_header = array("Authorization" => "Bearer " . Config::get('values.tamara_api_token'), "Content-Type" => "application/json", "Accept" => "application/json");
            $client = new \GuzzleHttp\Client([
                'verify' => false,
            ]);
            $responseFull = $client->request('GET', Config::get('values.tamara_base_url') . "orders/" . $request['orderId'], [
                'headers' => $post_header,
            ]);
            $checkout_data = json_decode((string) $responseFull->getBody(), true);
            //dd($checkout_data);
            if (@$checkout_data['status'] == "authorised") {
                // payment success
                afterTamaraPaymentSuccess($booking_common_id, $checkout_data, null);
                return Redirect::to(Config::get('values.web_app_url') . 'booking/success/' . $booking->reference_id);
            } else {
                // payment failed
                return Redirect::to(Config::get('values.web_app_url') . 'booking/failed/' . $booking->reference_id);
            }
        } catch (\Exception $e) {
            // payment failed
            return Redirect::to(Config::get('values.web_app_url') . 'booking/failed/' . $booking->reference_id . '?error=' . $e->getMessage());
        }
    }

    function orderSstatus($orderReference, $token, $outlet) {        
        // construct order object JSON
        $payPost = '';
        $statusUrl = "https://api-gateway-uat.ngenius-payments.com/transactions/outlets/".$outlet."/orders/".$orderReference;
        // $statusUrl = "https://api-gateway.ngenius-payments.com/transactions/outlets/".$outlet."/orders/".$orderReference;
        $payHead = array("Authorization: Bearer ".$token, "Content-Type: application/vnd.ni-payment.v2+json", "Accept: application/vnd.ni-payment.v2+json");
       // $payPost = json_encode($ord); 
    
        $payOutput = $this->invokeCurlRequestResponse("GET", $statusUrl, $payHead, $payPost);
        return $payOutput;
    }

    function invokeCurlRequestResponse($type, $url, $headers, $post) {
        $ch = curl_init();
    
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);       
    
        if ($type == "POST" || $type == "PUT") {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
            if ($type == "PUT") {
              curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
            }
        }
    
        $server_output = curl_exec ($ch);
       // $res = json_decode($server_output);
        return $server_output;
    
    }

    public function checkout_outstanding_gateway_entry(Request $request)
    {
        $segments = $request->segments();
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
            // $data['params']['booking_id'] = 10;
        } else {
            // test input
            /*$data['params']['booking_id'] = $segments[4];
            $data['params']['customer'] = new stdClass();
            $data['params']['booking_reference'] = "REF_" . time();
            $data['params']['amount'] = 73;*/
        }
        /************************************************************* */
        //$data['params']['amount'] = 1; // TEST PURPOSE ONLY
        //Log::debug(json_encode(@$data));
        $input = $request->all();
        $validator = Validator::make(
            (array) $input,
            [
                'customer_id' => 'required|integer',
            ],
            [],
            [
                'customer_id' => 'Customer ID',
            ]
        );
        if ($validator->fails()) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        /************************************************************* */
        $data['input'] = $request->all();
        
        $online_payment = new OnlinePayment();
        $online_payment->amount = $request->amount;
        $online_payment->transaction_charge = 0;
        $online_payment->payment_status = 'initiated';
        $online_payment->customer_id = $request->customer_id;
        $online_payment->description = $request->description;
        $online_payment->payment_type = 'card';
        $online_payment->user_agent = @$_SERVER['HTTP_USER_AGENT'];
        $online_payment->post_data = null;
        $online_payment->paid_from = 'M';
        $online_payment->payment_datetime = date('Y-m-d H:i:s');
        $online_payment->save();
        
        $data['payment'] = $online_payment;
        $data['customer'] = DB::table('customers as c')
            ->select('c.*', )
            ->where([['c.customer_id', '=', $request->customer_id]])
            ->first();
        // return view('payment.gateway-entry.ngenius_entry', $data);
        return view('payment.gateway-entry.outstanding_ccavenue', $data);
    }

    public function outstanding_processing(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        
        return view('payment.outstanding-processing', ['post' => $request]);
    }

    public function outstanding_verify(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        try {
            if (@$request->merchant_reference && strtolower($request->status) == 'success') {
                // payment success
                if ($debug) {
                    //$merchant_reference = "TEST";
                }
                $merchant_reference = $request->merchant_reference;
                // update payment data on database
                $onlinepayment = OnlinePayment::where([['payment_id', '=', $merchant_reference]])->first();
                $onlinepayment->transaction_id = $request->order;
                $onlinepayment->amount = $request->amount;
                $onlinepayment->payment_status = strtolower($request->status);
                $onlinepayment->ip_address = @$request->ip();
                $onlinepayment->user_agent = @$request->header('User-Agent');
                $onlinepayment->post_data = json_encode($request->all());
                $onlinepayment->save();
                // update booking to active
                
                /********************************************************************************************* */
                // success app notify
                // $notify = new stdClass();
                // $notify->customer_id = $booking->customer_id;
                // $notify->booking_id = $booking->booking_id;
                // $notify->service_date = $booking->booking_type == "OD" ? $booking->service_start_date : null;
                // $notify->content = "Booking is confirmed with Ref. Id. {{booking_ref_id}} and payment Ref. Id. " . $request->order . ".";
                // addCustomerNotification($notify);
                /********************************************************************************************* */
                // send mails
                $mail = new AdminApiMailController;
                $mail->online_payment_confirm_to_customer($onlinepayment->payment_id, $request);
                $mail->online_payment_confirm_to_admin($onlinepayment->payment_id, $request);
                // send_booking_confirmation_mail_to_any($booking->booking_common_id ?: $booking->booking_id);
                /********************************************************************************************* */
            }
            $response['data'] = $request;
            $response['merchant_reference'] = @$request->merchant_reference ?: null;
            $response['fort_id'] = @$request->order ?: null;
            $response['response_message'] = "success";
            $response['transaction_message'] = @strtolower($request->status) ?: null;
            // $response['transaction_code'] = @$responseBody['transaction_code'] ?: null;
            $response['captured_amount'] = @$request->amount ?: null;
            $response['message'] = 'Payment status received.';
            return Response::json(array('result' => array('status' => 'success', 'data' => $response), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        } catch (\Exception $e) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
    }
}
