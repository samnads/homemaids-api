<?php

namespace App\Http\Controllers;

use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Response;

class CustomerApiReferralController extends Controller
{
    public function referral_credits(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
        } else {
            // test input
            $data['params']['id'] = Config::get('values.debug_customer_id');
        }
        /************************************************************* */
        // required input check
        $input = @$data['params'];
        $validator = Validator::make((array) $input,
            [
            ],
            [],
            [
            ]
        );
        if ($validator->fails()) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        /************************************************************* */
        $response['status'] = 'success';
        $response['referral_credits'] = ['referral_credit_msg' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.", 'referral_code' => 'abC23d', 'total_credits_earned' => 50, 'credits_remaining' => 20];
        $response['message'] = 'Referral Credits fetched successfully.';
        return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
    }
}
