<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServiceAddons extends Model
{
    use HasFactory;
    protected $table = 'service_addons';
    public $timestamps = true;
    protected $primaryKey = 'service_addons_id';
}
