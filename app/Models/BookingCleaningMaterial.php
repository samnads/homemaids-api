<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookingCleaningMaterial extends Model
{
    use HasFactory;
    protected $table = 'booking_cleaning_materials';
    public $timestamps = false;
    protected $primaryKey = 'id';
}
