<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookingSubscriptionPackages extends Model
{
    use HasFactory;
    protected $table = 'booking_subscription_packages';
    public $timestamps = true;
    protected $primaryKey = 'id';
}
