<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerPaymentToken extends Model
{
    use HasFactory;
    protected $table = 'customer_payment_token';
    public $timestamps = false;
    protected $primaryKey = 'token_id';
}
