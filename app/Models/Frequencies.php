<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Frequencies extends Model
{
    use HasFactory;
    protected $table = 'frequencies';
    public $timestamps = false;
    protected $primaryKey = 'id';
}
