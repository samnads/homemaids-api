<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerCoupons extends Model
{
    use HasFactory;
    protected $table = 'customer_coupons';
    public $timestamps = false;
    protected $primaryKey = 'id';
}
