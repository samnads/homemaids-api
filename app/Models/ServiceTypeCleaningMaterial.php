<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServiceTypeCleaningMaterial extends Model
{
    use HasFactory;
    protected $table = 'service_type_cleaning_materials';
    public $timestamps = true;
    protected $primaryKey = 'id';
}
