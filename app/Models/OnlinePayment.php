<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OnlinePayment extends Model
{
    use HasFactory;
    protected $table = 'online_payments';
    public $timestamps = false;
    protected $primaryKey = 'payment_id';
}
