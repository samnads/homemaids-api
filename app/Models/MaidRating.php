<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MaidRating extends Model
{
    use HasFactory;
    protected $table = 'maid_rating';
    public $timestamps = true;
    protected $primaryKey = 'id';
}
