<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SpecialOffers extends Model
{
    use HasFactory;
    protected $table = 'special_offers';
    public $timestamps = true;
    protected $primaryKey = 'id';
}
