CREATE TABLE `service_type_cleaning_materials` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `service_type_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `thumbnail_file` varchar(155) NOT NULL,
  `sort_order` int(3) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `service_type_id` (`service_type_id`),
  CONSTRAINT `service_type_cleaning_materials_ibfk_1` FOREIGN KEY (`service_type_id`) REFERENCES `service_types` (`service_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO `service_type_cleaning_materials` (`id`, `service_type_id`, `name`, `amount`, `thumbnail_file`, `sort_order`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1,	1,	'Material 1',	10.00,	'default.png',	1,	'2024-04-03 14:29:12',	'2024-04-03 14:29:12',	NULL),
(2,	1,	'Material 2',	15.00,	'default.png',	2,	'2024-04-03 14:29:12',	'2024-04-03 14:29:12',	NULL),
(3,	1,	'Material 3',	7.00,	'default.png',	3,	'2024-04-03 14:29:12',	'2024-04-03 14:29:12',	NULL),
(4,	1,	'Material 4',	5.00,	'default.png',	4,	'2024-04-03 14:29:12',	'2024-04-03 14:29:12',	NULL);
------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `booking_cleaning_materials` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `booking_id` bigint(20) unsigned NOT NULL,
  `service_type_cleaning_material_id` bigint(20) unsigned NOT NULL,
  `service_type_cleaning_material_name` varchar(255) NOT NULL,
  `unit_price` decimal(10,2) unsigned NOT NULL,
  `quantity` int(10) unsigned NOT NULL,
  `amount` decimal(10,0) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `service_type_cleaning_material_id` (`service_type_cleaning_material_id`),
  KEY `booking_id` (`booking_id`),
  CONSTRAINT `booking_cleaning_materials_ibfk_1` FOREIGN KEY (`service_type_cleaning_material_id`) REFERENCES `service_type_cleaning_materials` (`id`),
  CONSTRAINT `booking_cleaning_materials_ibfk_2` FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`booking_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;