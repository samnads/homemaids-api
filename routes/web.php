<?php

use App\Http\Controllers\PaymentFrameController;
use App\Http\Controllers\WebviewStaticPageController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Route::group(['middleware' => 'web'], function () {
    Route::get('payment/gateway/checkout/entry/{booking_id}', [PaymentFrameController::class, 'checkout_gateway_entry']); // card entry form
    Route::any('payment/gateway/checkout/go', [PaymentFrameController::class, 'checkout_gateway_go']); // checkout card form submit
    // Route::any('payment/gateway/checkout/go/{booking_id}', [PaymentFrameController::class, 'checkout_gateway_go']); // checkout card form submit
    Route::any('payment/gateway/checkout/process/{ref}/{booking_id}', [PaymentFrameController::class, 'checkout_payment_process']); // checkout data process
    Route::any('payment/gateway/checkout/success/{booking_id}', [PaymentFrameController::class, 'checkout_success'])->name('checkout-success'); // checkout success
    Route::any('payment/gateway/checkout/failed/{booking_id}', [PaymentFrameController::class, 'checkout_failed'])->name('checkout-failed'); // checkout failed
    Route::get('webview/terms_and_conditions', [WebviewStaticPageController::class, 'terms_and_conditions']);
    Route::get('webview/privacy_policy', [WebviewStaticPageController::class, 'privacy_policy']);
    Route::get('webview/about_us', [WebviewStaticPageController::class, 'about_us']);
    Route::get('webview/customer_info', [WebviewStaticPageController::class, 'customer_info']);
    Route::get('webview/material_info', [WebviewStaticPageController::class, 'material_info']);
    Route::get('outstanding/gateway/checkout/entry', [PaymentFrameController::class, 'checkout_outstanding_gateway_entry']); // card entry form
});

Route::get('/', function () {
    return '<h1>HomeMaids</h1><br>Need something more in url ⏩ !';
});
Route::get('/api', function (Request $request) {
    return '<h1>HomeMaids</h1><br>Which api, add in url 🤐 ?';
});
