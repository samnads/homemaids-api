<?php

use App\Http\Controllers\CalculateController;
use App\Http\Controllers\CustomerApiAddressController;
use App\Http\Controllers\CustomerApiAreaController;
use App\Http\Controllers\CustomerApiBookingCancelController;
use App\Http\Controllers\CustomerApiBookingCancelReasonsController;
use App\Http\Controllers\CustomerApiBookingCreateController;
use App\Http\Controllers\CustomerApiBookingHistoryController;
use App\Http\Controllers\CustomerApiBookingRescheduleController;
use App\Http\Controllers\CustomerApiCardController;
use App\Http\Controllers\CustomerApiController;
use App\Http\Controllers\CustomerApiCouponController;
use App\Http\Controllers\CustomerApiDataController;
use App\Http\Controllers\CustomerApiDateTimeController;
use App\Http\Controllers\CustomerApiFaqController;
use App\Http\Controllers\CustomerApiServiceTypeController;
use App\Http\Controllers\CustomerApiMaidController;
use App\Http\Controllers\CustomerApiNotificationsController;
use App\Http\Controllers\CustomerApiOtpController;
use App\Http\Controllers\CustomerApiPackagesController;
use App\Http\Controllers\CustomerApiPaymentController;
use App\Http\Controllers\PaymentWebhookController;
use App\Http\Controllers\CustomerApiRetryPaymentController;
use App\Http\Controllers\CustomerApiRatingController;
use App\Http\Controllers\CustomerApiReferralController;
use App\Http\Controllers\CustomerApiServiceAddonsController;
use App\Http\Controllers\CustomerApiSpecialOffersController;
use App\Http\Controllers\CustomerApiWalletController;
use App\Http\Controllers\CustomerApiInvoiceController;
use App\Http\Controllers\AdminApiMailController;
use App\Http\Controllers\PaymentFrameController;
use App\Http\Controllers\CustomerApiOutstandingController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::prefix('customer')->group(function () {
    // for customer app only
    Route::group(['middleware' => 'validJsonInput'], function () {
        Route::get('available_datetime', [CustomerApiDateTimeController::class, 'available_datetime']); // GET | DONE
        Route::get('available_time', [CustomerApiDateTimeController::class, 'available_time']); // GET | DONE
        Route::get('crew_list', [CustomerApiMaidController::class, 'crew_list']); // GET | DONE
        Route::get('faq', [CustomerApiFaqController::class, 'faq']); // GET | DONE
        Route::get('faq_help', [CustomerApiFaqController::class, 'faq_help']); // GET | DONE
        Route::any('customer_login', [CustomerApiOtpController::class, 'customer_login']); // post | DONE
        Route::any('check_otp', [CustomerApiOtpController::class, 'check_otp']); // post | DONE
        Route::any('resend_otp', [CustomerApiOtpController::class, 'resend_otp']); // post | DONE
        //Route::get('service_types', [CustomerApiServiceTypeController::class, 'service_types']); // GET | DONE
        Route::get('service_type_data', [CustomerApiServiceTypeController::class, 'service_type_data']); // GET | DONE
        Route::get('cancel_reasons', [CustomerApiBookingCancelReasonsController::class, 'cancel_reasons']); // GET | DONE
        Route::get('coupon_list', [CustomerApiCouponController::class, 'coupon_list']); // GET | DONE
        Route::get('special_offers', [CustomerApiSpecialOffersController::class, 'special_offers']); // GET | DONE
        Route::get('packages', [CustomerApiPackagesController::class, 'packages']); // GET | DONE
        Route::get('service_addons', [CustomerApiServiceAddonsController::class, 'service_addons']); // GET | DONE
        Route::get('referral_credits', [CustomerApiReferralController::class, 'referral_credits']);
        Route::any('calculate', [CalculateController::class, 'calculate']);
        Route::get('area_list', [CustomerApiAreaController::class, 'area_list']); // GET | DONE
        Route::get('fcm_test', [CustomerApiNotificationsController::class, 'fcm_test']); // get | DONE
        Route::any('apple-process-pay', [PaymentFrameController::class, 'checkout_apple_pay_process']); // POST | DONE
        Route::any('google-pay-process', [PaymentFrameController::class, 'checkout_google_pay_process']);
        Route::any('outstanding-amount', [CustomerApiOutstandingController::class, 'outstanding_amount']);
    });
    Route::get('data', [CustomerApiDataController::class, 'data']);
    Route::any('payment/iframe', [PaymentFrameController::class, 'iframe']);
    Route::any('payment/payfort/tokenization', [PaymentFrameController::class, 'payfort_tokenization']);
    Route::any('payment/payfort/tokenization_form', [PaymentFrameController::class, 'tokenization_form']);
    // Route::any('payment/payfort/processing', [PaymentFrameController::class, 'processing']);
    Route::any('payment/payfort/redirect', [PaymentFrameController::class, 'redirect']);
    //Route::any('payment/payfort/verify', [PaymentFrameController::class, 'verify']);
    Route::any('payment/payfort/success', [PaymentFrameController::class, 'success']);
    Route::any('payment/payfort/failed', [PaymentFrameController::class, 'failed']);
    Route::any('payment/payfort/transaction_feedback', [PaymentFrameController::class, 'payfort_transaction_feedback']);
    // payment web hooks
    Route::post('payment/tamara/webhook/order_approved', [PaymentWebhookController::class, 'tamara_order_approved']);
    // checkout payment process
    Route::any('payment/checkout/process-and-redirect/{booking_id}', [PaymentFrameController::class, 'checkoutPaymentProcessAndRedirect']); // POST | DONE
    Route::any('payment/tamara/process-and-redirect/{booking_id}', [PaymentFrameController::class, 'tamaraPaymentProcessAndRedirect']); // POST | DONE
    // invoice payment
    Route::post('invoice-payment', [CustomerApiInvoiceController::class, 'invoice_payment']);
    Route::any('invoice-payment/checkout/process-and-redirect/{booking_id}', [CustomerApiInvoiceController::class, 'checkoutInvoicePaymentProcessAndRedirect']);
    //
    Route::any('payment/iframe', [PaymentFrameController::class, 'iframe']);
    Route::any('payment/ccavenue/verify', [PaymentFrameController::class, 'verify']);
    Route::any('payment/ccavenue/outstanding-verify', [PaymentFrameController::class, 'outstanding_verify']);
    Route::any('payment/ccavenue/processing', [PaymentFrameController::class, 'processing']);
    Route::any('payment/ccavenue/outstanding-processing', [PaymentFrameController::class, 'outstanding_processing']);
    Route::group(['middleware' => ['validJsonInput', 'customerTokenCheck']], function () {
        Route::any('validate_token', [CustomerApiController::class, 'validate_token']); // POST
        Route::get('address_list', [CustomerApiAddressController::class, 'address_list']); // GET / WAIT
        Route::any('set_default_address', [CustomerApiAddressController::class, 'set_default_address']); // GET / WAIT
        Route::any('name_entry', [CustomerApiController::class, 'name_entry']); // post | DONE
        Route::any('add_card', [CustomerApiCardController::class, 'add_card']); // post | DONE
        Route::any('card_details', [CustomerApiCardController::class, 'card_details']); // GET | DONE
        Route::any('link_card', [CustomerApiCardController::class, 'link_card']); // POST | DONE
        Route::any('delete_card', [CustomerApiCardController::class, 'delete_card']); // post | DONE
        Route::any('create_booking', [CustomerApiBookingCreateController::class, 'create_booking']); // post
        Route::any('reschedule', [CustomerApiBookingRescheduleController::class, 'reschedule']); // post
        Route::any('add_address', [CustomerApiAddressController::class, 'add_address']); // POST / DONE
        Route::any('edit_address', [CustomerApiAddressController::class, 'edit_address']); // POST / DONE
        Route::any('delete_address', [CustomerApiAddressController::class, 'delete_address']); // POST / DONE
        Route::any('update_customer_data', [CustomerApiController::class, 'update_customer_data']); // POST
        Route::any('update_avatar', [CustomerApiController::class, 'update_avatar']); // POST
        Route::any('booking_history', [CustomerApiBookingHistoryController::class, 'booking_history']); // TEST
        Route::any('booking_cancel', [CustomerApiBookingCancelController::class, 'booking_cancel']); // POST
        Route::any('booking_all_cancel', [CustomerApiBookingCancelController::class, 'booking_all_cancel']); // POST
        Route::any('customer_logout', [CustomerApiController::class, 'customer_logout']); // post | DONE
        Route::any('rate_booking', [CustomerApiRatingController::class, 'rate_booking']); // post
        Route::get('notifications', [CustomerApiNotificationsController::class, 'notifications'])->name('notifications'); // get | DONE
        Route::any('all_notification_read', [CustomerApiNotificationsController::class, 'all_notification_read']); // post | DONE
        Route::any('clear_notiication', [CustomerApiNotificationsController::class, 'clear_notiication']); // post | DONE
        Route::get('customer_soa', [CustomerApiController::class, 'customer_soa']); // GET | DONE
        Route::any('contact_send', [CustomerApiController::class, 'contact_send']); // POST
        Route::any('change_payment_mode', [CustomerApiPaymentController::class, 'change_payment_mode']); // POST | DONE
        Route::any('retry_payment', [CustomerApiRetryPaymentController::class, 'retry_payment']); // POST | DONE
        Route::get('wallet', [CustomerApiWalletController::class, 'wallet']);
        Route::any('delete_account', [CustomerApiController::class, 'deleteAccount']); // POST
        Route::get('customer_data', [CustomerApiController::class, 'customer_data']); // GET | DONE
        Route::post('booking_details_by_ref', [CustomerApiBookingHistoryController::class, 'booking_details_by_ref']); // TEST
    });
    Route::get('/', function (Request $request) {
        return '<h1>HomeMaids</h1><br>You\'re in customer app API base url 😀 !';
    });
});

Route::prefix('admin')->group(function () {
    Route::prefix('mail')->group(function () {
        // mail routes, helpul to resend mails from anywhere outside api
        Route::get('schedule-cancel-to-admin/{booking_id}/{service_date}', [AdminApiMailController::class, 'schedule_cancel_to_admin']);
        Route::get('schedule-cancel-to-customer/{booking_id}/{service_date}', [AdminApiMailController::class, 'schedule_cancel_to_customer']);
        Route::get('registration-success-to-customer/{customer_id}', [AdminApiMailController::class, 'registration_success_to_customer']);
    });
});

Route::get('email-test', function () {
    $details['email'] = 'samnad.s@azinova.info';
    dispatch(new App\Jobs\SendEmailJob($details));
});

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::fallback(function () {
    return '<h1>HomeMaids</h1><br>Oops, requested api endpoint not found 🥶 !';
});
